//
//  EnemyBullet.m
//  EscapeSpaceShooter
//
//  Created by William Birmingham on 11/16/19.
//  Copyright © 2019 William Birmingham. All rights reserved.
//

#import "EnemyBullet.h"

@implementation EnemyBullet{
    Constants *constants;
    CGVector dir;
    float speed;
    VectorSupport *vectorSupport;
}
-(id)init:(CGVector)direction{
    if (self = [super init:direction]){   //@"player_outline.png"]){
        constants = [Constants getInstance];
        self.physicsBody.categoryBitMask = constants.enemyBulletCollisionCategory;
        self.physicsBody.contactTestBitMask = constants.playerCollisionCategory | constants.screenEdgeCollisionCategory | constants.sceneWallCollisionCategory;
        self.damage = 1;
         self.physicsBody.dynamic = YES;
//NSLog(@"Enemybullet category %i %i", self.physicsBody.categoryBitMask, constants.enemyBulletCollisionCategory);
        
//        self.physicsBody.dynamic = NO;
//        self.physicsBody.usesPreciseCollisionDetection = NO;
//        self.physicsBody.restitution = 1;
//        self.physicsBody.angularDamping = 1;
//        self.physicsBody.linearDamping = 0.25;
//        self.physicsBody.friction = 0;
//        self.physicsBody.allowsRotation = YES;
//        self.physicsBody.angularVelocity = 0;
//        self.physicsBody.mass = 1;
        vectorSupport = [[VectorSupport alloc]init];
    }
    return self;
}

-(void)dealloc{
    
}

@end
