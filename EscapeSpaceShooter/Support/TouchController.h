//
//  CameraController.h
//  EscapeSpaceShooter
//
//  Created by William Birmingham on 12/20/19.
//  Copyright © 2019, 2020 Imaginary Root Studio LLC. All rights reserved.
//

#ifndef TouchController_h
#define TouchController_h
#import <Foundation/Foundation.h>
#import <SpriteKit/SpriteKit.h>
#import "Constants.h"
#import "Player.h"
#import "PlayerBullet.h"
#import "VectorSupport.h"
#import "TapticEngine.h"



@interface TouchController : NSObject

-(id)init:(Player*)p;
-(void)touchesBegan:(NSSet *)touches andScene:(SKScene *)scene;
-(void)touchesMoved:(NSSet<UITouch *> *)touches andScene:(SKScene *)scene andNumberOfBullets:(int)numberOfBulletsFired;
-(void)touchesCancelled:(NSSet<UITouch *> *)touches;
-(void)touchesEnded:(NSSet<UITouch *> *)touches;
@end
#endif
