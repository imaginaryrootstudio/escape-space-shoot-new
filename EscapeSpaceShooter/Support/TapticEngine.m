//
//  TapticEngine.m
//  Escape: A Space Shooter
// Copyright (c) 2019, 2020 Imaginary Root Studio LLC. All rights reserved.


#import "TapticEngine.h"

@implementation TapticEngine{
    Constants *constants;
    UIImpactFeedbackGenerator* impactFeedbackGenerator;
    
}

-(id)init:(UIImpactFeedbackStyle)style{
    if (self = [super init]){
        constants = [Constants getInstance];
        impactFeedbackGenerator = [[UIImpactFeedbackGenerator alloc] initWithStyle:style];
    }
    return self;
}

-(void)prepare{
    [impactFeedbackGenerator prepare];
}

-(void)playImpactHaptic{
    [impactFeedbackGenerator impactOccurred];
}

-(SKAction*)playDoubleImpactHaptic{
    SKAction *tap = [SKAction runBlock:^{
        [self->impactFeedbackGenerator impactOccurred];
    }];
    SKAction *prepare = [SKAction runBlock:^{
        [self->impactFeedbackGenerator prepare];
    }];
    SKAction *wait = [SKAction waitForDuration:0.25];
    SKAction *sequence = [SKAction sequence:@[tap, prepare, wait, tap, prepare, wait, tap]];
    return sequence;
}
@end
