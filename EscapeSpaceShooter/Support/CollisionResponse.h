//
//  CollisionResponse.h
//  EscapeSpaceShooter
//
//  Created by William Birmingham on 4/5/20.
//  Copyright © 2020 William Birmingham. All rights reserved.
//

#ifndef CollisionResponse_h
#define CollisionResponse_h

#import <Foundation/Foundation.h>
#import <SpriteKit/SpriteKit.h>
#import "Constants.h"
#import "VectorSupport.h"
#import "Bullet.h"
#import "Player.h"
#import "Bomb.h"
#import "ShooterBase.h"
#import "Enemy.h"
#import "SceneWall.h"
#import "PlayerBullet.h"
#import "TapticEngine.h"
#import "RailShooter.h"
#import "TargetedShootBase.h"
#import "ScoringLabel.h"
#import "KeyObject.h"

@protocol CollisionResponseProtocol
@required
-(void)reversePlayerPosition:(Player*)player;
-(void)changeScene;
-(void)updateLevelStat:(NSString*)statName andValue:(float)value;
-(void)processPickup:(SKNode*)pickup;
-(void)willZeroBulletsFired;
//-(void)playPlayerDamageFX;
@end

@interface CollisionResponse : NSObject

-(id)init;
-(void) response:(SKPhysicsContact*) contact; //(SKPhysicsBody*)object1 and:(SKPhysicsBody*)object2;
@property (nonatomic, weak) id <CollisionResponseProtocol> delegate;
//@property (nonatomic, strong) SKAudioNode* audioNode;

@end
#endif /* CollisionResponse_h */
