//
//  ScreenWalls.h
//  Escape: A Space Shooter
// Copyright (c) 2019, 2020 Imaginary Root Studio LLC. All rights reserved.

#import <Foundation/Foundation.h>
#import <SpriteKit/SpriteKit.h>
#import "Constants.h"

@interface ScreenWalls : NSObject

-(id)init:(SKScene*)scene andSampleSprite:(SKSpriteNode*)anySpriteNode;
-(NSArray*)buildWalls;

@end
