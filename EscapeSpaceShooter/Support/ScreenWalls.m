//
//  ScreenWalls.m
//  Escape: A Space Shooter
// Copyright (c) 2019, 2020 Imaginary Root Studio LLC. All rights reserved.

#import "ScreenWalls.h"

@implementation ScreenWalls{
    Constants *constants;
    SKScene *gameScene;
    SKSpriteNode *top;
    SKSpriteNode *bottom;
    SKSpriteNode *right;
    SKSpriteNode *left;
    
    uint32_t playerCategoryContact;
//    uint32_t redEnemyCategoryContact;
    uint32_t screenEdgeCategoryContact;
//    uint32_t bluePowerUpCategoryContact;
//    uint32_t greenPowerUpCategoryContact;
//    uint32_t yellowPowerUpCategoryContact;
//    uint32_t cyanPowerUpCategoryContact;
//    uint32_t purplePowerUpCategoryContact;

    float spriteWidth;
    float spriteHeight;
}

-(id)init:(SKScene*)scene andSampleSprite:(SKSpriteNode*)anySpriteNode{
    if(self = [super init]){
        constants = [Constants getInstance];
        spriteWidth = anySpriteNode.size.width;
        spriteHeight = anySpriteNode.size.height;
        gameScene = scene;
    }
    return self;
}

-(NSArray*)buildWalls{  //and make Mexico pay for them :)
    top = [[SKSpriteNode alloc]initWithColor:[UIColor whiteColor] size:CGSizeMake(gameScene.size.width, 10)];
    top.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:top.size];
    top.position = CGPointMake(100,100);//CGPointMake(gameScene.size.width/2,gameScene.size.height+spriteHeight);
    top.physicsBody.dynamic = NO;
    top.physicsBody.categoryBitMask = constants.screenEdgeCollisionCategory;
    top.physicsBody.contactTestBitMask = constants.playerCollisionCategory | constants.bulletCollisionCategory | constants.enemyBulletCollisionCategory | constants.playerBulletCollisionCategory| constants.enemyCollisionCategory;
    
    bottom = [[SKSpriteNode alloc]initWithColor:[UIColor whiteColor] size:CGSizeMake(gameScene.size.width, 10)];
    bottom.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:bottom.size];
    bottom.position = CGPointMake(gameScene.size.width/2, - spriteHeight);
    bottom.physicsBody.dynamic = NO;
    bottom.physicsBody.categoryBitMask = constants.screenEdgeCollisionCategory;
    bottom.physicsBody.contactTestBitMask = constants.playerCollisionCategory | constants.bulletCollisionCategory | constants.enemyBulletCollisionCategory | constants.playerBulletCollisionCategory| constants.enemyCollisionCategory;
    
//    bottom.physicsBody.categoryBitMask = screenEdgeCategoryContact;
//    bottom.physicsBody.contactTestBitMask = playerCategoryContact | gBulletCategory;
    
    right = [[SKSpriteNode alloc]initWithColor:[UIColor whiteColor] size:CGSizeMake(10, gameScene.size.height)];
    right.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:right.size];
    
    right.position = CGPointMake(gameScene.size.width + 2.5*spriteWidth, gameScene.size.height/2);
    right.physicsBody.dynamic = NO;
    right.physicsBody.categoryBitMask = constants.screenEdgeCollisionCategory;
    right.physicsBody.contactTestBitMask = constants.playerCollisionCategory | constants.bulletCollisionCategory | constants.enemyBulletCollisionCategory | constants.playerBulletCollisionCategory| constants.enemyCollisionCategory;
    
//    right.physicsBody.categoryBitMask = screenEdgeCategoryContact;
//    right.physicsBody.contactTestBitMask = playerCategoryContact | gBulletCategory;
//
    left = [[SKSpriteNode alloc]initWithColor:[UIColor whiteColor] size:CGSizeMake(10, gameScene.size.height)];
    left.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:left.size];
    left.position = CGPointMake(-2*spriteWidth, gameScene.size.height/2);
    left.physicsBody.dynamic = NO;
    left.physicsBody.categoryBitMask = constants.screenEdgeCollisionCategory;
    left.physicsBody.contactTestBitMask = constants.playerCollisionCategory | constants.bulletCollisionCategory | constants.enemyBulletCollisionCategory | constants.playerBulletCollisionCategory| constants.enemyCollisionCategory;
    
//    left.physicsBody.categoryBitMask = screenEdgeCategoryContact;
//    left.physicsBody.contactTestBitMask = playerCategoryContact | gBulletCategory;
    
    NSArray *foo = [NSArray arrayWithObjects:top,bottom,right, left, nil];
    return foo;
}

-(void)dealloc{
//    NSLog(@"ScreenWalls");
}

@end
