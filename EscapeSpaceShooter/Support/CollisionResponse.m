//
//  CollisionResponse.m
//  EscapeSpaceShooter
//
//  Created by William Birmingham on 4/5/20.
//  Copyright © 2020 William Birmingham. All rights reserved.
//

#import "CollisionResponse.h"
@implementation CollisionResponse{
    Constants *constants;
    TapticEngine *tapticEngineDouble;
}

-(id)init{
    if (self = [super init]){
        constants = [Constants getInstance];
        tapticEngineDouble = [[TapticEngine alloc]init:UIImpactFeedbackStyleMedium];
        [tapticEngineDouble prepare];
        [self.delegate willZeroBulletsFired];
    }
    return self;
}
-(void)response:(SKPhysicsContact*)contact{   //(SKPhysicsBody*)obj1 and:(SKPhysicsBody*)obj2{
    
    SKPhysicsBody *obj1, *obj2;
    
    if (contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask){
        obj1 = contact.bodyA;
        obj2 = contact.bodyB;
    }
    else{
        obj1 = contact.bodyB;
        obj2 = contact.bodyA;
    }
    if ((obj1.categoryBitMask & constants.playerCollisionCategory) != 0 &&
        (obj2.categoryBitMask & constants.enemyCollisionCategory) != 0){
        Player *p = (Player*)obj1.node;
        [p playerTakeDamage:1];
        [self.delegate reversePlayerPosition:p];
    }
    
    if ((obj1.categoryBitMask & constants.bulletCollisionCategory) != 0 &&
        (obj2.categoryBitMask & constants.screenEdgeCollisionCategory) != 0){
        Bullet *e = (Bullet*)obj1.node;
        [e removeAllActions];
        [e removeFromParent];
    }
    
    if ((obj1.categoryBitMask & constants.enemyCollisionCategory) != 0 &&
        (obj2.categoryBitMask & constants.playerBulletCollisionCategory) != 0){
        Bullet *b = (Bullet*)obj2.node;
        [b remove];
        Enemy *e = (Enemy*)obj1.node;
        [e playSound];
        NSArray *a = [NSArray arrayWithArray:[e children]];
        ShooterBase *sb = (ShooterBase*)[e childNodeWithName:@"shooterBase"];
        RailShooter *r = (RailShooter*)[e childNodeWithName:@"RailShooter"];
        CGPoint pos;
        bool showScore = YES;
//        self.audioNode = [[SKAudioNode alloc]initWithFileNamed:@"PlayerHurte.mp3"];
//        self.audioNode.autoplayLooped = NO;
//        self.audioNode.positional = YES;
//        //[self.adu addChild:self.audioNode];
//        SKAction *play = [SKAction play];
//       // SKAction *stop = [SKAction stop];
//        //SKAction *wait = [SKAction waitForDuration:0.5];
//        //SKAction *seq = [SKAction sequence:@[play, wait, stop]];
//        [self.audioNode runAction:play]; //seq];
        if (r){//[a[0] isKindOfClass:[RailShooter class]]){
            [r takeDamage:1.0];
            pos = CGPointMake(r.position.x, r.position.y);
            if (r.health <= 0)
                showScore = NO;
        }
        else{
            [sb takeDamage:1.0 andStopOnZeroHealth:YES];
            pos = CGPointMake(sb.position.x, sb.position.y);
            if (sb.health < 0)
                showScore = NO;
        }
        ScoringLabel *scoringLabel = [[ScoringLabel alloc]init:constants.currentScene withFont:gDefaultFontName
                                                      andColor:[UIColor whiteColor] andSize:28];
        if (showScore){
            [scoringLabel createLabel:e.position andText:@"+5"];
            constants.score += 5;
            [self.delegate updateLevelStat:@"enemiesHit" andValue:1];
        }
    }
    
    if ((obj1.categoryBitMask & constants.playerCollisionCategory) != 0 &&
        (obj2.categoryBitMask & constants.bulletCollisionCategory) != 0){
        Bullet *e = (Bullet*)obj1.node;
        [e removeAllActions];
        [e removeFromParent];
    }
#pragma mark Player-EnemyBullet
    if ((obj1.categoryBitMask & constants.playerCollisionCategory) != 0 &&
        (obj2.categoryBitMask & constants.enemyBulletCollisionCategory) != 0){
        EnemyBullet *e = (EnemyBullet*)obj2.node;
        int32_t n = e.damage;
        [e removeAllActions];
        [e removeFromParent];
        Player *p = (Player*)obj1.node;
        [p playerTakeDamage:n];
        [self.delegate updateLevelStat:@"numberOfTimesHit" andValue:1];
        ScoringLabel *scoringLabel = [[ScoringLabel alloc]init:constants.currentScene withFont:gDefaultFontName
                                                      andColor:[UIColor whiteColor] andSize:32];
        [scoringLabel createLabel:p.position andText:@"-1"];
        constants.score--;
        //          numberOfTimesHit++;
    }
    
    if ((obj1.categoryBitMask & constants.enemyBulletCollisionCategory) != 0 &&
        (obj2.categoryBitMask & constants.sceneWallCollisionCategory ) != 0){
        EnemyBullet *e = (EnemyBullet*)obj1.node;
        [e removeAllActions];
        [e removeFromParent];
    }
#pragma mark EnemyBullet-PlayerBullet
    if ((obj1.categoryBitMask & constants.enemyBulletCollisionCategory) != 0 &&
        (obj2.categoryBitMask & constants.playerBulletCollisionCategory ) != 0){
        EnemyBullet *e = (EnemyBullet*)obj1.node;
        [e removeAllActions];
        [e removeFromParent];
        PlayerBullet *b = (PlayerBullet*)obj2.node;
        [b removeAllActions];
        [b removeFromParent];
    }
#pragma mark Bullet-Scenewall
    if ((obj1.categoryBitMask & constants.bulletCollisionCategory) != 0 &&
        (obj2.categoryBitMask & constants.sceneWallCollisionCategory ) != 0){
        Bullet *e = (Bullet*)obj1.node;
        [e removeAllActions];
        [e removeFromParent];
        
    }
#pragma mark Player-SceneWall
    if ((obj1.categoryBitMask & constants.sceneWallCollisionCategory ) != 0 &&
        (obj2.categoryBitMask & constants.playerBulletCollisionCategory)){
        Bullet *e = (Bullet*)obj2.node;
        [e removeAllActions];
        [e removeFromParent];
    }
    
    if ((obj1.categoryBitMask & constants.playerCollisionCategory) != 0 &&
        (obj2.categoryBitMask & constants.sceneWallCollisionCategory ) != 0){
        
        Player* p = (Player*)obj1.node;
        SceneWall *w = (SceneWall*)obj2.node;
        [self.delegate reversePlayerPosition:p];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"RemoveBullet" object:self];
    }
    
    if ((obj1.categoryBitMask & constants.enemyCollisionCategory) != 0 &&
        (obj2.categoryBitMask & constants.sceneWallCollisionCategory ) != 0){
        //          Enemy *e = (Enemy*)obj1.node;
        //          coolDownTimer = gGunNormalCoolDown;
        NSLog(@"CollisionResponse::response Enemy Hit wall, how is this possible?");
    }
    
#pragma mark Player-Key
    if ((obj1.categoryBitMask & constants.playerCollisionCategory) != 0 &&
        (obj2.categoryBitMask & constants.keyCollisionCategory ) != 0){
#pragma TODO Add Protocol Function
        SKAction *foo = [tapticEngineDouble playDoubleImpactHaptic];
        [self.delegate changeScene];
    }
#pragma mark PlayerBullet-Key
    if ((obj1.categoryBitMask & constants.playerBulletCollisionCategory) != 0 &&
        (obj2.categoryBitMask & constants.keyCollisionCategory ) != 0){
        //        Bullet *e = (Bullet*)obj1.node;
        PlayerBullet *e = (PlayerBullet*)obj1.node;
        [e removeAllActions];
        [e removeFromParent];
        KeyObject* p = (KeyObject*)obj1.node;
        [p removeAllActions];
        [p removeAllChildren];
        [p removeFromParent];
        ScoringLabel *scoringLabel = [[ScoringLabel alloc]init:constants.currentScene withFont:gDefaultFontName
                                                      andColor:[UIColor whiteColor] andSize:32];
        [scoringLabel createLabel:p.position andText:@"!!"];
        [self.delegate updateLevelStat:@"enemiesHit" andValue:1];
    }
#pragma mark Player-Bomb
    if ((obj1.categoryBitMask & constants.playerCollisionCategory) != 0 &&
        (obj2.categoryBitMask & constants.bombCollisionCategory ) != 0){
        Bomb    *e = (Bomb*)obj2.node;
        
        SKAction *remove = [SKAction runBlock:^{
            [e removeAllActions];
            [e removeFromParent];
        }];
        SKAction *wait = [SKAction waitForDuration:1.5];
        SKAction *sequence = [SKAction sequence:@[wait, remove]];
        [constants.currentScene runAction:sequence];
        
        Player *p = (Player*)obj1.node;
        [p playerTakeDamage:5];
        ScoringLabel *scoringLabel = [[ScoringLabel alloc]init:constants.currentScene withFont:gDefaultFontName
                                                      andColor:[UIColor whiteColor] andSize:32];
        [scoringLabel createLabel:e.position andText:@"-10"];
        [self.delegate updateLevelStat:@"numberOfTimesHit" andValue:1];
    }
    
    if ((obj1.categoryBitMask & constants.playerCollisionCategory) != 0 &&
        (obj2.categoryBitMask & constants.movementBlockingWallCategory) != 0){
        Player *p = (Player*)obj1.node;
        [self.delegate reversePlayerPosition:p];
    }
    
    if ((obj1.categoryBitMask & constants.playerCollisionCategory) != 0 &&
        (obj2.categoryBitMask & constants.pickupCollisionCategory ) != 0){
        SKNode *p = (SKNode*)obj2.node;
        [self.delegate processPickup:p];
        ScoringLabel *scoringLabel = [[ScoringLabel alloc]init:constants.currentScene withFont:gDefaultFontName
                                                      andColor:[UIColor whiteColor] andSize:32];
        [scoringLabel createLabel:p.position andText:@"+5"];
        constants.score += 5;
    }
}
@end
