//
//  TapticEngine.h
//  Escape: A Space Shooter
// Copyright (c) 2019, 2020 Imaginary Root Studio LLC. All rights reserved.


#ifndef TapticEngine_h
#define TapticEngine_h
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <SpriteKit/SpriteKit.h>
#import "Constants.h"

@interface TapticEngine: NSObject


-(id) init:(UIImpactFeedbackStyle)style;
-(void)prepare;
-(void)playImpactHaptic;
-(SKAction*)playDoubleImpactHaptic;



@end


#endif /* TapticEngine_h */
