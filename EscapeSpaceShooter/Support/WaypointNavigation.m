//
//  WaypointNavigation.m
//  EscapeSpaceShooter
//
//  Created by William Birmingham on 12/23/19.
//  Copyright © 2019,, 2020 Imaginary Root Studio LLC. All rights reserved.
//

#import "WaypointNavigation.h"

@implementation WaypointNavigation{
    VectorSupport *vectorSupport;
    Constants *constants;
    SKNode *node;
    NSMutableArray *wayPointList;
    //    CGVector dir;
    int currentWayPoint;
    bool directionChangeAllowed;
}



-(id)init:(SKNode*)object andWaypoints:(NSArray*)waypoints{
    if (self = [super init]){
        constants = [Constants getInstance];
        vectorSupport = [[VectorSupport alloc]init];
        node = object;
        wayPointList = [[NSMutableArray alloc]init];
        [wayPointList addObjectsFromArray:waypoints];
        currentWayPoint = 0;
        directionChangeAllowed = YES;
        self.finishedWaypoints = NO;
        self.repeatForever = NO;
        // dir = CGVectorMake(0, 0);
        self.direction = CGVectorMake(0, 0);
    }
    return self;
}
/*-(CGVector)*/ -(void)calculateNewDirection{
    CGVector dir = CGVectorMake(0, 0);
    SKSpriteNode* currentNode = wayPointList[currentWayPoint];
    SKSpriteNode* nextNode = wayPointList[currentWayPoint + 1];
    //    NSLog(@"Currentnode  %f %f Nextnode  %f %f", currentNode.position.x, currentNode.position.y, nextNode.position.x, nextNode.position.y);
    //    CGPoint nextPosition = nextNode.position;
    dir = CGVectorMake(nextNode.position.x - currentNode.position.x, nextNode.position.y - currentNode.position.y);
    //    dir.dx *= -1;
    //    dir.dy *= -1;
    //dir = [vectorSupport normalize:dir];
    self.direction = [vectorSupport normalize:dir];
    node.zRotation = currentNode.zRotation;
    //return dir;
    //return self.direction;
}
-(void)changeDirectionChangeReset{
    directionChangeAllowed = YES;
}
-(void)navigateWaypoints{
    if (self.finishedWaypoints)     //It appears that removing all actions takes time, so this may run AFTER the waypoints are complete.
        return;
    SKSpriteNode* currentNode = wayPointList[currentWayPoint];
    SKSpriteNode* nextNode = wayPointList[currentWayPoint + 1];
    //        NSLog(@"Current %f %f  Next %f %f", currentNode.position.x, currentNode.position.y, nextNode.position.x, nextNode.position.y);
    CGVector distanceRaw = CGVectorMake(nextNode.position.x - node.position.x, nextNode.position.y - node.position.y);
    float distance = [vectorSupport magnitude:CGVectorMake(distanceRaw.dx, distanceRaw.dy)];
    if (distance < 15 && directionChangeAllowed){
        currentWayPoint++;
        if (!self.repeatForever && currentWayPoint == wayPointList.count-1 ){ // reached the end, we're done!
            self.finishedWaypoints = YES;
            return;
        }
        currentWayPoint %= wayPointList.count; // rollover for infinite looping
        directionChangeAllowed = NO;
        /*self.direction = */[self calculateNewDirection];
        [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(changeDirectionChangeReset) userInfo:NULL repeats:NO];
    }
}
-(void)follow{
    [self calculateNewDirection];
    directionChangeAllowed = NO;
    [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(changeDirectionChangeReset) userInfo:NULL repeats:NO];
    SKAction *runBlock = [SKAction runBlock:^{
        [self navigateWaypoints];
    }];
    
    SKAction *sleep = [SKAction waitForDuration:0.1];
    SKAction *sequence = [SKAction sequence:@[runBlock,sleep]];
    SKAction *run = [SKAction repeatActionForever:sequence];
    [node runAction:run];
}


@end
