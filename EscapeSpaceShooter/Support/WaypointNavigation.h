//
//  WaypointNavigation.h
//  EscapeSpaceShooter
//
//  Created by William Birmingham on 12/23/19.
//  Copyright © 2019, 2020 Imaginary Root Studio LLC. All rights reserved.
//

#ifndef WaypointNavigation_h
#define WaypointNavigation_h
#import <Foundation/Foundation.h>
#import <SpriteKit/SpriteKit.h>
#import "Constants.h"
#import "VectorSupport.h"

//Notes:
// 1) Starts waypoint navigation at waypoints[0]
// 2) finishedWaypoints is YES after one cycle through the waypoints
// 3) updates direction and requires CALLER to update its own position

@interface WaypointNavigation : NSObject

@property bool finishedWaypoints;
@property bool repeatForever;
@property CGVector direction;

-(id)init:(SKNode*)object andWaypoints:(NSArray*)waypoints;
-(void)follow;

@end
#endif /* WaypointNavigation_h */
