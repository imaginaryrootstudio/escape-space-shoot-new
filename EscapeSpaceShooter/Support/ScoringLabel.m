//  ScoringLabel.m
//  Escape Space Shooter (based on CatchTheDoodle)
//
//  Created by William Birmingham on 12/16/15.
//  Copyright © 2015, 2020, 2021,  William Birmingham. All rights reserved.
//


#import "ScoringLabel.h"


@implementation ScoringLabel{
    SKScene* scene;
    VectorSupport *vectorSupport;
}

CGPoint screenCenter;

//Constants
float timeForActions = 1.1;
float speed = 75;
float scaleAmount = 5;


-(id) init:(SKScene*)gameScene withFont:(NSString*)font andColor:(UIColor*)color andSize:(int)size{
    if (self = [super init]){
        scene = gameScene;
        self.fontName = font;
        self.fontColor = color;
        self.fontSize = size;
        
        screenCenter.x = [UIScreen mainScreen].bounds.size.width/2;
        screenCenter.y = [UIScreen mainScreen].bounds.size.height/2;
        
        vectorSupport = [[VectorSupport alloc] init];
    }
    return self;
}

-(CGVector)getFlyVector:(CGPoint)location{
    int x = 1;
    int y = 1;
    if (location.x > screenCenter.x)
        x = -1;
    if (location.y > screenCenter.y)
        y = -1;
    CGVector bar = CGVectorMake(x, y);
    bar =  [vectorSupport normalize:bar];
    return bar;
}

-(void)createLabelWithNoVelocity:(CGPoint)location andText:(NSString *)text{
    SKLabelNode *label = [[SKLabelNode alloc]initWithFontNamed:self.fontName];
    label = [SKLabelNode labelNodeWithFontNamed:self.fontName];
    label.position = location;
    label.fontColor = self.fontColor;
    label.fontSize = self.fontSize;
    label.text = text;
    label.zPosition = 1.0;
    
    label.colorBlendFactor = 0;
    
    SKAction *fadeOut = [SKAction fadeOutWithDuration:timeForActions];
    SKAction *scale = [SKAction scaleTo: scaleAmount duration:timeForActions];
    SKAction *remove = [SKAction removeFromParent];
    SKAction *group = [SKAction group:@[fadeOut, scale]];
    SKAction *sequence = [SKAction sequence:@[group, remove]];
    [label runAction:sequence];
    [scene addChild:label];
}
-(void)createLabel:(CGPoint)location andText:(NSString *)text{
    SKLabelNode *label = [[SKLabelNode alloc]initWithFontNamed:self.fontName];
    label = [SKLabelNode labelNodeWithFontNamed:self.fontName];
    label.position = location;
    label.fontColor = self.fontColor;
    label.fontSize = self.fontSize;
    label.text = text;
    label.zPosition = 1.0;
    
    label.colorBlendFactor = 0;
    
    CGVector foo = [self getFlyVector:location];
    foo = [vectorSupport scalarMultiply:foo andScalar:speed];
    
    SKAction *move = [SKAction moveBy:foo duration:timeForActions];
    SKAction *fadeOut = [SKAction fadeOutWithDuration:timeForActions];
    SKAction *scale = [SKAction scaleTo: scaleAmount duration:timeForActions];
    SKAction *remove = [SKAction removeFromParent];
    SKAction *group = [SKAction group:@[move, fadeOut, scale]];
    SKAction *sequence = [SKAction sequence:@[group, remove]];
    [label runAction:sequence];
    [scene addChild:label];
}

@end
