//
//  Constants.m
//  Escape: A Space Shooter
// Copyright (c) 2019, 2020 Imaginary Root Studio LLC. All rights reserved.


#import "Constants.h"
#include <sys/sysctl.h>
#include <sys/utsname.h>

@implementation Constants{
    //    FileReadWrite *fileReadWrite;
    //    IAPSupport *iapSupport;
}

//Singleton Method
+ (Constants *) getInstance{
    static Constants* sharedSingleton;
    @synchronized(self)
    {
        if (!sharedSingleton)
            sharedSingleton = [[Constants alloc] init];
        return sharedSingleton;
    }
}

//---------
//Helper functions
//---------

+(UIColor*)sceneLightColor{
    UIColor* foo = [UIColor colorWithRed:0.85 green:0.85 blue:0.90 alpha:0.8];
    return foo;
}

-(UIViewController*) getRootViewController {
    return [UIApplication
            sharedApplication].keyWindow.rootViewController;
}

-(id) init {
    if ((self = [super init])) {
        self.frameTimeInterval = 0.01667;
        struct utsname systemInfo;
        uname(&systemInfo);
        NSString *platform = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
        // platforms ids from https://gist.github.com/adamawolf/3048717
        //        iPhone10,3 : iPhone X
        //        iPhone10,6 : iPhone X GSM
        //        iPhone11,2: iPhone XS
        //        iPhone11,4: iPhone XS Max
        //        iPhone11,6: iPhone XS Max China
        //        iPhone11,8: iPhone XR
        if ([platform isEqualToString:@"iPhone10,3"] || [platform isEqualToString:@"iPhone10,6"]
            || [platform isEqualToString:@"iPhone11,2"] || [platform isEqualToString:@"iPhone11,4"]
            || [platform isEqualToString:@"iPhone11,6"]) //XS and X Max
            self.isiPhoneX = YES;
        else
            self.isiPhoneX = NO;
        
        self.timeToSpawn = 2.5;
        
        //music and sound FX
        self.backgroundMusicEnabled = YES;
        self.soundFxEnabled = YES;
        
        //Animation support
        
        //enemy Stop
        //        self.stopTextureAtlas = [SKTextureAtlas atlasNamed:@"EnemyStop"];
        //        int foo = (int)self.stopTextureAtlas.textureNames.count;
        //        self.stopCels = [[NSMutableArray alloc]init];
        //        for (int i = 0; i<foo;i++){
        //            NSString *name = [NSString stringWithFormat:@"spritesheet_%d", i];
        //            SKTexture *bar = [self.stopTextureAtlas textureNamed:name];
        //            [self.stopCels addObject:bar];
        //        }
        //
        //        //enemy Die
        //        self.enemyDieTextureAtlas = [SKTextureAtlas atlasNamed:@"EnemyDieAnimation"];
        //        foo = (int)self.enemyDieTextureAtlas.textureNames.count;
        //        self.enemyDieCels = [[NSMutableArray alloc]init];
        //        for (int i = 0; i<foo;i++){
        //            NSString *name = [NSString stringWithFormat:@"enemy_death_%d", i];
        //            SKTexture *bar = [self.enemyDieTextureAtlas textureNamed:name];
        //            [self.enemyDieCels addObject:bar];
        //        }
        //
        //        //player Die
        //        self.playerDieTextureAtlas = [SKTextureAtlas atlasNamed:@"PlayerDieAnimation"];
        //        foo = (int)self.playerDieTextureAtlas.textureNames.count;
        //        self.playerDieCels = [[NSMutableArray alloc]init];
        //        for (int i = 0; i<foo;i++){
        //            NSString *name = [NSString stringWithFormat:@"player_death%d", i];
        //            SKTexture *bar = [self.playerDieTextureAtlas textureNamed:name];
        //            [self.playerDieCels addObject:bar];
        //        }
        
        self.matchmakingStarted = NO;
        self.usingAI = NO;
        self.cancelAI = NO;
        self.showInstructions = YES;
        self.playerDisplayName = @"Me!";
//        self.timeInWave = 0;
//        self.timeInGame = 0;
//        self.waveCounter = 0;
//        self.numberOfWaves = 0;
//        self.numberOfWaveCycles = 0;
        
        //TODO: change following line for multiplayer
        self.numberOfPlayers = 1;
        
        self.showGameOverScene = NO;
#pragma mark Collision Categories
        self.noCollisionCategory = 0x1 << 0;
        self.playerCollisionCategory =0x1 << 1;
        self.bulletCollisionCategory = 0x1 << 2;
        self.enemyCollisionCategory = 0x1 << 3;
        self.screenEdgeCollisionCategory = 0x1 << 4;
        self.enemyBulletCollisionCategory = 0x1 << 5;
        self.sceneWallCollisionCategory = 0x1 << 6;
        self.playerBulletCollisionCategory = 0x1 << 7;
        self.keyCollisionCategory = 0x1 << 8;
        self.bombCollisionCategory = 0x1 << 9;
        self.movementBlockingWallCategory = 0x1 << 10;
        self.pickupCollisionCategory = 0x1 << 11;
        
        self.score = 0;
        self.enemyAttackType = kNoAttack;
    }
    return self;
}

//arrayOfStaringIntegers must be NSNumber, like the return type
-(NSNumber*)returnUniqueInteger:(NSArray*)arrayOfStartingIntegers withSeed:(int)seed{
    NSNumber *uniqueNumber = [[NSNumber alloc]init];
    NSUInteger index;
    do{
        int newNumber = arc4random_uniform(gOffsetForUniqueInteger) + seed;
        uniqueNumber = [NSNumber numberWithInt:newNumber];
        index = [arrayOfStartingIntegers indexOfObject:uniqueNumber];
        //        NSLog(@"Looping in returnUniqueInteger %@",uniqueNumber);
    }while (index != NSNotFound);
    //    NSLog(@"UniqueInteger %@",uniqueNumber);
    return uniqueNumber;
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    //     NSLog(@"Constants dealloc **");
}

@end
