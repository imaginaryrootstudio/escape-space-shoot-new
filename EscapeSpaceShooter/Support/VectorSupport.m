//
//  VectorSupport.m

//  Copyright (c) 2017, 2018, 2019, , 2020 Imaginary Root Studio LLC. All rights reserved.
//

#import "VectorSupport.h"

@implementation VectorSupport{
    
}

-(id)init{
    if (self = [super init]){
    }
    return self;
}
-(CGVector)add:(CGVector)v1 andVector2:(CGVector)v2{
    CGVector result= CGVectorMake(v1.dx+v2.dx, v1.dy+v2.dy);
    return result;
}

-(CGVector)subtract:(CGVector)v1 andVector2:(CGVector)v2{
    CGVector result= CGVectorMake(v1.dx-v2.dx, v1.dy-v2.dy);
    return result;
}
-(CGVector)makeVectorFromPoints:(CGPoint)p1 andPoint:(CGPoint)p2{
    CGVector result= CGVectorMake(p1.x-p2.x,p1.y-p2.y);
    return result;
}

-(float)magnitude:(CGVector)v1{
    return sqrtf(v1.dx*v1.dx + v1.dy*v1.dy);
}

-(CGVector)normalize:(CGVector)v1{
    if (v1.dx == 0 && v1.dy == 0)
        return v1;
    float mag = [self magnitude:v1];
    CGVector temp = CGVectorMake(v1.dx/mag, v1.dy/mag);
//    if (temp.dx < 0 || temp.dy < 0)
//        NSLog(@"Negative");
    return temp;
}

-(double)convertVectorToRotation:(CGVector)v1{
    return -atan2((double)v1.dx, (double)v1.dy);
}

-(CGVector)scalarMultiply:(CGVector)v1 andScalar:(float)s{
    CGVector temp = CGVectorMake(v1.dx*s , v1.dy*s);
    return temp;
}

-(BOOL)vectorEqualZero:(CGVector)v1{
    if (v1.dx == 0 && v1.dy == 0)
        return YES;
    else
        return NO;
}
-(double)dotProduct:(CGVector)v1 andVector:(CGVector)v2{
    double product = v1.dx * v2.dx + v1.dy * v2.dy;
    return product;
}

#pragma mark CGPOINT

-(BOOL)CGPointEqual:(CGPoint)p1 andPoint2:(CGPoint)p2{
    if (p1.x==p2.x && p1.y == p2.y)
        return YES;
    return NO;
}
-(BOOL)CGPointEqualZero:(CGPoint)p1{
    if (p1.x==0 && p1.y == 0)
        return YES;
    return NO;
}
-(CGPoint)CGPointSubtraction:(CGPoint)p1 andPoint2:(CGPoint)p2{
    return CGPointMake(p1.x-p2.x, p1.y - p2.y);
}
-(float)CGPointDistance:(CGPoint)p1 andPoint2:(CGPoint)p2{
    CGPoint temp = [self CGPointSubtraction:p1 andPoint2:p2];
    return sqrtf(temp.x*temp.x + temp.y*temp.y);
}

-(float)lerp:(float) v0 andPoint2:(float) v1 andWeight:(float) t{
  return (1 - t) * v0 + t * v1;
}
@end
