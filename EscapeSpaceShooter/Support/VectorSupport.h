//
//  VectorSupport.h
//  Copyright (c) 2017, 2018, 2019, 2020 Imaginary Root Studio LLC. All rights reserved.
//

//#import <Foundation/Foundation.h>
#import <SpriteKit/SpriteKit.h>
#import "Constants.h"

@interface VectorSupport : NSObject

-(id)init;

//CGVector

-(CGVector)add:(CGVector)v1 andVector2:(CGVector) v2;
-(CGVector)subtract:(CGVector)v1 andVector2:(CGVector) v2;
-(CGVector)makeVectorFromPoints:(CGPoint)p1 andPoint:(CGPoint)p2;
-(CGVector)normalize:(CGVector)v1;
-(float)magnitude:(CGVector)v1;
-(CGVector)scalarMultiply:(CGVector)v1 andScalar:(float)s;
-(BOOL)vectorEqualZero:(CGVector)v1;
-(double)dotProduct:(CGVector)v1 andVector:(CGVector)v2;
-(double)convertVectorToRotation:(CGVector)v1;

//CGPoint
-(BOOL)CGPointEqual:(CGPoint)p1 andPoint2:(CGPoint)p2;
-(BOOL)CGPointEqualZero:(CGPoint)p1;
-(CGPoint)CGPointSubtraction:(CGPoint)p1 andPoint2:(CGPoint)p2;
-(float)CGPointDistance:(CGPoint)p1 andPoint2:(CGPoint)p2;

//Lerp
-(float)lerp:(float) v0 andPoint2:(float) v1 andWeight:(float) t;

@end
