//
//  CameraController.m
//  EscapeSpaceShooter
//
//  Created by William Birmingham on 12/20/19.
//  Copyright © 2019, 2020 Imaginary Root Studio LLC. All rights reserved.
//

#import "CameraController.h"

@implementation CameraController{
    SKCameraNode *c;
    NSArray *wayPointList;
    Player *p;
    CGVector dir;
    WaypointNavigation *navigation;
    VectorSupport *vectorSupport;
    float xScale;
    float yScale;
 
}

-(id)init{
    NSLog(@"Do not use the default init: CameraController.m");
    return self;
}

-(id)init:(SKCameraNode*)camera andWaypoints:(NSArray*)waypoints andPlayer:(Player*)player{
    if (self = [super init]){
        wayPointList = [NSArray arrayWithArray:waypoints];
        c = camera;
        xScale = c.xScale;
        yScale = c.yScale;
        self.cameraScaleFollowPlayer = CGVectorMake(c.xScale, c.yScale);
        c.xScale = 1.5;
        c.yScale = 1.5;
        p = player;
        p.hidden = YES;
        dir = CGVectorMake(0, 0);
        navigation = [[WaypointNavigation alloc] init:c andWaypoints:wayPointList];
        vectorSupport = [[VectorSupport alloc]init];
        self.nowFollowingPlayer = NO;
        self.finishedWaypoints = NO;
    }
    return self;
}

-(void)followPlayer{
    //p.hidden = NO;
    self.nowFollowingPlayer = YES;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CameraFollowingPlayer" object:self];
    [p fadeInPlayer];
    c.xScale = self.cameraScaleFollowPlayer.dx;
    c.yScale = self.cameraScaleFollowPlayer.dy;
    //    [p addChild:c];
    SKAction *follow = [SKAction runBlock:^{
        CGPoint t = CGPointMake([self->vectorSupport lerp:self->c.position.x andPoint2:self->p.position.x andWeight:0.05],
                                [self->vectorSupport lerp:self->c.position.y andPoint2:self->p.position.y andWeight:0.05]);
        self->c.position = t;
    }];
    SKAction *wait = [SKAction waitForDuration:0.05];
    SKAction *seq = [SKAction sequence:@[follow, wait]];
    [c runAction:[SKAction repeatActionForever:seq]];
}
-(void)unfollowPlayer{
    [c removeAllActions];
}

-(void)followWaypoints:(BOOL)followPlayerAfterFlyover andFlyOverSpeed:(float)flyOverSpeed{
//    c.xScale = 1.5;
//    c.yScale = 1.5;
    navigation.repeatForever = NO;
    //    dir = navigation.direction;
    c.xScale = self.cameraScaleFlyover.dx;
    c.yScale = self.cameraScaleFlyover.dy;
    SKAction *runBlock = [SKAction runBlock:^{
        self->dir = self->navigation.direction;
        CGVector deltaPosition = self->dir;
        deltaPosition = [self->vectorSupport scalarMultiply:deltaPosition andScalar: flyOverSpeed];
        deltaPosition = [self->vectorSupport add:deltaPosition andVector2:CGVectorMake(self->c.position.x,
                                                                                       self->c.position.y)];
        self->c.position = CGPointMake(deltaPosition.dx, deltaPosition.dy);
//        NSLog(@"Finished %i", navigation.finishedWaypoints);
        if (self->navigation.finishedWaypoints){
//            NSLog(@"Finished waypoints");
            self.finishedWaypoints = YES;
            [self->c removeAllActions];
            if (followPlayerAfterFlyover)
                [self followPlayer];
        }
    }];
    
    SKAction *sleep = [SKAction waitForDuration:0.05];
    SKAction *sequence = [SKAction sequence:@[runBlock,sleep]];
    SKAction *run = [SKAction repeatActionForever:sequence];
    [c runAction:run];
    [navigation follow];
}
-(void)setCameraScale:(CGVector)cameraScale{
    c.xScale = cameraScale.dx;
    c.yScale = cameraScale.dy;
}
@end
