
//  ScoringLabel.h
//  Escape Space Shooter (based on CatchTheDoodle)
//
//  Created by William Birmingham on 12/16/15.
//  Copyright © 2015, 2020, 2021,  William Birmingham. All rights reserved.
//

#ifndef ScoringLabel_h
#define ScoringLabel_h
#import <Foundation/Foundation.h>
#import <SpriteKit/SpriteKit.h>
#import "VectorSupport.h"

@interface ScoringLabel : NSObject

@property int fontSize;
@property NSString *fontName;
@property UIColor* fontColor;


//-(id) init;
-(id) init:(SKScene*)gameScene withFont:(NSString*)font andColor:(UIColor*)color andSize:(int)size;
-(void)createLabel:(CGPoint)location andText:(NSString*)text;
-(void)createLabelWithNoVelocity:(CGPoint)location andText:(NSString *)text;
@end

#endif /* ScoringLabel_h */
