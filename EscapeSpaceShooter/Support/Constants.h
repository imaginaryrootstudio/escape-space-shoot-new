//
//  Constants.h
//  Escape: A Space Shooter
// Copyright (c) 2019, 2020 Imaginary Root Studio LLC. All rights reserved.

//#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <SpriteKit/SpriteKit.h>

#pragma mark SoundFX file names
#define gPlayerShooterSoundFX @"NewPlayerShoot.mp3"
#define gPlayerHurtSoundFX @"PlayerHurt.mp3"
#define gEnemyDamageSoundFX @"EnemyDamage.mp3"

//Font website: https://www.fontspace.com/space-wham-font-f30570
#define gDefaultFontName @"Space Wham"

// Key for player high score in user defaults
#define kPlayerHighScoreUserDefaults @"backgroundColor" //trying to confuse hackers

//File for high scores (if GameCenter is not available)
#define kHighScoreFile @"highScore.txt"

//Game Center
#define kLeaderboardPoints @"imaginaryrootstudio.SquaredTwo.Time"
#define gOffsetForUniqueInteger 80

//http://stackoverflow.com/questions/27781733/ios-sprite-kit-rotating-a-skspritenode
#define SK_DEGREES_TO_RADIANS(_ANGLE_) ((_ANGLE_) * 0.01745329252f) // PI / 180
#define SK_RADIANS_TO_DEGREES(_ANGLE_) ((_ANGLE_) * 57.29577951f) // PI * 180

#pragma mark Text Constants
#define gContinueText @"--Tap here to continue--" //@"--Tap anywhere to continue--"
#pragma mark Player Constants

#pragma mark Player
//Player  constants
#define gPlayerWalkSpeed 3.0
#define gPlayerRunSpeed 6.0
#define gPlayerDeadSpace 75
#define gSceneFlyOverSpeed 10
#define gTextFlyOverSpeed 5
#define gEnemyHealth 2


#define gPlayerColorBlendFactor 0.9
#define gPlayerTextSize 24
#define gPlayerFadeOutAlpha 0.15;
#define gPlayerAliasLabelName @"PlayerAliasLabel"
#define gPlayerZPosition 0.9
#define gPlayerScale 0.08
#define gPlayerInitialHealth 2
#define gPlayerInvincibilityTime 2;

//Enemy
#define gBulletNormalSpeed 9.5
#define gBulletHighSpeed 5.5
#define gBulletLifetime 4
#define gBulletZPosition 0.1

//Gun
#define gGunNormalCoolDown .25
#define gGunDirectionCalcPeriod 0.01

#pragma mark Contact Constants
//Contact constants   ---   MUST BE POWER OF 2
//#define gNoCategory 0
//#define gPlayerCategory 1
//#define gBulletCategory 2
//#define gEnemyCategory 4
//#define gScreenEdgeCategory 8
//#define gRedEnemyCategory 2
//#define gScreenEdgeCategory 4
//#define gGreenPowerUpCategory 8
//#define gYellowPowerUpCategory 16
//#define gBluePowerUpCategory 32
//#define gCyanPowerUpCategory 64
//#define gPurplePowerUpCategory 128

//#pragma mark Powerup Constants
////Powerup constants
//#define gPowerUpZPosition 0.6
//#define gPowerUpScale gPlayerScale
//#define gPowerUpMinimumVelocity 70
//#define gPowerUpVelocityvariance 80
//#define gBluePowerUpInvincibilityAmount 5
//#define gPurplePowerUpSlowDownTime 5
//#define gPurplePowerUpVelocitySlowDown 0.25



//Multiplayer matchmatching support
#define gMatchMakingTimeToWaitBeforeStartingAI 27   //seconds


typedef NS_ENUM(NSInteger, BulletType) {
    kNoBulletType,
    kSimpleBulletType,
    kHeavyBulletType,
    kPlayerBulletType
};

//Attack types

typedef NS_ENUM(NSInteger, kEnemyAttackType) {
    kNoAttack
};

typedef NS_ENUM(NSInteger, PowerUpType) {
    kNoPowerUp
};


//Game States

typedef NS_ENUM(NSInteger, GameState) {
    kGameStateNotSet
};

//Message Types

typedef NS_ENUM(NSInteger, MessageType) {
    kNoMessageType
};



@interface Constants : NSObject
//#define gNoCategory 0
//#define gPlayerCategory 1
//#define gBulletCategory 2
//#define gEnemyCategory 4
//#define gScreenEdgeCategory 8

#pragma mark Collision Categories
@property uint32_t noCollisionCategory;
@property uint32_t playerCollisionCategory;
@property uint32_t bulletCollisionCategory;
@property uint32_t enemyCollisionCategory;
@property uint32_t screenEdgeCollisionCategory;
@property uint32_t enemyBulletCollisionCategory;
@property uint32_t sceneWallCollisionCategory;
@property uint32_t playerBulletCollisionCategory;
@property uint32_t keyCollisionCategory;
@property uint32_t bombCollisionCategory;
@property uint32_t movementBlockingWallCategory;
@property uint32_t pickupCollisionCategory;

//Timing variables
@property float frameTimeInterval;
@property float timeToSpawn;

//Global variables
@property NSString *playerDisplayName;
@property BOOL isiPhoneX;

@property BOOL soundFxEnabled;
@property BOOL backgroundMusicEnabled;
//@property BOOL playerAuthenticated;
@property NSInteger numberOfPlayers;

////Game difficulty vars
//@property long numberOfWaves;
//@property float timeInWave;
//@property float timeInGame;  // survival time for the player
//@property long waveCounter;
//@property long numberOfWaveCycles; //every four waves is a cycle

//Game Attack type
@property kEnemyAttackType enemyAttackType;

//matchingmaking timer support
@property BOOL matchmakingStarted;
@property BOOL usingAI;
@property BOOL cancelAI; // if user cancels matchmaking, don't allow AI to start match

////Animation support
//@property SKTextureAtlas *stopTextureAtlas;
//@property SKTextureAtlas *enemyDieTextureAtlas;
//@property SKTextureAtlas *playerDieTextureAtlas;
//@property NSMutableArray *stopCels;
//@property NSMutableArray *enemyDieCels;
//@property NSMutableArray *playerDieCels;

#pragma mark Score
@property uint32_t score;
@property uint32_t bulletsFired;

//Instruction control
@property bool showInstructions;

//Support for delaying game over
@property BOOL showGameOverScene;

////YES if player had his finger on the player icon during game play
//@property BOOL fingerOnPlayer;
//@property int numberOfTimesFingerIsOnPlayer;
//@property int numberOfTimesFingerIsOffPlayer;

//@property (strong) NSDateFormatter *dateFormatter;
@property SKScene *currentScene;

+(Constants*) getInstance;

//Helper functions
-(UIViewController*) getRootViewController;
-(NSNumber*)returnUniqueInteger:(NSArray*)arrayOfStartingIntegers withSeed:(int)seed;


@end
