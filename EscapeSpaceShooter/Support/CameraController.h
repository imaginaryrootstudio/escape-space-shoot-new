//
//  CameraController.h
//  EscapeSpaceShooter
//
//  Created by William Birmingham on 12/20/19.
//  Copyright © 2019, 2020 Imaginary Root Studio LLC. All rights reserved.
//

#ifndef CameraController_h
#define CameraController_h
#import <Foundation/Foundation.h>
#import <SpriteKit/SpriteKit.h>
#import "Constants.h"
#import "Player.h"
#import "WaypointNavigation.h"
#import "VectorSupport.h"



@interface CameraController : NSObject
@property bool nowFollowingPlayer;
@property CGVector cameraScaleFlyover;
@property CGVector cameraScaleFollowPlayer;
@property bool finishedWaypoints;

-(id)init:(SKCameraNode*)camera andWaypoints:(NSArray*)waypoints andPlayer:(Player*)player;
-(void)followPlayer;
-(void)unfollowPlayer;
-(void)followWaypoints:(BOOL)followPlayerAfterFlyover andFlyOverSpeed:(float)flyOverSpeed;
-(void)setCameraScale:(CGVector)cameraScale;
@end
#endif /* CameraController_h */
