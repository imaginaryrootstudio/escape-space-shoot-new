//
//  CameraController.m
//  EscapeSpaceShooter
//
//  Created by William Birmingham on 12/20/19.
//  Copyright © 2020 Imaginary Root Studio LLC. All rights reserved.
//

#import "TouchController.h"

@implementation TouchController{
    Player *player;
    float coolDownTimer;
    bool inCoolDown;
    bool ignoreTouchesCollision;
    Constants *constants;
    UITouch *currentTouch;
    float touchPositionX;
    float touchPositionY;
    VectorSupport *vectorSupport;
    CGVector selfAngle;
}

-(id)init{
    NSLog(@"Do not use the default init: TouchController.m");
    return self;
}

-(id)init:(Player*)p{
    if (self = [super init]){
        constants = [Constants getInstance];
        player = p;
        currentTouch = nil;
        touchPositionX = 0;
        touchPositionY = 0;
        vectorSupport = [[VectorSupport alloc]init];
        selfAngle = CGVectorMake(0, 1);
    }
    return self;
}

-(void)touchesBegan:(NSSet *)touches andScene:(SKScene *)scene{
    UITouch *touch = [touches anyObject];
    //CGPoint location = [touch locationInNode:scene];
   // SKNode *node = [self nodeAtPoint:location];
    if (currentTouch != nil)
        return;
//    if (endGame)
//        return;
    if (player.isHidden)
        return;
    for (UITouch *touch in touches) {
       // CGPoint location = [touch locationInNode:scene];
        touchPositionX = [touch locationInNode:scene].x;
        touchPositionY = [touch locationInNode:scene].y;
        currentTouch = touch;
    }
    ignoreTouchesCollision = NO;
    //[player addChild:playerTrailFX];
    [player playPlayerTrailFX];
    
}

-(void)touchesMoved:(NSSet<UITouch *> *)touches andScene:(SKScene *)scene andNumberOfBullets:(int)numberOfBulletsFired{
    if (ignoreTouchesCollision){
        return;
    }
//    if (endGame)
//        return;
    if (player.isHidden)
        return;
    float deltaX = 0;
    float deltaY = 0;
    //    Player *p = ((Player*)players[localPlayerIndex]);
    int iPhoneNotchFactorHorizontal = 0;
    int iPhoneNotchFactorVertical = 0;
    if (constants.isiPhoneX){
        iPhoneNotchFactorHorizontal = 60;
        iPhoneNotchFactorVertical = 78;
    }
    CGVector rotationVector = CGVectorMake(0, 0);
    for (UITouch *t in touches){
        if (t != currentTouch)
            continue;
        CGPoint location = [t locationInNode:scene];
        if (touchPositionX != 0 && touchPositionY != 0){
            /*CGVector*/ rotationVector = CGVectorMake(location.x - player.position.x, location.y - player.position.y);
            float distance =[vectorSupport magnitude:rotationVector];
            rotationVector = [vectorSupport normalize:rotationVector];
            selfAngle = rotationVector;
            float rotationAngle = [vectorSupport convertVectorToRotation:selfAngle];// -atan2((double)selfAngle.dx, (double)selfAngle.dy);
            rotationAngle = [vectorSupport lerp:rotationAngle andPoint2:player.zRotation andWeight:.5];
            if (rotationAngle < (-2 * M_PI))
                rotationAngle = -2 * M_PI;
            if (rotationAngle > (2 * M_PI))
                rotationAngle = 2 * M_PI;
            player.zRotation = rotationAngle;// + M_PI/2;
            float effectiveSpeed = gPlayerWalkSpeed * (distance/gPlayerDeadSpace);
            CGVector temp = [vectorSupport scalarMultiply:rotationVector andScalar:effectiveSpeed];//   gPlayerWalkSpeed];
            deltaX = temp.dx + player.position.x;
            deltaY = temp.dy + player.position.y;
            if (distance < gPlayerDeadSpace){
                continue;
            }
        }
        player.lastPosition = player.position;
        CGPoint newPosition = CGPointMake(deltaX, deltaY);
        player.position = newPosition;
        touchPositionX = location.x;
        touchPositionY = location.y;
    }
   // coolDownTimer += frameTime;
    
    if (!inCoolDown){
        inCoolDown = YES;
        [self performSelector:@selector(resetCoolDown) withObject:nil afterDelay:gGunNormalCoolDown];
        coolDownTimer = 0;
        PlayerBullet *playerBullet;
        playerBullet = [[PlayerBullet alloc] init:rotationVector];
        playerBullet.position = player.position;
        [playerBullet setScale: 0.35];
        playerBullet.zRotation = player.zRotation;
        [scene addChild:playerBullet];
        numberOfBulletsFired++;
        [player playShootSoundFX];
    }
}
-(void)resetCoolDown{
    inCoolDown = NO;
}

-(void)touchesCancelled:(NSSet<UITouch *> *)touches{
    if (player.isHidden)
        return;
    coolDownTimer = gGunNormalCoolDown;
    currentTouch = nil;
    inCoolDown = NO;
    ignoreTouchesCollision = NO;
    [player stopPlayerTrailFX];
    //[playerTrailFX removeFromParent];
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches{
    coolDownTimer = gGunNormalCoolDown;
    ignoreTouchesCollision = NO;
    for (UITouch *t in touches)
        if (t != currentTouch)
            return;
    currentTouch = nil;
    //[playerTrailFX removeFromParent];
      [player stopPlayerTrailFX];
}

@end
