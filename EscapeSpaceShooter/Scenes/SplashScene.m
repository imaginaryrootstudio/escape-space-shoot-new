//
//  SplashScene.m
//  Escape: A Space Shooter
//
//  Created by William Birmingham on 1/23/19.
//  Copyright © 2019, 2020 Imaginary Root Studio LLC. All rights reserved.

#import "BasicRectangle.h"
#import "SplashScene.h"

@implementation SplashScene {
    Constants *constants;
}

-(void) didMoveToView:(SKView *)view{
    constants = [Constants getInstance];
    SKAction *runBlock = [SKAction runBlock:^{
        //GameSceneL0 *scene = (GameSceneL0 *)[SKScene nodeWithFileNamed:@"GameSceneL0"];
//        GameSceneL3 *scene = (GameSceneL3 *)[SKScene nodeWithFileNamed:@"GameSceneL3"];
        GameSceneL_Story_1 *scene = (GameSceneL_Story_1 *)[SKScene nodeWithFileNamed:@"GameSceneL_Story_1"];
       // GameScene *scene = (GameScene *)[SKScene nodeWithFileNamed:@"GameScene"];
        scene.scaleMode = SKSceneScaleModeAspectFill;
        SKView *skView = (SKView *)self.view;
        [skView presentScene:scene];
    }];
    SKAction *wait = [SKAction waitForDuration:1];
    SKAction *sequence = [SKAction sequence:@[wait, runBlock]];
    [self runAction:sequence];
}

-(void)dealloc{
}
@end
