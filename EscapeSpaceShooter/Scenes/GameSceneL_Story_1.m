//
//  GameSceneL_Story_1.m
//  Escape: A Space Shooter
// Copyright (c) 2020 Imaginary Root Studio LLC. All rights reserved.

#import "GameSceneL_Story_1.h"

@implementation GameSceneL_Story_1{
    
    Player* player;
    double rotation;
    CGVector selfAngle; //angle of the player
    double speedScale; //based on magnitude of selfAngle
    float dot;
    VectorSupport *vectorSupport;
    float touchPositionX, touchPositionY;
    NSMutableArray *players;
    NSInteger localPlayerIndex;
    Constants *constants;
    //Labels
    SKLabelNode *localPlayerScoreLabel;
    SKLabelNode *otherPlayerScoreLabel;
    SKLabelNode *host;
    SKLabelNode *playingTime;
    
    //Timing
    float timeSinceLastEnemySpawn;
    CFTimeInterval frameTime;
    NSTimeInterval lastUpdateTimeInterval;
    
    float timeInWave;
    
    //Lighting
    SKLightNode *light;
    
    //Misc
    BOOL sceneCreated;
    long numberOfEnemiesOnScreen;
    
    //used to prevent two touches from effecting player movement
    UITouch *currentTouch;
    
    SKTexture *screenCapture;
    
    SKSpriteNode *background;
    
    bool endGame;
    
    TapticEngine *enemyTaptic;
    TapticEngine *powerupTaptic;
    
    SKCameraNode *camera;
    
    WaypointNavigation *waypointNavigation;
    CameraController *cameraController;
    
    KeyObject *keyObject;
    
    PickupObject *pickupObject;
    
    //Game stat counters
    int enemiesHit;
    int numberOfBulletsFired;
    int numberOfTimesHit;
    
    //Emitter nodes
    SKEmitterNode *exitParticleFX;
    
    SKEmitterNode *emitter2;
    
    SKEmitterNode *backgroundFX1;
    SKEmitterNode *backgroundFX2;
}

-(void)didMoveToView:(SKView *)view {
    //[self changeScene];
    constants = [Constants getInstance];
    constants.currentScene = self;
    currentTouch = nil;
    
    self.physicsWorld.gravity = CGVectorMake(0, 0); //no gravity
    self.physicsWorld.contactDelegate = self;
    sceneCreated = NO;
    if (!sceneCreated){
        [self initializeScene];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(pauseGameScene)
                                                 name:@"PauseGameScene"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(resumeGameScene)
                                                 name:@"ResumeGameScene"
                                               object:nil];
}
- (void)pauseGameScene {
    if (self.view.scene) {
        self.view.paused = self.view.scene.paused = YES;
        //        [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
        UIView *v = (UIView*)self.view;
        v.userInteractionEnabled = YES;
    }
}
- (void)resumeGameScene {
    if (self.view.scene) {
        self.view.paused = self.view.scene.paused = NO;
        //        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        UIView *v = (UIView*)self.view;
        v.userInteractionEnabled = YES;
    }
}
- (void)initializeScene{
    sceneCreated = YES;
    self.backgroundColor = [UIColor blackColor];
    
    SKSpriteNode *p = (SKSpriteNode*)[self childNodeWithName:@"PlayerSpawn"];
    if (p == NULL)
        NSLog(@"GameSceneL2::init element not found PlayerSpawn");
    SKAction *block = [SKAction runBlock:^{
        PlayerBullet *playerBullet;
        playerBullet = [[PlayerBullet alloc] init:CGVectorMake(0,1)];
        playerBullet.position = p.position;
        [playerBullet setScale:1];
        playerBullet.zRotation = 0;
        playerBullet.zPosition = 1;
        [self addChild:playerBullet];
    }];
    SKAction *wait = [SKAction waitForDuration:0.9];
    SKAction *seq = [SKAction sequence:@[block, wait]];
    [self runAction:[SKAction repeatActionForever:seq]];

    SKSpriteNode *p1 = (SKSpriteNode*)[self childNodeWithName:@"EnemySpawn"];
    if (p1 == NULL)
        NSLog(@"GameSceneL2::init element not found PlayerSpawn");
    SKAction *block2 = [SKAction runBlock:^{
        EnemyBullet *eBullet;
        eBullet = [[EnemyBullet alloc] init:CGVectorMake(1,0)];
        eBullet.position = p1.position;
        [eBullet setScale:1];
        eBullet.zRotation = 0;
        eBullet.zPosition = 1;
        [self addChild:eBullet];
    }];
    SKAction *wait2 = [SKAction waitForDuration:1.1];
    SKAction *seq2 = [SKAction sequence:@[block2, wait2]];
    [self runAction:[SKAction repeatActionForever:seq2]];
    
    timeSinceLastEnemySpawn = 0.0f;
    lastUpdateTimeInterval = (float)0;
    frameTime = (float)0;
    
    camera = (SKCameraNode*)[self childNodeWithName:@"SceneCamera"];
    if (camera == NULL)
        NSLog(@"GameSceneL_Story_1::init element not found camera");
    
    //WaypointNavigation
    SKSpriteNode *c0 = (SKSpriteNode*)[self childNodeWithName:@"WayPoint0Camera"];
    if (c0 == NULL)
        NSLog(@"GameSceneL_Story_1::init element not found WayPoint10Camera");
    SKSpriteNode *c1 = (SKSpriteNode*)[self childNodeWithName:@"WayPoint1Camera"];
    if (c1 == NULL)
        NSLog(@"GameSceneL_Story_1::init element not found WayPoint1Camera");
    
    camera.position = c0.position;
    cameraController = [[CameraController alloc]init:camera andWaypoints:@[c0,c1] andPlayer:player ];
    cameraController.cameraScaleFlyover = CGVectorMake(2, 2);
    cameraController.cameraScaleFollowPlayer = CGVectorMake(0.85, 0.85);
    //TODO: Make speed normal for deployment
    [cameraController followWaypoints:NO andFlyOverSpeed: gTextFlyOverSpeed * 2.5];
\
    SKNode *p10 = (SKNode*) [self childNodeWithName:@"ExitSpawn"];
    NSString *fxTrail = [[NSBundle mainBundle] pathForResource:@"Objective" ofType:@"sks"];
    exitParticleFX = [NSKeyedUnarchiver unarchiveObjectWithFile:fxTrail];
    if (exitParticleFX == nil)
        NSLog(@"GameScene::initializedScene can't load player particle emitter");
    exitParticleFX.targetNode = self;
    exitParticleFX.name = @"exitParticleFX";
    exitParticleFX.position = p10.position;
    exitParticleFX.zPosition = 1;
    [self addChild:exitParticleFX];
    
    
    //check for waypoint nav to complete
    SKAction *blockWaypoint = [SKAction runBlock:^{
        if (self->cameraController.finishedWaypoints)
            [self changeScene];
    }];
    SKAction *waitWaypoint = [SKAction waitForDuration:0.5];
    SKAction *sequence = [SKAction sequence:@[blockWaypoint, waitWaypoint]];
    [self runAction:[SKAction repeatActionForever:sequence]];
}

-(void)changeScene{
    [self removeAllActions];
    SKAction *runBlock = [SKAction runBlock:^{
        InterstatialGameScene *s = (InterstatialGameScene *)[SKScene nodeWithFileNamed:@"InterstatialGameScene"];
        s.scaleMode = SKSceneScaleModeAspectFill;
        SKView *skView = (SKView *)self.view;
        s.numberOfBullets = 0;
        s.numberOfEnemiesHit = 0;
        s.numberOfPlayerHits = 0;
        s.nextLevel = @"GameSceneL0_0";
        s.showStats = NO;
        [skView presentScene:s];
    }];
    SKAction *wait = [SKAction waitForDuration:0.5];
    SKAction *sequence = [SKAction sequence:@[wait, runBlock]];
    [self runAction:sequence];
}
-(void)changeSceneToGameOver{
    sleep(1.1); //allow last sound fx to play
    screenCapture = [self.view textureFromNode:self];
    for(SKNode * child in self.scene.children)
        [child removeAllActions];
    [self.scene removeAllChildren];
    [self removeAllActions];
//    constants.timeInGame = 0;
//    constants.timeInWave = 0;
    //    SKTransition *reveal = [SKTransition revealWithDirection:SKTransitionDirectionLeft duration:1.0];
    timeInWave = 0;
    //    self.timeInGame = 0;
}

#pragma mark Update

-(void)update:(CFTimeInterval)currentTime {
    // Time stuff
    frameTime = currentTime - lastUpdateTimeInterval;
    lastUpdateTimeInterval = currentTime;
    if (frameTime > 1) { // more than a second since last update
        frameTime = 1.0 / 60.0;
        lastUpdateTimeInterval = currentTime;
    }
#pragma mark Camera_Player_Following
    //    self.timeInGame += frameTime;
    timeInWave += frameTime;
    //   endGame = NO;
    Player *losingPlayer;
    //Call the game object updates
//    if (!endGame){
//        for (Player *p in players){
//            [p update:frameTime];
//            // TODO: Make 20.0 60.0 for release
//            if (p.health <= 0 ){
//                endGame = YES;
//                losingPlayer = p;
//            }
//        }
//    }
//    if (endGame & constants.showGameOverScene){
//        [self changeSceneToGameOver];
//    }
}

#pragma mark dealloc
-(void)dealloc{
    //    constants.waveCounter = 0;
    //    constants.numberOfWaveCycles = 0;
    //    constants.numberOfWaves = 0;
    //    constants.timeInGame = 0;
    //    constants.timeInWave = 0;
    currentTouch = nil;
    [self removeAllChildren];
    [self removeAllActions];
    NSLog(@"GameSceneL_Story_1 dealloc");
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
