//
//  InterstatialGameScene.h
//  EscapeSpaceShooter
//
//  Created by William Birmingham on 2/24/20.
//  Copyright © 2020 William Birmingham. All rights reserved.
//

#ifndef InterstatialGameScene_h
#define InterstatialGameScene_h

#import <SpriteKit/SpriteKit.h>
#import <Foundation/Foundation.h>
#import "GameScene.h"
#import "GameScene.h"
#import "GameSceneL0.h"
#import "GameSceneL0_0.h"
#import "GameSceneL1.h"
#import "GameSceneL2.h"
#import "GameSceneL3.h"
#import "Constants.h"

@interface InterstatialGameScene:SKScene

@property int numberOfPlayerHits;
@property int numberOfBullets;
@property int numberOfEnemiesHit;
@property int score;
@property NSString *nextLevel;
@property bool showStats;
@end

#endif /* InterstatialGameScene_h */
