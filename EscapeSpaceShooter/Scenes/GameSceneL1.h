//
//  GameSceneL0.h
//  Escape: A Space Shooter
// Copyright (c) 2020 Imaginary Root Studio LLC. All rights reserved.


#import <SpriteKit/SpriteKit.h>
//#import "MultiplayerNetworking.h"
#import "Constants.h"
#import "TapticEngine.h"
#import "Player.h"
#import "VectorSupport.h"
#import "WaypointNavigation.h"
#import "CameraController.h"
#import "KeyObject.h"
#import "InterstatialGameScene.h"
#import "Bomb.h"
#import "SceneWall.h"
#import "CollisionResponse.h"
#import "TouchController.h"


//TODO: For multiplayer, add the following:
// MultiplayerNetworkingProtocol

@interface GameSceneL1 : SKScene <SKPhysicsContactDelegate, UIGestureRecognizerDelegate, CollisionResponseProtocol>

//@property /*(nonatomic, strong)*/ MultiplayerNetworking *networkEngine;
@property float timeInGame;

@end
