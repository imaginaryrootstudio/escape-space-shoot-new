//
//  GameScene.m
//  Escape: A Space Shooter
// Copyright (c) 2019, 2020 Imaginary Root Studio LLC. All rights reserved.

//Remember: we're in landscape, so X,Y coordinates for the screen are reversed.
#import "GameScene.h"
#import "Enemy.h"
#import "TargetedShootBase.h"
#import "RailShooter.h"
#import "TrackingShooter.h"
#import "SceneWall.h"
#import "RotatingShooter.h"

@implementation GameScene{
    Player* player;
    double rotation;
    CGVector selfAngle; //angle of the player
    double speedScale; //based on magnitude of selfAngle
    float dot;
    VectorSupport *vectorSupport;
    float touchPositionX, touchPositionY;
    NSMutableArray *players;
    NSInteger localPlayerIndex;
    Constants *constants;
    //Labels
    SKLabelNode *localPlayerScoreLabel;
    SKLabelNode *otherPlayerScoreLabel;
    SKLabelNode *host;
    SKLabelNode *playingTime;
    
    //Timing
    float timeSinceLastEnemySpawn;
    CFTimeInterval frameTime;
    NSTimeInterval lastUpdateTimeInterval;
    
    float timeInWave;
    
    //Lighting
    SKLightNode *light;
    
    //    ScreenWalls *screenWalls;
    
    //Misc
    BOOL sceneCreated;
    long numberOfEnemiesOnScreen;
    
    //used to prevent two touches from effecting player movement
    UITouch *currentTouch;
    
    //    //Lighting and Parallax
    //    MainSceneLights *sceneLighting;
    SKTexture *screenCapture;
    
    SKSpriteNode *background;
    
    bool endGame;
    
    TapticEngine *enemyTaptic;
    TapticEngine *powerupTaptic;
    
    Bullet *bullet;
    Enemy *enemyRailShooter;
    Enemy *enemyTurret1;
    Enemy *enemyTurret2;
    float coolDownTimer;
    bool inCoolDown;
    
    ShooterBase *shooterBase;
    TargetedShootBase *targetedShootBase;
    RailShooter *railShooter;
    RotatingShooter *rotatingShooter;
    //    TrackingShooter *trackingShooter;
    SceneWall *sceneWall;
    
    bool ignoreTouchesCollision;
    
    SKCameraNode *camera;
    
    WaypointNavigation *waypointNavigation;
    CameraController *cameraController;
    
    KeyObject *keyObject;
    
    //Game stat counters
    int enemiesHit;
    int numberOfBulletsFired;
    int numberOfTimesHit;
    
    //Emitter nodes
    SKEmitterNode *playerTrailFX;
    SKLabelNode *levelText;
    
    CollisionResponse *collisionResponse;
    
    StarfieldFX *starfieldFX;
}

-(void)didMoveToView:(SKView *)view {
    constants = [Constants getInstance];
    constants.currentScene = self;
    currentTouch = nil;
    
    
    //TODO: undo the following lines for multiplayer
    //    //Setup AuthenticationViewController
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showAuthenticationViewController)
    //                                                 name: PresentAuthenticationViewController object:nil];
    //    if (!constants.playerAuthenticated)
    //        [[GameKitHelper sharedGameKitHelper] authenticateLocalPlayer];
    
    self.physicsWorld.gravity = CGVectorMake(0, 0); //no gravity
    self.physicsWorld.contactDelegate = self;
    sceneCreated = NO;
    if (!sceneCreated){
        [self initializeScene];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pauseGameScene) name:@"PauseGameScene" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resumeGameScene) name:@"ResumeGameScene" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                              selector:@selector(removeText)
                                                  name:@"CameraFollowingPlayer"
                                                object:nil];
}
-(void)removeText{
    [levelText removeFromParent];
}
- (void)pauseGameScene {
    if (self.view.scene) {
        self.view.paused = self.view.scene.paused = YES;
        [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    }
}
- (void)resumeGameScene {
    if (self.view.scene) {
        self.view.paused = self.view.scene.paused = NO;
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    }
}
- (void)initializeScene{
    sceneCreated = YES;
    vectorSupport = [[VectorSupport alloc]init];
    rotation = 0;
    dot = 0;
    speedScale = 0;
    selfAngle = CGVectorMake(0, 1);         //start point to top of screen
    self.backgroundColor = [UIColor blackColor];
    self.timeInGame = 0;
    timeInWave = 0;

    //TODO: change this for multiplayer
    localPlayerIndex =  0;
    SKSpriteNode *p1 = (SKSpriteNode*)[self childNodeWithName:@"PlayerSpawn"];
    player = [[Player alloc] init:0 andInitialPosition:p1.position];
    player.name = @"Test_Player";
    //player.position = p1.position;
    [self addChild:player];
    
    //TODO: review following for multiplayer;
//    players = [NSMutableArray arrayWithCapacity:constants.numberOfPlayers];
    //TODO: Fix this for multiplayer
    //    for (int i = 0; i < constants.numberOfPlayers; i++){
    //        Player *p = [[Player alloc] init:localPlayerIndex];
    //        NSString *s = [NSString stringWithFormat:@"player%i",i];
    //        p.name = s;
    //        p.position = CGPointMake((1+i)*self.frame.size.width/(1+constants.numberOfPlayers), self.frame.size.height/2);
    //        [players addObject:p];
    //        [self addChild:p];
    //    }
    
    //TODO: remove the following after debugging
//    host = [SKLabelNode labelNodeWithFontNamed:gDefaultFontName];
//    host.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
//    host.fontColor = [UIColor whiteColor];
//    host.fontSize = 12;
//    host.zPosition = 0.6;
//    [self addChild:host];
    
//    timeSinceLastEnemySpawn = 0.0f;
    lastUpdateTimeInterval = (float)0;
    frameTime = (float)0;
    
    numberOfEnemiesOnScreen = 0;
    
    endGame = NO;
    
    coolDownTimer = 0;
    inCoolDown = NO;
#pragma mark Scene Set Up
    SKSpriteNode *sb1 = (SKSpriteNode*)[self childNodeWithName:@"WayPoint1"];
    SKSpriteNode *sb2 = (SKSpriteNode*)[self childNodeWithName:@"WayPoint2"];
    SKSpriteNode *sb3 = (SKSpriteNode*)[self childNodeWithName:@"WayPoint3"];
    SKSpriteNode *sb4 = (SKSpriteNode*)[self childNodeWithName:@"WayPoint4"];
    SKSpriteNode *rotatorBase = (SKSpriteNode*)[self childNodeWithName:@"RotatingShooter"];
    enemyTurret1 = [[Enemy alloc] init:sb1.position andDirection:CGVectorMake(2, 0)];
    enemyTurret2 = [[Enemy alloc] init:sb4.position andDirection:CGVectorMake(2, 0)];
    enemyRailShooter = [[Enemy alloc] init:sb3.position andDirection:CGVectorMake(1, 0)];
    
    Enemy *rotatingEnemy = [[Enemy alloc] init:sb1.position andDirection:CGVectorMake(1, 0)];
    rotatingEnemy.position = rotatorBase.position;
    rotatingEnemy.zRotation = rotatorBase.zRotation;
    
    ShooterBase* shooterBase1 = [[ShooterBase alloc]init:CGVectorMake(1, 0) andCoolDown:1.5 andBulletType: kSimpleBulletType];
    enemyTurret1.position = sb1.position;
    enemyTurret1.zRotation = sb1.zRotation;
    [enemyTurret1 addChild:shooterBase1];
    [self addChild:enemyTurret1];
    
    rotatingShooter = [[RotatingShooter alloc] init:0.25 andRotationRate:0.2];
    [rotatingEnemy addChild:rotatingShooter];
    [self addChild:rotatingEnemy];
    //    rotatingShooter.position = sb1.position;
    //    rotatingShooter.zRotation = sb1.zRotation;
    //    ShooterBase *b = [[ShooterBase alloc]init:CGVectorMake(1, 0) andCoolDown:1.25 andBulletType:kSimpleBulletType];
    //    b.position = sb1.position;
    //[rotatingShooter addChild:b];
    //    [self addChild:rotatingShooter];
    
    ShooterBase* shooterBase2 = [[ShooterBase alloc]init:CGVectorMake(1, 0) andCoolDown:1.5 andBulletType: kSimpleBulletType];
    enemyTurret2.position = sb4.position;
    enemyTurret2.zRotation = sb4.zRotation;
    [enemyTurret2 addChild:shooterBase2];
    
    [self addChild:enemyTurret2];
    
    //    targetedShootBase = [[TargetedShootBase alloc]init:CGVectorMake(0,1) andCoolDown:0.15 andPlayer:player];
    //    [enemy addChild:targetedShootBase];
    railShooter = [[RailShooter alloc]init:CGVectorMake(0, -1) andCoolDown:0.4 andPlayer:player];
    [railShooter loadWayPoints:[NSArray arrayWithObjects:sb2,sb3,nil]];
    [enemyRailShooter addChild:railShooter];
    [railShooter moveRailShooter];
    [self addChild:enemyRailShooter];
    //    trackingShooter = [[TrackingShooter alloc]init:CGVectorMake(0, -1) andCoolDown:0.3 andPlayer:player];
    //    [enemy addChild:trackingShooter];
    //    [trackingShooter moveTrackingShooter];
    sceneWall = [[SceneWall alloc]init];
    sceneWall.position = sb3.position;
    //    [self addChild:sceneWall];
    ignoreTouchesCollision = NO;
    //    [trackingShooter setMoveDirection:CGVectorMake(0, 0)];
    
    camera = (SKCameraNode*)[self childNodeWithName:@"SceneCamera"];
    
    //Set up walls
    SKSpriteNode *sw1 = (SKSpriteNode*)[self childNodeWithName:@"LeftWall"];
    SceneWall *s1 = [[SceneWall alloc]init];
    s1.position = sw1.position;
    s1.zRotation = sw1.zRotation;
    s1.xScale = sw1.xScale;
    s1.yScale = sw1.yScale;
    s1.name = @"LeftWall";
    [self addChild:s1];
    sw1 = (SKSpriteNode*)[self childNodeWithName:@"RightWall"];
    SceneWall *s2 = [[SceneWall alloc]init];
    s2.position = sw1.position;
    s2.zRotation = sw1.zRotation;
    s2.yScale = sw1.yScale;
    s2.xScale = sw1.xScale;
    s2.name = @"RightWall";
    [self addChild:s2];
    sw1 = (SKSpriteNode*)[self childNodeWithName:@"TopWall"];
    SceneWall *s3 = [[SceneWall alloc]init];
    s3.position = sw1.position;
    s3.zRotation = sw1.zRotation;
    s3.xScale = sw1.xScale;
    s3.yScale = sw1.yScale;
    s3.name = @"TopWall";
    [self addChild:s3];
    sw1 = (SKSpriteNode*)[self childNodeWithName:@"BottomWall"];
    SceneWall *s4 = [[SceneWall alloc]init];
    s4.position = sw1.position;
    s4.zRotation = sw1.zRotation;
    s4.xScale = sw1.xScale;
    s4.yScale = sw1.yScale;
    s4.name = @"BottomWall";
    [self addChild:s4];
    
    SKSpriteNode *playerWall = (SKSpriteNode*)[self childNodeWithName:@"PlayerWall"];
    SceneWall *pw = [[SceneWall alloc]init];
    pw.position = playerWall.position;
    pw.zRotation = playerWall.zRotation;
    pw.xScale = playerWall.xScale;
    pw.yScale = playerWall.yScale;
    pw.name = @"PlayerWall";
    [self addChild:pw];
    
    //WaypointNavigation
    SKSpriteNode *c1 = (SKSpriteNode*)[self childNodeWithName:@"WayPoint1Camera"];
    SKSpriteNode *c2 = (SKSpriteNode*)[self childNodeWithName:@"WayPoint2Camera"];
    camera.position = c1.position;
    //    waypointNavigation = [[WaypointNavigation alloc]init:camera andWaypoints:@[c1, c2]];
    cameraController = [[CameraController alloc]init:camera andWaypoints:@[c1,c2] andPlayer:player ];
//   [cameraController followWaypoints:YES andFlyOverSpeed:gSceneFlyOverSpeed];
    cameraController.cameraScaleFlyover = CGVectorMake(1.35, 1.35);
    cameraController.cameraScaleFollowPlayer = CGVectorMake(1, 1);
    [cameraController followWaypoints:YES andFlyOverSpeed:gSceneFlyOverSpeed];
    
    SKEmitterNode *emitter = (SKEmitterNode*)[self childNodeWithName:@"KeyParticles"];
    keyObject = [[KeyObject alloc]init:emitter andPosition:emitter.position];
    [self addChild:keyObject];
    //    Enemy *en = [[Enemy alloc]init:emitter.position andDirection:CGVectorMake(1, 0)];
    //    [self addChild:en];
    //    NSLog(@"Object %i %i", keyObject.physicsBody.categoryBitMask, keyObject.physicsBody.contactTestBitMask);
    
    // Stat counter initialization
    enemiesHit = 0;
    numberOfBulletsFired = 0;
    numberOfTimesHit = 0;
    
    //Emitter setup
    playerTrailFX = [[SKEmitterNode alloc]init];
     NSString *fxTrail = [[NSBundle mainBundle] pathForResource:@"PlayerTrail" ofType:@"sks"];
     playerTrailFX = [NSKeyedUnarchiver unarchiveObjectWithFile:fxTrail];
    if (playerTrailFX == nil)
         NSLog(@"GameScene::initializedScene can't load player particle emitter");
     playerTrailFX.targetNode = self.scene;
     playerTrailFX.name = @"playerTrailFX";
    levelText = (SKLabelNode*)[self childNodeWithName:@"LevelText"];
    
#pragma mark Collision Response
    collisionResponse = [[CollisionResponse alloc]init];
    collisionResponse.delegate = self;
    
#pragma mark Starfield
    starfieldFX = [[StarfieldFX alloc] init:self topWallPosition:s3.position bottomWallPosition: s4.position
                           leftWallPosition:s1.position rightWallPosition: s2.position];
    
}
#pragma mark CollisionResponse Protocol functions
-(void)reversePlayerPosition:(Player*)p{
    CGVector reverseDirection = CGVectorMake(p.position.x - p.lastPosition.x, p.position.y - p.lastPosition.y);
    reverseDirection = [vectorSupport normalize:reverseDirection];
    reverseDirection = [vectorSupport scalarMultiply:reverseDirection andScalar:-20];
    reverseDirection = [vectorSupport add:CGVectorMake(p.position.x, p.position.y) andVector2:reverseDirection];
    [player removeFromParent];
    player.position = CGPointMake(reverseDirection.dx, reverseDirection.dy);
    [self addChild:player];
    ignoreTouchesCollision = YES;
}

-(void)changeScene{
    SKAction *runBlock = [SKAction runBlock:^{
        InterstatialGameScene *s = (InterstatialGameScene *)[SKScene nodeWithFileNamed:@"InterstatialGameScene"];
        s.scaleMode = SKSceneScaleModeAspectFill;
        SKView *skView = (SKView *)self.view;
        s.numberOfBullets = self->numberOfBulletsFired;
        s.numberOfEnemiesHit = self->enemiesHit;
        s.numberOfPlayerHits = self->numberOfTimesHit;
        s.showStats = YES;
        s.nextLevel = @"GameScene";
        [skView presentScene:s];
    }];
    SKAction *wait = [SKAction waitForDuration:0.5];
    SKAction *sequence = [SKAction sequence:@[wait, runBlock]];
    [self runAction:sequence];
}

-(void)updateLevelStat:(NSString*)statName andValue:(float)value{
    if ([statName isEqualToString:@"numberOfTimesHit"]){
        numberOfTimesHit += (int)value;
        return;
    }
    if ([statName isEqualToString:@"enemiesHit"]){
        enemiesHit += value;
        return;
    }
    else
        NSLog(@"GameSceneL2::updateLevelStat stat not found %@", statName);
}

-(void)processPickup:(SKNode *)pickup{
    //not needed for this game
    return;
}


#pragma mark Touch Processing
//-(void)tapGesture{
//    [playerTrailFX removeFromParent];
//    currentTouch = nil;
////    NSLog(@"Tap gesture");
//}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    SKNode *node = [self nodeAtPoint:location];
    //    if (inTouch)
    //        return;
    //    inTouch = YES;
    if (currentTouch != nil)
        return;
    if (endGame)
        return;
    if (player.isHidden)
        return;
    //    NSLog(@"Touches began Number of touches %lu", (unsigned long)touches.count);
    for (UITouch *touch in touches) {
        CGPoint location = [touch locationInNode:self];
        touchPositionX = [touch locationInNode:self].x;
        touchPositionY = [touch locationInNode:self].y;
        currentTouch = touch;
    }
    ignoreTouchesCollision = NO;
    [player addChild:playerTrailFX];
    //    selfAngle = CGVectorMake(touchPositionX - self.position.x, touchPositionY - self.position.y);
    //    [playerTrailFX removeFromParent];
    //    [players[localPlayerIndex] addChild:playerTrailFX];
    //    NSLog(@"Node parent name %@", node.parent.name);
    //    if ([node.parent.name isEqualToString:@"player0"]){
    //        constants.fingerOnPlayer = YES;
    //        constants.numberOfTimesFingerIsOnPlayer++;
    //    } else {
    //        constants.numberOfTimesFingerIsOffPlayer++;
    //    }
}

-(void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event  {
    if (ignoreTouchesCollision){
        return;
    }
    if (endGame)
        return;
    if (player.isHidden)
        return;
    float deltaX = 0;
    float deltaY = 0;
    //    Player *p = ((Player*)players[localPlayerIndex]);
    int iPhoneNotchFactorHorizontal = 0;
    int iPhoneNotchFactorVertical = 0;
    if (constants.isiPhoneX){
        iPhoneNotchFactorHorizontal = 60;
        iPhoneNotchFactorVertical = 78;
    }
    //    NSLog(@"Touches moved Number of touches %lu", (unsigned long)touches.count);
    CGVector rotationVector = CGVectorMake(0, 0);
    for (UITouch *t in touches){
        if (t != currentTouch)
            continue;
        CGPoint location = [t locationInNode:self];
        if (touchPositionX != 0 && touchPositionY != 0){
            /*CGVector*/ rotationVector = CGVectorMake(location.x - player.position.x, location.y - player.position.y);
            float distance =[vectorSupport magnitude:rotationVector];
            rotationVector = [vectorSupport normalize:rotationVector];
            selfAngle = rotationVector;
            float rotationAngle = [vectorSupport convertVectorToRotation:selfAngle];// -atan2((double)selfAngle.dx, (double)selfAngle.dy);
            rotationAngle = [vectorSupport lerp:rotationAngle andPoint2:player.zRotation andWeight:.5];
            if (rotationAngle < (-2 * M_PI))
                rotationAngle = -2 * M_PI;
            if (rotationAngle > (2 * M_PI))
                rotationAngle = 2 * M_PI;
            player.zRotation = rotationAngle;// + M_PI/2;
            float effectiveSpeed = gPlayerWalkSpeed * (distance/gPlayerDeadSpace);
            CGVector temp = [vectorSupport scalarMultiply:rotationVector andScalar:effectiveSpeed];//   gPlayerWalkSpeed];
            deltaX = temp.dx + player.position.x;
            deltaY = temp.dy + player.position.y;
            if (distance < gPlayerDeadSpace){
                continue;
            }
            //            coolDownTimer += frameTime;
            //            if (!inCoolDown){ //if (coolDownTimer > gGunNormalCoolDown){
            //                inCoolDown = YES;
            //                SKAction *waitCoolDown = [SKAction waitForDuration:gGunNormalCoolDown];
            //                SKAction *b = [SKAction runBlock:^{
            //                    self->inCoolDown = NO;
            //                }];
            //                SKAction *shootSequence = [SKAction sequence:@[waitCoolDown, b]];
            //                [self runAction:shootSequence];
            //
            //                coolDownTimer = 0;
            //
            //                bullet = [[Bullet alloc]init: rotationVector]; //selfAngle];
            //                bullet.position = player.position;// CGPointMake(self.frame.size.width/3, self.frame.size.height/2);
            //                [self addChild:bullet];
            //            }
        }
        player.lastPosition = player.position;
        CGPoint newPosition = CGPointMake(deltaX, deltaY);
        player.position = newPosition;
        touchPositionX = location.x;
        touchPositionY = location.y;
    }
    coolDownTimer += frameTime;
    
    if (!inCoolDown){ //if (coolDownTimer > gGunNormalCoolDown){
        inCoolDown = YES;
        [self performSelector:@selector(resetCoolDown) withObject:nil afterDelay:gGunNormalCoolDown];
        coolDownTimer = 0;
        PlayerBullet *playerBullet;
        playerBullet = [[PlayerBullet alloc] init:rotationVector];
        playerBullet.position = player.position;
        [playerBullet setScale: 0.35];
        playerBullet.zRotation = player.zRotation;
        [self addChild:playerBullet];
        numberOfBulletsFired++;
        [player playShootSoundFX];
    }
}
-(void)resetCoolDown{
    inCoolDown = NO;
    //    NSLog(@"Cooldown");
}
-(void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    if (player.isHidden)
        return;
    coolDownTimer = gGunNormalCoolDown;
    //    for (UITouch *t in touches)
    //        NSLog(@"Touches cancelled: %@", t);
    //    NSLog(@"Touches cancelled");: current touch %@ number of touchs %lu", currentTouch, (unsigned long)touches.count);
    //    NSLog(@"Touches cancelled: number of touches %lu", (unsigned long)touches.count);
    
    //    [playerTrailFX removeFromParent];
    currentTouch = nil;
    inCoolDown = NO;
    ignoreTouchesCollision = NO;
    [playerTrailFX removeFromParent];
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    coolDownTimer = gGunNormalCoolDown;
    ignoreTouchesCollision = NO;
    for (UITouch *t in touches)
        if (t != currentTouch)
            return;
    //    [playerTrailFX removeFromParent];
    // inTouch = NO;
    currentTouch = nil;
    [playerTrailFX removeFromParent];
}

-(void)changeSceneToGameOver{
    sleep(1.1); //allow last sound fx to play
    //IAP Support: Update the number of power ups collected this round;
    //    [iapSupport setNumberOfCollectedPowerUps];
    //    [gameTimeLabel removeFromScene];
    screenCapture = [self.view textureFromNode:self];
    //    [gameTimeLabel setLabelText:@" "];
    for(SKNode * child in self.scene.children)
        [child removeAllActions];
    [self.scene removeAllChildren];
    [self removeAllActions];
    //[self.scene rem   oveAllActions];
    //    playerTrailFX.targetNode = nil;
    //collisionFX.targetNode = nil;
    //    sceneLighting = nil;
    //    //    jsonSendRec = nil;
    //    gameTimeLabel = nil;
    //    powerUpManager = nil;
    //    enemyManager = nil;
    //    screenWalls = nil;
//    constants.timeInGame = 0;
//    constants.timeInWave = 0;
    //    gameLogic = nil;
    //self.networkEngine.gameState = kGameStateGameOver;
    SKTransition *reveal = [SKTransition revealWithDirection:SKTransitionDirectionLeft duration:1.0];
    //    GameOver *scene = [GameOver nodeWithFileNamed:@"GameOver"];
    //    GameOver *scene = [[GameOver alloc] initWithSize:self.size];
    //    scene.scaleMode = SKSceneScaleModeAspectFill;
    //    scene.time = (int64_t)self.timeInGame;
    //    scene.screenCapture = screenCapture;
    timeInWave = 0;
    self.timeInGame = 0;
    //    [self.view presentScene:scene transition: reveal];
}

#pragma mark Update

-(void)update:(CFTimeInterval)currentTime {
    // Time stuff
    frameTime = currentTime - lastUpdateTimeInterval;
    lastUpdateTimeInterval = currentTime;
    if (frameTime > 1) { // more than a second since last update
        frameTime = 1.0 / 60.0;
        lastUpdateTimeInterval = currentTime;
    }
#pragma mark Camera_Player_Following
    //    CGPoint t = CGPointMake([vectorSupport lerp:camera.position.x andPoint2:player.position.x andWeight:0.05],
    //                            [vectorSupport lerp:camera.position.y andPoint2:player.position.y andWeight:0.05]);
    //    camera.position = t;
    //    [bullet update:frameTime];
    self.timeInGame += frameTime;
    timeInWave += frameTime;
    //   endGame = NO;
    Player *losingPlayer;
    //Call the game object updates
    if (!endGame){
        for (Player *p in players){
            [p update:frameTime];
            // TODO: Make 20.0 60.0 for release
            if (p.health <= 0 ){
                endGame = YES;
                losingPlayer = p;
                //                [p playDieAnimation];
                //[self changeSceneToGameOver];
            }
        }
    }
    
    //    if (!endGame){
    //        [gameLogic update:frameTime];
    //        movementType = gameLogic.movementType;
    //        [enemyManager update:frameTime];
    //        [powerUpManager update:frameTime];
    //        [gameTimeLabel updateLabelTime:self.timeInGame]; //constants.timeInGame];
    //        [sceneLighting update:frameTime];
    //    } else {
    //        [enemyManager stopAllEnemies];
    //        [powerUpManager stopAllPowerUps];
    //        Player *p = ((Player*)players[localPlayerIndex]);
    //    }
    
    if (endGame & constants.showGameOverScene){
        //        GameOver* gameOver = [[GameOver alloc] init];
        //        gameOver.time = self.timeInGame;
        //  Retrieve the score from the leaderboard here, allowing time for network ops before the game over scene
        //  [LeaderBoardManager getLocalPlayerScoreValueFromLeaderBoard] is called in GameOver scene just before checking for new
        //  high score
        //        [lbManager retrieveLocalPlayerScoreFromLeaderBoard];
        [self changeSceneToGameOver];
    }
}

#pragma mark Physics Support Functions

-(void)removeCollisionFX{
    [self removeFromParent];
}

//TODO:Add FXs for other powerup types

-(void)collisionActionFX:(PowerUpType)powerUpType{
    NSString *fileName = [[NSString alloc]init];
    //    switch (powerUpType) {
    //        case kGreenPowerUp:
    //            fileName = @"CollisionPlayerAndGreen";
    //            break;
    //        case kNone:
    //            fileName = @"Collision";
    //            //NSLog(@"CollisionFX");
    //            break;
    //        default:
    //            fileName = @"";
    //            break;
    //    }
    //    SKAction *wait = [SKAction waitForDuration:0.8];
    //    SKAction *rem = [SKAction removeFromParent];
    //
    //    //SKAction *r = [SKAction performSelector:@selector(removeCollisionFX) onTarget:self];
    //    SKAction *add = [SKAction runBlock:^{
    //        SKEmitterNode *collisionFX = [[SKEmitterNode alloc]init];
    //        NSString *ct = [[NSBundle mainBundle] pathForResource:fileName /* @"Collision"*/ ofType:@"sks"];
    //        collisionFX = [NSKeyedUnarchiver unarchiveObjectWithFile:ct];
    //        if (collisionFX == nil)
    //            NSLog(@"GameScene::initializedScene can't load collision particle emitter");
    //        collisionFX.targetNode = self.scene;
    //        collisionFX.name = [fileName stringByAppendingString: @"collisionFX"];
    //        SKAction *seq = [SKAction sequence:@[/*add,*/wait,rem]];
    //        collisionFX.position = ((Player*)players[localPlayerIndex]).position;
    //        [collisionFX runAction:seq];
    //        [self addChild:collisionFX];
    //        //MainSceneLights *light =[[MainSceneLights alloc]init:self andPlayerSprite:players[localPlayerIndex]];
    //    }];
    //    [self runAction:add];
}

//-(void)playPowerUpSoundFX{
//    if (!constants.soundFxEnabled)
//        return;
//    SKAction *powerupSoundfx = [SKAction playSoundFileNamed:@"powerupSoundFX.wav" waitForCompletion:YES];
//    [self runAction:powerupSoundfx];
//}
//
//-(void)playGreenPowerUpSoundFX{
//    if (!constants.soundFxEnabled)
//        return;
//    SKAction *powerupSoundfx = [SKAction playSoundFileNamed:@"powerupSoundFX.wav" waitForCompletion:YES];
//    [self runAction:powerupSoundfx];
//}
//-(void)playYellowPowerUpSoundFX{
//    if (!constants.soundFxEnabled)
//        return;
//    SKAction *powerupSoundfx = [SKAction playSoundFileNamed:@"powerupSoundFX.wav" waitForCompletion:YES];   //@"enemy.wav" waitForCompletion:YES];
//    [self runAction:powerupSoundfx];
//}
//
//-(void)playEnemySoundFX{
//    if (!constants.soundFxEnabled)
//        return;
//    //    SKAction *enemySoundfx = [SKAction playSoundFileNamed:@"enemy.wav" waitForCompletion:NO];
//    SKAction *enemySoundfx = [SKAction playSoundFileNamed:@"NewDamageSound.wav" waitForCompletion:NO];
//    [self runAction:enemySoundfx];
//}


//  Collision categories
//
//self.noCollisionCategory = 0x1 << 0; // 0x0;
//self.playerCollisionCategory =0x1 << 1;// 0x1;
//self.bulletCollisionCategory = 0x1 << 2; //0x4;
//self.enemyCollisionCategory = 0x1 << 3;// 0x8;
//self.screenEdgeCollisionCategory = 0x1 << 4; //0x10;
//self.enemyBulletCollisionCategory = 0x1 << 5;// 0x20;
//self.sceneWallCollisionCategory = 0x1 << 6;// 64;//E 0x30;
//self.playerBulletCollisionCategory = 0x1 << 7;
//self.keyCollisionCategory = 0x1 << 8;

#pragma mark Contact
-(void)didBeginContact:(SKPhysicsContact *)contact{
    
    [collisionResponse response:contact];
//    SKPhysicsBody *obj1, *obj2;
//    
//    //    if (self.networkEngine.gameState != kGameStateActive)
//    //        return;
//    
//    if (contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask){
//        obj1 = contact.bodyA;
//        obj2 = contact.bodyB;
//    }
//    else{
//        obj1 = contact.bodyB;
//        obj2 = contact.bodyA;
//    }
//    
//    if ((obj1.categoryBitMask & constants.playerCollisionCategory) != 0 &&
//        (obj2.categoryBitMask & constants.enemyCollisionCategory) != 0){
//        Player *p = (Player*)obj1.node;
//        [p playerTakeDamage:1];
//        [self reversePlayerPosition:p];
//    }
//    
//    if ((obj1.categoryBitMask & constants.bulletCollisionCategory) != 0 &&
//        (obj2.categoryBitMask & constants.screenEdgeCollisionCategory) != 0){
//        Bullet *e = (Bullet*)obj1.node;
//        [e removeAllActions];
//        [e removeFromParent];
//    }
//    
//    //    if ((obj1.categoryBitMask & constants.bulletCollisionCategory) != 0 &&
//    //        (obj2.categoryBitMask & constants.enemyCollisionCategory) != 0){
//    if ((obj1.categoryBitMask & constants.enemyCollisionCategory) != 0 &&
//        (obj2.categoryBitMask & constants.playerBulletCollisionCategory) != 0){
//        Bullet *b = (Bullet*)obj2.node;
//        [b remove];
//        Enemy *e = (Enemy*)obj1.node;
//        ShooterBase *sb = (ShooterBase*)[e childNodeWithName:@"shooterBase"];
//        [sb takeDamage:1.0 andStopOnZeroHealth:NO];
//        enemiesHit++;
//    }
//    
//    if ((obj1.categoryBitMask & constants.playerCollisionCategory) != 0 &&
//        (obj2.categoryBitMask & constants.bulletCollisionCategory) != 0){
//        //        NSLog(@"Bullet hit player");
//        Bullet *e = (Bullet*)obj1.node;
//        [e removeAllActions];
//        [e removeFromParent];
//    }
//    
//    if ((obj1.categoryBitMask & constants.playerCollisionCategory) != 0 &&
//        (obj2.categoryBitMask & constants.enemyBulletCollisionCategory) != 0){
//        //        NSLog(@"Bullet hit player");
//        EnemyBullet *e = (EnemyBullet*)obj2.node;
//        int32_t n = e.damage;
//        [e removeAllActions];
//        [e removeFromParent];
//        Player *p = (Player*)obj1.node;
//        [p playerTakeDamage:n];
//        numberOfTimesHit++;
//    }
//    
//    if ((obj1.categoryBitMask & constants.enemyBulletCollisionCategory) != 0 &&
//        (obj2.categoryBitMask & constants.sceneWallCollisionCategory ) != 0){
//        //                NSLog(@"Enemy Bullet hit wall");
//        EnemyBullet *e = (EnemyBullet*)obj1.node;
//        [e removeAllActions];
//        [e removeFromParent];
//    }
//    if ((obj1.categoryBitMask & constants.enemyBulletCollisionCategory) != 0 &&
//         (obj2.categoryBitMask & constants.playerBulletCollisionCategory ) != 0){
//         EnemyBullet *e = (EnemyBullet*)obj1.node;
//         [e removeAllActions];
//         [e removeFromParent];
//        PlayerBullet *b = (PlayerBullet*)obj2.node;
//        [b removeAllActions];
//        [b removeFromParent];
//     }
//    if ((obj1.categoryBitMask & constants.bulletCollisionCategory) != 0 &&
//        (obj2.categoryBitMask & constants.sceneWallCollisionCategory ) != 0){
//        //                NSLog(@"Bullet hit wall");
//        Bullet *e = (Bullet*)obj1.node;
//        [e removeAllActions];
//        [e removeFromParent];
//    }
//    
//    if ((obj1.categoryBitMask & constants.sceneWallCollisionCategory ) != 0 &&
//        (obj2.categoryBitMask & constants.playerBulletCollisionCategory)){
//        Bullet *e = (Bullet*)obj2.node;
//        [e removeAllActions];
//        [e removeFromParent];
//    }
//    
//    if ((obj1.categoryBitMask & constants.playerCollisionCategory) != 0 &&
//        (obj2.categoryBitMask & constants.sceneWallCollisionCategory ) != 0){
//        
//        Player* p = (Player*)obj1.node;
//        SceneWall *w = (SceneWall*)obj2.node;
//        [self reversePlayerPosition:p];
//        //        CGVector reverseDirection = CGVectorMake(p.position.x - p.lastPosition.x, p.position.y - p.lastPosition.y);
//        //        reverseDirection = [vectorSupport normalize:reverseDirection];
//        //        reverseDirection = [vectorSupport scalarMultiply:reverseDirection andScalar:-20];
//        //        reverseDirection = [vectorSupport add:CGVectorMake(p.position.x, p.position.y) andVector2:reverseDirection];
//        //        [player removeFromParent];
//        //        player.position = CGPointMake(reverseDirection.dx, reverseDirection.dy);
//        //        [self addChild:player];
//        //        ignoreTouchesCollision = YES;
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"RemoveBullet" object:self];
//    }
//    
//    if ((obj1.categoryBitMask & constants.enemyCollisionCategory) != 0 &&
//        (obj2.categoryBitMask & constants.sceneWallCollisionCategory ) != 0){
//        Enemy *e = (Enemy*)obj1.node;
//        coolDownTimer = gGunNormalCoolDown;
//    }
//    
//    if ((obj1.categoryBitMask & constants.playerCollisionCategory) != 0 &&
//        (obj2.categoryBitMask & constants.keyCollisionCategory ) != 0){
//        [self changeScene];
//    }
//    if ((obj1.categoryBitMask & constants.playerBulletCollisionCategory) != 0 &&
//        (obj2.categoryBitMask & constants.keyCollisionCategory ) != 0){
//       Bullet *e = (Bullet*)obj1.node;
//       [e removeAllActions];
//       [e removeFromParent];
//
//    }
}

- (void)didLoadReferenceNode:(SKNode *)node{
    NSLog(@"Loaded");
}

//-(void)reversePlayerPosition:(Player*)p{
//    CGVector reverseDirection = CGVectorMake(p.position.x - p.lastPosition.x, p.position.y - p.lastPosition.y);
//    reverseDirection = [vectorSupport normalize:reverseDirection];
//    reverseDirection = [vectorSupport scalarMultiply:reverseDirection andScalar:-20];
//    reverseDirection = [vectorSupport add:CGVectorMake(p.position.x, p.position.y) andVector2:reverseDirection];
//    [player removeFromParent];
//    player.position = CGPointMake(reverseDirection.dx, reverseDirection.dy);
//    [self addChild:player];
//    ignoreTouchesCollision = YES;
//}
//
//-(void)changeScene{
//    SKAction *runBlock = [SKAction runBlock:^{
//        InterstatialGameScene *s = (InterstatialGameScene *)[SKScene nodeWithFileNamed:@"InterstatialGameScene"];
//        s.scaleMode = SKSceneScaleModeAspectFill;
//        SKView *skView = (SKView *)self.view;
//        s.numberOfBullets = self->numberOfBulletsFired;
//        s.numberOfEnemiesHit = self->enemiesHit;
//        s.numberOfPlayerHits = self->numberOfTimesHit;
//        [skView presentScene:s];
//    }];
//    SKAction *wait = [SKAction waitForDuration:0.5];
//    SKAction *sequence = [SKAction sequence:@[wait, runBlock]];
//    [self runAction:sequence];
//}


-(void)dealloc{
//    constants.waveCounter = 0;
//    constants.numberOfWaveCycles = 0;
//    constants.numberOfWaves = 0;
//    constants.timeInGame = 0;
//    constants.timeInWave = 0;
    currentTouch = nil;
    [self removeAllChildren];
    [self removeAllActions];
    NSLog(@"Game Scene dealloc");
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    //    [self.view removeGestureRecognizer:tapGestureRecognizer];
    //    NSLog(@"Game scene dealloc");
}
@end
