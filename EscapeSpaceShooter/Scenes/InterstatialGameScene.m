//
//  InterstatialGameScene.m
//  EscapeSpaceShooter
//
//  Created by William Birmingham on 2/24/20.
//  Copyright © 2020 William Birmingham. All rights reserved.
//

#import "InterstatialGameScene.h"

@implementation InterstatialGameScene {
    SKCameraNode *camera;
    Constants *constants;
}

-(void) didMoveToView:(SKView *)view{
    constants = [Constants getInstance];
    camera = (SKCameraNode*)[self childNodeWithName:@"SceneCamera"];
    [camera setScale:1.5];
    SKLabelNode *enemyHits = (SKLabelNode*) [self childNodeWithName:@"NumberTimesHitLabel"];
    enemyHits.text = [NSString stringWithFormat:@"Times hit: %i", self.numberOfPlayerHits];
    SKLabelNode *bulletsFired = (SKLabelNode*) [self childNodeWithName:@"BulletsFiredLabel"];
    bulletsFired.text = [NSString stringWithFormat:@"Bullets fired: %i", constants.bulletsFired]; //self.numberOfBullets];
    SKLabelNode *enemiesHit = (SKLabelNode*) [self childNodeWithName:@"EnemiesHitLabel"];
    enemiesHit.text = [NSString stringWithFormat:@"Enemies hit: %i", self.numberOfEnemiesHit];
    SKLabelNode *score = (SKLabelNode*) [self childNodeWithName:@"ScoreLabel"];
    score.text =[NSString stringWithFormat:@"Score: %i", constants.score];// @"Score: Not implemented";
    SKLabelNode *scoreV2 = (SKLabelNode*) [self childNodeWithName:@"NewScoreLabel"];
    scoreV2.text = [NSString stringWithFormat:@"Score: %i", constants.score];
    if (!self.showStats){
        [enemiesHit removeFromParent];
        [bulletsFired removeFromParent];
        [score removeFromParent];
        [enemyHits removeFromParent];
    }
    
    SKAction *runBlock = [SKAction runBlock:^{
        SKScene *scene;
       // self.nextLevel = @"GameSceneL3";
        if ([self.nextLevel isEqualToString:@"GameSceneL0"]){
            scene = (GameSceneL0 *)[SKScene nodeWithFileNamed:self.nextLevel];
        }
        else if ([self.nextLevel isEqualToString:@"GameSceneL0_0"]){
            scene = (GameSceneL0_0 *)[SKScene nodeWithFileNamed:self.nextLevel];
        }
        else if ([self.nextLevel isEqualToString:@"GameSceneL1"]){
            scene = (GameSceneL1 *)[SKScene nodeWithFileNamed:self.nextLevel];
        }
        else if ([self.nextLevel isEqualToString:@"GameSceneL2"]){
            scene = (GameSceneL2*)[SKScene nodeWithFileNamed:self.nextLevel];
        }
        else if ([self.nextLevel isEqualToString:@"GameSceneL3"]){
            scene = (GameSceneL3*)[SKScene nodeWithFileNamed:self.nextLevel];
        }
        else if ([self.nextLevel isEqualToString:@"GameScene"]){
            scene = (GameScene *)[SKScene nodeWithFileNamed:self.nextLevel];
        }
        else
            NSLog(@"InterstatialGameScene::didMoveToView scene %@ not found", self.nextLevel);
        scene.scaleMode = SKSceneScaleModeAspectFill;
        SKView *skView = (SKView *)self.view;
        [skView presentScene:scene];
    }];
    
    SKAction *wait = [SKAction waitForDuration:2.5];
    SKAction *sequence = [SKAction sequence:@[wait, runBlock]];
    [self runAction:sequence];
}

-(void)dealloc{
    NSLog(@"Interstatial");
}
@end

