//
//  GameSceneL0.m
//  Escape: A Space Shooter
// Copyright (c) 2020 Imaginary Root Studio LLC. All rights reserved.

#import "GameSceneL1.h"
//#import "Enemy.h"
//#import "TargetedShootBase.h"
//#import "RailShooter.h"
//#import "TrackingShooter.h"
//#import "SceneWall.h"
//#import "RotatingShooter.h"


@implementation GameSceneL1{
    Player* player;
    double rotation;
    CGVector selfAngle; //angle of the player
    double speedScale; //based on magnitude of selfAngle
    float dot;
    VectorSupport *vectorSupport;
    float touchPositionX, touchPositionY;
    NSMutableArray *players;
    NSInteger localPlayerIndex;
    Constants *constants;
    //Labels
    SKLabelNode *localPlayerScoreLabel;
    SKLabelNode *otherPlayerScoreLabel;
    SKLabelNode *host;
    SKLabelNode *playingTime;
    
    //Timing
    float timeSinceLastEnemySpawn;
    CFTimeInterval frameTime;
    NSTimeInterval lastUpdateTimeInterval;
    
    float timeInWave;
    
    //Lighting
    SKLightNode *light;
    
    //    ScreenWalls *screenWalls;
    
    //Misc
    BOOL sceneCreated;
    long numberOfEnemiesOnScreen;
    
    //used to prevent two touches from effecting player movement
    UITouch *currentTouch;
    
    //    //Lighting and Parallax
    //    MainSceneLights *sceneLighting;
    SKTexture *screenCapture;
    
    SKSpriteNode *background;
    
    bool endGame;
    
    TapticEngine *enemyTaptic;
    TapticEngine *powerupTaptic;
    
    float coolDownTimer;
    bool inCoolDown;
    
    ShooterBase *shooterBase;
    bool ignoreTouchesCollision;
    SKCameraNode *camera;
    
    WaypointNavigation *waypointNavigation;
    CameraController *cameraController;
    
    KeyObject *keyObject;
    
    //Game stat counters
    int enemiesHit;
    int numberOfBulletsFired;
    int numberOfTimesHit;
    
    //Emitter nodes
    SKEmitterNode *playerTrailFX;
    
    CollisionResponse* collisionResponse;
    TouchController *touchController;
    
    SKLabelNode *levelText;
}

-(void)didMoveToView:(SKView *)view {
    constants = [Constants getInstance];
    constants.currentScene = self;
    currentTouch = nil;
    //TODO: undo the following lines for multiplayer
    //    //Setup AuthenticationViewController
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showAuthenticationViewController)
    //                                                 name: PresentAuthenticationViewController object:nil];
    //    if (!constants.playerAuthenticated)
    //        [[GameKitHelper sharedGameKitHelper] authenticateLocalPlayer];
    
    self.physicsWorld.gravity = CGVectorMake(0, 0); //no gravity
    self.physicsWorld.contactDelegate = self;
    sceneCreated = NO;
    if (!sceneCreated){
        [self initializeScene];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pauseGameScene) name:@"PauseGameScene" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resumeGameScene) name:@"ResumeGameScene" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                              selector:@selector(removeText)
                                                  name:@"CameraFollowingPlayer"
                                                object:nil];
}
-(void)removeText{
    [levelText removeFromParent];
}

- (void)pauseGameScene {
    if (self.view.scene) {
        self.view.paused = self.view.scene.paused = YES;
        [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    }
}
- (void)resumeGameScene {
    if (self.view.scene) {
        self.view.paused = self.view.scene.paused = NO;
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    }
}
- (void)initializeScene{
    sceneCreated = YES;
    vectorSupport = [[VectorSupport alloc]init];
    rotation = 0;
    dot = 0;
    speedScale = 0;
    selfAngle = CGVectorMake(0, 1);         //start point to top of screen
    self.backgroundColor = [UIColor blackColor];
    self.timeInGame = 0;
    timeInWave = 0;
    
    //TODO: change this for multiplayer
    localPlayerIndex =  0;
    SKSpriteNode *p1 = (SKSpriteNode*)[self childNodeWithName:@"PlayerSpawn"];
    player = [[Player alloc] init:0 andInitialPosition:p1.position];
    player.name = @"Player";
    //player.position = p1.position;
    [self addChild:player];
#pragma mark camera setup
    
    ignoreTouchesCollision = NO;
    camera = (SKCameraNode*)[self childNodeWithName:@"SceneCamera"];
    
    SKSpriteNode *c1 = (SKSpriteNode*)[self childNodeWithName:@"WayPoint1Camera"];
    SKSpriteNode *c2 = (SKSpriteNode*)[self childNodeWithName:@"WayPoint2Camera"];
    camera.position = c1.position;
    cameraController = [[CameraController alloc]init:camera andWaypoints:@[c1,c2] andPlayer:player ];
    cameraController.cameraScaleFlyover = CGVectorMake(1.1, 1.1);
    cameraController.cameraScaleFollowPlayer = CGVectorMake(0.85, 0.85);
    [cameraController followWaypoints:YES andFlyOverSpeed:gSceneFlyOverSpeed];
    
    SKEmitterNode *emitter = (SKEmitterNode*)[self childNodeWithName:@"KeyParticles"];
    keyObject = [[KeyObject alloc]init:emitter andPosition:emitter.position];
    [self addChild:keyObject];
    [keyObject setScale:.8];
    
    // Stat counter initialization
    enemiesHit = 0;
    numberOfBulletsFired = 0;
    numberOfTimesHit = 0;
    
    //TODO: review following for multiplayer;
    players = [NSMutableArray arrayWithCapacity:constants.numberOfPlayers];
    
    //TODO: remove the following after debugging
    host = [SKLabelNode labelNodeWithFontNamed:gDefaultFontName];
    host.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    host.fontColor = [UIColor whiteColor];
    host.fontSize = 12;
    host.zPosition = 0.6;
    [self addChild:host];
    
    timeSinceLastEnemySpawn = 0.0f;
    lastUpdateTimeInterval = (float)0;
    frameTime = (float)0;
    
    endGame = NO;
    coolDownTimer = 0;
    inCoolDown = NO;
    
    collisionResponse = [[CollisionResponse alloc]init];
    collisionResponse.delegate = self;
    
#pragma mark wall setup
    SKSpriteNode *wall = (SKSpriteNode*)[self childNodeWithName:@"TopWall"];
    SceneWall *topWall = [[SceneWall alloc]init];
    topWall.position = wall.position;
    topWall.zRotation = wall.zRotation;
    topWall.xScale = wall.xScale;
    topWall.yScale = wall.yScale;
    topWall.name = @"TopWall";
    [self addChild:topWall];
    
    wall = (SKSpriteNode*)[self childNodeWithName:@"BottomWall"];
    SceneWall *bottomWall = [[SceneWall alloc]init];
    bottomWall.position = wall.position;
    bottomWall.zRotation = wall.zRotation;
    bottomWall.xScale = wall.xScale;
    bottomWall.yScale = wall.yScale;
    bottomWall.name = @"BottomWall";
    [self addChild:bottomWall];
    
    wall = (SKSpriteNode*)[self childNodeWithName:@"RightWall"];
    SceneWall *rightWall = [[SceneWall alloc]init];
    rightWall.position = wall.position;
    rightWall.zRotation = wall.zRotation;
    rightWall.xScale = wall.xScale;
    rightWall.yScale = wall.yScale;
    rightWall.name = @"rightWall";
    [self addChild:rightWall];
    
    wall = (SKSpriteNode*)[self childNodeWithName:@"LeftWall"];
    SceneWall *leftWall = [[SceneWall alloc]init];
    leftWall.position = wall.position;
    leftWall.zRotation = wall.zRotation;
    leftWall.xScale = wall.xScale;
    leftWall.yScale = wall.yScale;
    leftWall.name = @"LeftWall";
    [self addChild:leftWall];
    
    //In-scene walls
     wall = (SKSpriteNode*)[self childNodeWithName:@"LevelWall0"];
     if (wall == NULL)
         NSLog(@"GameSceneL2::init element not found LevelWall0");
     SceneWall *levelWall0 = [[SceneWall alloc]init];
     levelWall0.position = wall.position;
     levelWall0.zRotation = wall.zRotation;
     levelWall0.xScale = wall.xScale;
     levelWall0.yScale = wall.yScale;
     levelWall0.name = @"levelWall0";
     [self addChild:levelWall0];
    
    // Stat counter initialization
    enemiesHit = 0;
    numberOfBulletsFired = 0;
    numberOfTimesHit = 0;
    
    //Emitter setup
    playerTrailFX = [[SKEmitterNode alloc]init];
    NSString *fxTrail = [[NSBundle mainBundle] pathForResource:@"PlayerTrail" ofType:@"sks"];
    playerTrailFX = [NSKeyedUnarchiver unarchiveObjectWithFile:fxTrail];
    if (playerTrailFX == nil)
        NSLog(@"GameScene::initializedScene can't load player particle emitter");
    playerTrailFX.targetNode = self.scene;
    playerTrailFX.name = @"playerTrailFX";
    
#pragma mark timed bombs
    //     NSLog(@"Player pos %f %f", player.position.x, player.position.y);
    SKSpriteNode *b = (SKSpriteNode*)[self childNodeWithName:@"Bomb1Timed"];
    Bomb *b1 = [[Bomb alloc] init:b.position andPlayer:player andTime:1.5];
    [self addChild:b1];
    b = (SKSpriteNode*)[self childNodeWithName:@"Bomb2Timed"];
    Bomb *b2 = [[Bomb alloc] init:b.position andPlayer:player andTime:3.5];
    [self addChild:b2];
    b = (SKSpriteNode*)[self childNodeWithName:@"Bomb3Timed"];
    Bomb *b3 = [[Bomb alloc] init:b.position andPlayer:player andTime:5.5];
    [self addChild:b3];
    b = (SKSpriteNode*)[self childNodeWithName:@"Bomb1Prox"];
    Bomb *b4 = [[Bomb alloc] init:b.position /*andType:kProximityBombType*/ andPlayer:player];
    [self addChild:b4];
    b = (SKSpriteNode*)[self childNodeWithName:@"Bomb2Prox"];
    Bomb *b5 = [[Bomb alloc] init:b.position /*andType:kProximityBombType*/ andPlayer:player];
    [self addChild:b5];
    b = (SKSpriteNode*)[self childNodeWithName:@"Bomb3Prox"];
    Bomb *b6 = [[Bomb alloc] init:b.position /*andType:kProximityBombType*/ andPlayer:player];
    [self addChild:b6];

    touchController = [[TouchController alloc]init:player];
    
    levelText = (SKLabelNode*)[self childNodeWithName:@"LevelText"];
}

#pragma mark Touch Processing

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
        [touchController touchesBegan:touches andScene:self];
//    UITouch *touch = [touches anyObject];
//    CGPoint location = [touch locationInNode:self];
//    SKNode *node = [self nodeAtPoint:location];
//    if (currentTouch != nil)
//        return;
//    if (endGame)
//        return;
//    if (player.isHidden)
//        return;
//    for (UITouch *touch in touches) {
//        CGPoint location = [touch locationInNode:self];
//        touchPositionX = [touch locationInNode:self].x;
//        touchPositionY = [touch locationInNode:self].y;
//        currentTouch = touch;
//    }
//    ignoreTouchesCollision = NO;
//    [player addChild:playerTrailFX];
}

-(void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event  {
    if (endGame)
        return;
    if (player.isHidden)
        return;
    [touchController touchesMoved:touches andScene:self andNumberOfBullets:numberOfBulletsFired];

}

-(void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    if (player.isHidden)
        return;
    [touchController touchesCancelled:touches];
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [touchController touchesEnded:touches];
}

-(void)changeSceneToGameOver{
    sleep(1.1); //allow last sound fx to play
    screenCapture = [self.view textureFromNode:self];
    for(SKNode * child in self.scene.children)
        [child removeAllActions];
    [self.scene removeAllChildren];
    [self removeAllActions];
//    constants.timeInGame = 0;
//    constants.timeInWave = 0;
//    SKTransition *reveal = [SKTransition revealWithDirection:SKTransitionDirectionLeft duration:1.0];
    timeInWave = 0;
    self.timeInGame = 0;
}

#pragma mark Update

-(void)update:(CFTimeInterval)currentTime {
    // Time stuff
    frameTime = currentTime - lastUpdateTimeInterval;
    lastUpdateTimeInterval = currentTime;
    if (frameTime > 1) { // more than a second since last update
        frameTime = 1.0 / 60.0;
        lastUpdateTimeInterval = currentTime;
    }
#pragma mark Camera_Player_Following
    self.timeInGame += frameTime;
    timeInWave += frameTime;
    //   endGame = NO;
    Player *losingPlayer;
    //Call the game object updates
    if (!endGame){
        for (Player *p in players){
            [p update:frameTime];
            // TODO: Make 20.0 60.0 for release
            if (p.health <= 0 ){
                endGame = YES;
                losingPlayer = p;
            }
        }
    }
    
    if (endGame & constants.showGameOverScene){
        [self changeSceneToGameOver];
    }
}

#pragma mark Physics Support Functions

-(void)removeCollisionFX{
    [self removeFromParent];
}

//TODO:Add FXs for other powerup types

-(void)collisionActionFX:(PowerUpType)powerUpType{
    NSString *fileName = [[NSString alloc]init];
    
}

#pragma mark Contact
-(void)didBeginContact:(SKPhysicsContact *)contact{
    [collisionResponse response: contact];
    return;
    
}

- (void)didLoadReferenceNode:(SKNode *)node{
    NSLog(@"Loaded");
}
#pragma mark CollisonResponseProtocol Functions

-(void)reversePlayerPosition:(Player*)p{
    CGVector reverseDirection = CGVectorMake(p.position.x - p.lastPosition.x, p.position.y - p.lastPosition.y);
    reverseDirection = [vectorSupport normalize:reverseDirection];
    reverseDirection = [vectorSupport scalarMultiply:reverseDirection andScalar:-20];
    reverseDirection = [vectorSupport add:CGVectorMake(p.position.x, p.position.y) andVector2:reverseDirection];
    [player removeFromParent];
    player.position = CGPointMake(reverseDirection.dx, reverseDirection.dy);
    [self addChild:player];
    ignoreTouchesCollision = YES;
}

-(void)changeScene{
    SKAction *runBlock = [SKAction runBlock:^{
        InterstatialGameScene *s = (InterstatialGameScene *)[SKScene nodeWithFileNamed:@"InterstatialGameScene"];
        s.scaleMode = SKSceneScaleModeAspectFill;
        SKView *skView = (SKView *)self.view;
        s.numberOfBullets = self->numberOfBulletsFired;
        s.numberOfEnemiesHit = self->enemiesHit;
        s.numberOfPlayerHits = self->numberOfTimesHit;
        s.nextLevel = @"GameSceneL2";
        [skView presentScene:s];
    }];
    SKAction *wait = [SKAction waitForDuration:0.5];
    SKAction *sequence = [SKAction sequence:@[wait, runBlock]];
    [self runAction:sequence];
}

-(void)updateLevelStat:(NSString*)statName andValue:(float)value{
    if ([statName isEqualToString:@"numberOfTimesHit"]){
        numberOfTimesHit += (int)value;
        return;
    }
    if ([statName isEqualToString:@"enemiesHit"]){
        enemiesHit += value;
        return;
    }
    else
        NSLog(@"GameSceneL2::updateLevelStat stat not found %@", statName);
}

#pragma mark dealloc

-(void)dealloc{
//    constants.waveCounter = 0;
//    constants.numberOfWaveCycles = 0;
//    constants.numberOfWaves = 0;
//    constants.timeInGame = 0;
//    constants.timeInWave = 0;
    currentTouch = nil;
    [self removeAllChildren];
    [self removeAllActions];
    NSLog(@"Game Scene L1 dealloc");
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
