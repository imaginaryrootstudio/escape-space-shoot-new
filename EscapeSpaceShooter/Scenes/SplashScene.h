//
//  SplashScene.h
//  Escape: A Space Shooter
//
//  Created by William Birmingham on 1/23/19.
//  Copyright © 2019, 2020, 2021 Imaginary Root Studio LLC. All rights reserved.
//


#ifndef SplashScene_h
#define SplashScene_h
#import <SpriteKit/SpriteKit.h>
#import <Foundation/Foundation.h>
#import "GameScene.h"
#import "Constants.h"
#import "GameSceneL0.h"
//#import "GameSceneL1.h"
#import "GameSceneL3.h"
#import "GameSceneL_Story_1.h"
#import "StarfieldFX.h"

@interface SplashScene:SKScene

@end
#endif /* SplashScene_h */
