//
//  GameSceneL0-0.m
//  Escape: A Space Shooter
// Copyright (c) 2020 Imaginary Root Studio LLC. All rights reserved.

#import "GameSceneL0_0.h"

@implementation GameSceneL0_0{
    
    Player* player;
    double rotation;
    CGVector selfAngle; //angle of the player
    double speedScale; //based on magnitude of selfAngle
    float dot;
    VectorSupport *vectorSupport;
    float touchPositionX, touchPositionY;
    NSMutableArray *players;
    NSInteger localPlayerIndex;
    Constants *constants;
    //Labels
    SKLabelNode *localPlayerScoreLabel;
    SKLabelNode *otherPlayerScoreLabel;
    SKLabelNode *host;
    SKLabelNode *playingTime;
    SKLabelNode *levelText;
    
    //Timing
    float timeSinceLastEnemySpawn;
    CFTimeInterval frameTime;
    NSTimeInterval lastUpdateTimeInterval;
    
    float timeInWave;
    
    //Lighting
    SKLightNode *light;
    
    //Misc
    BOOL sceneCreated;
    long numberOfEnemiesOnScreen;
    
    //used to prevent two touches from effecting player movement
    UITouch *currentTouch;
  
    SKTexture *screenCapture;
    
    SKEmitterNode *backgroundFX;
    
    bool endGame;
    
    TapticEngine *enemyTaptic;
    TapticEngine *powerupTaptic;
    
//    Bullet *bullet;

    Enemy *enemyTurret0;
    Enemy *enemyTurret1;
    
    float coolDownTimer;
    bool inCoolDown;
    bool ignoreTouchesCollision;
    
    ShooterBase *shooterBase;

    SceneWall *sceneWall;
    
    SKCameraNode *camera;
    
    WaypointNavigation *waypointNavigation;
    CameraController *cameraController;
    
    KeyObject *keyObject;
    
    //Game stat counters
    int enemiesHit;
    int numberOfBulletsFired;
    int numberOfTimesHit;
    
    //Emitter nodes
    SKEmitterNode *playerTrailFX;
//    SKEmitterNode *background;
    
    CollisionResponse *collisionReponse;
    
    TouchController *touchController;
    
    StarfieldFX *starfieldFX;
}

-(void)didMoveToView:(SKView *)view {
    constants = [Constants getInstance];
    constants.currentScene = self;
    currentTouch = nil;
    //TODO: undo the following lines for multiplayer
    //    //Setup AuthenticationViewController
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showAuthenticationViewController)
    //                                                 name: PresentAuthenticationViewController object:nil];
    //    if (!constants.playerAuthenticated)
    //        [[GameKitHelper sharedGameKitHelper] authenticateLocalPlayer];
    
    self.physicsWorld.gravity = CGVectorMake(0, 0); //no gravity
    self.physicsWorld.contactDelegate = self;
    sceneCreated = NO;
    if (!sceneCreated){
        [self initializeScene];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(pauseGameScene)
                                                 name:@"PauseGameScene"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(resumeGameScene)
                                                 name:@"ResumeGameScene"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                              selector:@selector(removeText)
                                                  name:@"CameraFollowingPlayer"
                                                object:nil];
}
-(void)removeText{
    [levelText removeFromParent];
}
- (void)pauseGameScene {
    if (self.view.scene) {
        self.view.paused = self.view.scene.paused = YES;
        [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    }
}
- (void)resumeGameScene {
    if (self.view.scene) {
        self.view.paused = self.view.scene.paused = NO;
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    }
}
- (void)initializeScene{
    sceneCreated = YES;
    vectorSupport = [[VectorSupport alloc]init];
    rotation = 0;
    dot = 0;
    speedScale = 0;
    selfAngle = CGVectorMake(0, 1);         //start point to top of screen
    self.backgroundColor = [UIColor blackColor];
    self.timeInGame = 0;
    timeInWave = 0;
    
    //TODO: change this for multiplayer
    localPlayerIndex =  0;
    SKSpriteNode *p1 = (SKSpriteNode*)[self childNodeWithName:@"PlayerSpawn"];
    player = [[Player alloc] init:0 andInitialPosition:p1.position];
    player.name = @"Player";
    //player.position = p1.position;
    [self addChild:player];
//
//    SKView * skViewFront;
//
//      // Create and configure the front scene.
//      SKScene * sceneFront = [SKScene sceneWithSize:skViewFront.bounds.size];
//      sceneFront.scaleMode = SKSceneScaleModeAspectFill;
//    [sceneFront addChild:player];
//    [self.view presentScene:sceneFront];
//
//      // Present the front scene.
//      [skViewFront presentScene:sceneFront];
//    float rotationAngle = 100;
//    float tempAngle = (M_PI/180.0f)*rotationAngle;
//    CATransform3D transform = CATransform3DMakeRotation(tempAngle, 1.0, 0.0, 0.0);
//    sceneFront.scene.view.layer.transform = transform;
 
    //TODO: remove the following after debugging
    host = [SKLabelNode labelNodeWithFontNamed:gDefaultFontName];
    host.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    host.fontColor = [UIColor whiteColor];
    host.fontSize = 12;
    host.zPosition = 0.6;
    [self addChild:host];
    
    timeSinceLastEnemySpawn = 0.0f;
    lastUpdateTimeInterval = (float)0;
    frameTime = (float)0;
    
    endGame = NO;
    coolDownTimer = 0;
    inCoolDown = NO;
    
    levelText = (SKLabelNode*)[self childNodeWithName:@"LevelText"];
    
#pragma mark Shooter Set Up
    SKSpriteNode *t0 = (SKSpriteNode*)[self childNodeWithName:@"Turret0"];
    SKSpriteNode *t1 = (SKSpriteNode*)[self childNodeWithName:@"Turret1"];
    
    if (t0 == nil)
        NSLog(@"GameSceneL0_0::initializeScene t0 not found");
    if (t1 == nil)
        NSLog(@"GameSceneL0_0::initializeScene t1 not found");
    
    enemyTurret0 = [[Enemy alloc] init:t0.position andDirection:CGVectorMake(2, 0)];
    enemyTurret1 = [[Enemy alloc] init:t1.position andDirection:CGVectorMake(2, 0)];

    ShooterBase* shooterBase0 = [[ShooterBase alloc]init:CGVectorMake(1, 0) andCoolDown:1 andBulletType: kSimpleBulletType];
    enemyTurret0.position = t0.position;
    enemyTurret0.zRotation = t0.zRotation;
    [enemyTurret0 addChild:shooterBase0];
    [self addChild:enemyTurret0];

    ShooterBase* shooterBase1 = [[ShooterBase alloc]init:CGVectorMake(1, 0) andCoolDown:1 andBulletType: kSimpleBulletType];
    enemyTurret1.position = t1.position;
    enemyTurret1.zRotation = t1.zRotation;
    [enemyTurret1 addChild:shooterBase1];
    [self addChild:enemyTurret1];
    
    ignoreTouchesCollision = NO;
    camera = (SKCameraNode*)[self childNodeWithName:@"SceneCamera"];
    
#pragma mark Wall Setup
    SKSpriteNode *wall = (SKSpriteNode*)[self childNodeWithName:@"TopWall"];
    SceneWall *topWall = [[SceneWall alloc]init];
    topWall.position = wall.position;
    topWall.zRotation = wall.zRotation;
    topWall.xScale = wall.xScale;
    topWall.yScale = wall.yScale;
    topWall.name = @"TopWall";
    [self addChild:topWall];
    
    wall = (SKSpriteNode*)[self childNodeWithName:@"BottomWall"];
    SceneWall *bottomWall = [[SceneWall alloc]init];
    bottomWall.position = wall.position;
    bottomWall.zRotation = wall.zRotation;
    bottomWall.xScale = wall.xScale;
    bottomWall.yScale = wall.yScale;
    bottomWall.name = @"BottomWall";
    [self addChild:bottomWall];
    
    wall = (SKSpriteNode*)[self childNodeWithName:@"RightWall"];
    SceneWall *rightWall = [[SceneWall alloc]init];
    rightWall.position = wall.position;
    rightWall.zRotation = wall.zRotation;
    rightWall.xScale = wall.xScale;
    rightWall.yScale = wall.yScale;
    rightWall.name = @"rightWall";
    [self addChild:rightWall];
    
    wall = (SKSpriteNode*)[self childNodeWithName:@"LeftWall"];
     SceneWall *leftWall = [[SceneWall alloc]init];
     leftWall.position = wall.position;
     leftWall.zRotation = wall.zRotation;
     leftWall.xScale = wall.xScale;
     leftWall.yScale = wall.yScale;
     leftWall.name = @"LeftWall";
     [self addChild:leftWall];
    
    //WaypointNavigation
    SKSpriteNode *c1 = (SKSpriteNode*)[self childNodeWithName:@"WayPoint1Camera"];
    SKSpriteNode *c2 = (SKSpriteNode*)[self childNodeWithName:@"WayPoint2Camera"];
    camera.position = c1.position;
    //    waypointNavigation = [[WaypointNavigation alloc]init:camera andWaypoints:@[c1, c2]];
    cameraController = [[CameraController alloc]init:camera andWaypoints:@[c1,c2] andPlayer:player ];
    cameraController.cameraScaleFlyover = CGVectorMake(1.1, 1.1);
    cameraController.cameraScaleFollowPlayer = CGVectorMake(0.85, 0.85);
    [cameraController followWaypoints:YES andFlyOverSpeed:gSceneFlyOverSpeed];
    
    SKEmitterNode *emitter = (SKEmitterNode*)[self childNodeWithName:@"KeyParticles"];
    keyObject = [[KeyObject alloc]init:emitter andPosition:emitter.position];
    keyObject.xScale = 0.9;
    keyObject.yScale = 0.9;
    [self addChild:keyObject];
    
    // Stat counter initialization
    enemiesHit = 0;
    numberOfBulletsFired = 0;
    numberOfTimesHit = 0;
    
#pragma mark FX, collison response, touch controller setup
    
    playerTrailFX = [[SKEmitterNode alloc]init];
    NSString *fxTrail = [[NSBundle mainBundle] pathForResource:@"PlayerTrail" ofType:@"sks"];
    playerTrailFX = [NSKeyedUnarchiver unarchiveObjectWithFile:fxTrail];
    if (!playerTrailFX)
         NSLog(@"GameScene::initializedScene can't load player particle emitter");
     playerTrailFX.targetNode = self.scene;
     playerTrailFX.name = @"playerTrailFX";
    
    starfieldFX = [[StarfieldFX alloc] init:self topWallPosition:topWall.position bottomWallPosition:bottomWall.position
                           leftWallPosition:leftWall.position rightWallPosition:rightWall.position];

    collisionReponse = [[CollisionResponse alloc]init];
    collisionReponse.delegate = self;
    
    touchController = [[TouchController alloc]init:player];
}


#pragma mark Touch Processing

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [touchController touchesBegan:touches andScene:self];
}

-(void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event  {
    if (endGame)
        return;
    if (player.isHidden)
        return;
    [touchController touchesMoved:touches andScene:self andNumberOfBullets:numberOfBulletsFired];
}

-(void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    if (player.isHidden)
        return;
    [touchController touchesCancelled:touches];
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [touchController touchesEnded:touches];
}

-(void)changeSceneToGameOver{
    sleep(1.1); //allow last sound fx to play
    screenCapture = [self.view textureFromNode:self];
    for(SKNode * child in self.scene.children)
        [child removeAllActions];
    [self.scene removeAllChildren];
    [self removeAllActions];
//    constants.timeInGame = 0;
//    constants.timeInWave = 0;
   // SKTransition *reveal = [SKTransition revealWithDirection:SKTransitionDirectionLeft duration:1.0];
    timeInWave = 0;
    self.timeInGame = 0;
}

#pragma mark Update

-(void)update:(CFTimeInterval)currentTime {
    // Time stuff
    frameTime = currentTime - lastUpdateTimeInterval;
    lastUpdateTimeInterval = currentTime;
    if (frameTime > 1) { // more than a second since last update
        frameTime = 1.0 / 60.0;
        lastUpdateTimeInterval = currentTime;
    }
#pragma mark Camera_Player_Following
    self.timeInGame += frameTime;
    timeInWave += frameTime;
    //   endGame = NO;
    Player *losingPlayer;
    //Call the game object updates
    if (!endGame){
        for (Player *p in players){
            [p update:frameTime];
            // TODO: Make 20.0 60.0 for release
            if (p.health <= 0 ){
                endGame = YES;
                losingPlayer = p;
            }
        }
    }
 
    if (endGame & constants.showGameOverScene){
        [self changeSceneToGameOver];
    }
}

#pragma mark Physics Support Functions

-(void)removeCollisionFX{
    [self removeFromParent];
}

//TODO:Add FXs for other powerup types

//-(void)collisionActionFX:(PowerUpType)powerUpType{
//    NSString *fileName = [[NSString alloc]init];
//}
//
//-(void)playPowerUpSoundFX{
//    if (!constants.soundFxEnabled)
//        return;
//    SKAction *powerupSoundfx = [SKAction playSoundFileNamed:@"powerupSoundFX.wav" waitForCompletion:YES];
//    [self runAction:powerupSoundfx];
//}
//
//-(void)playGreenPowerUpSoundFX{
//    if (!constants.soundFxEnabled)
//        return;
//    SKAction *powerupSoundfx = [SKAction playSoundFileNamed:@"powerupSoundFX.wav" waitForCompletion:YES];
//    [self runAction:powerupSoundfx];
//}
//-(void)playYellowPowerUpSoundFX{
//    if (!constants.soundFxEnabled)
//        return;
//    SKAction *powerupSoundfx = [SKAction playSoundFileNamed:@"powerupSoundFX.wav" waitForCompletion:YES];
//    [self runAction:powerupSoundfx];
//}

-(void)playEnemySoundFX{
    if (!constants.soundFxEnabled)
        return;
    SKAction *enemySoundfx = [SKAction playSoundFileNamed:@"NewDamageSound.wav" waitForCompletion:NO];
    [self runAction:enemySoundfx];
}

#pragma mark Contact
-(void)didBeginContact:(SKPhysicsContact *)contact{
    [collisionReponse response:contact];
}

- (void)didLoadReferenceNode:(SKNode *)node{
    NSLog(@"Loaded");
}

#pragma mark CollisionResponse Protocol functions

-(void)reversePlayerPosition:(Player*)p{
    CGVector reverseDirection = CGVectorMake(p.position.x - p.lastPosition.x, p.position.y - p.lastPosition.y);
    reverseDirection = [vectorSupport normalize:reverseDirection];
    reverseDirection = [vectorSupport scalarMultiply:reverseDirection andScalar:-20];
    reverseDirection = [vectorSupport add:CGVectorMake(p.position.x, p.position.y) andVector2:reverseDirection];
    [player removeFromParent];
    player.position = CGPointMake(reverseDirection.dx, reverseDirection.dy);
    [self addChild:player];
    ignoreTouchesCollision = YES;
}

-(void)changeScene{
    SKAction *runBlock = [SKAction runBlock:^{
        InterstatialGameScene *s = (InterstatialGameScene *)[SKScene nodeWithFileNamed:@"InterstatialGameScene"];
        s.scaleMode = SKSceneScaleModeAspectFill;
        SKView *skView = (SKView *)self.view;
        s.numberOfBullets = self->numberOfBulletsFired;
        s.numberOfEnemiesHit = self->enemiesHit;
        s.numberOfPlayerHits = self->numberOfTimesHit;
        s.nextLevel = @"GameSceneL2";
        s.showStats = YES;
        [skView presentScene:s];
    }];
    SKAction *wait = [SKAction waitForDuration:0.5];
    SKAction *sequence = [SKAction sequence:@[wait, runBlock]];
    [self runAction:sequence];
}

-(void)updateLevelStat:(NSString*)statName andValue:(float)value{
    if ([statName isEqualToString:@"numberOfTimesHit"]){
        numberOfTimesHit += (int)value;
        return;
    }
    if ([statName isEqualToString:@"enemiesHit"]){
        enemiesHit += value;
        return;
    }
    else
        NSLog(@"GameSceneL0_0::updateLevelStat stat not found %@", statName);
}

- (void)processPickup:(SKNode *)pickup {
    
}
-(void)willZeroBulletsFired{
    constants.bulletsFired = 0;
}

-(void)dealloc{
//    constants.waveCounter = 0;
//    constants.numberOfWaveCycles = 0;
//    constants.numberOfWaves = 0;
//    constants.timeInGame = 0;
//    constants.timeInWave = 0;
    currentTouch = nil;
    NSLog(@"GameScene L0 dealloc");
    [[NSNotificationCenter defaultCenter] removeObserver:self];

}


@end
