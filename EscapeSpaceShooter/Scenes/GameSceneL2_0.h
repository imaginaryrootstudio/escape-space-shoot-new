//
//  GameSceneL0_0.h
//  Escape: A Space Shooter
// Copyright (c) 2020 Imaginary Root Studio LLC. All rights reserved.


#import <SpriteKit/SpriteKit.h>
//#import "MultiplayerNetworking.h"
#import "Constants.h"
#import "TapticEngine.h"
#import "Player.h"
//#import "ScreenWalls.h"
#import "VectorSupport.h"
#import "Bullet.h"
#import "ShooterBase.h"
#import "WaypointNavigation.h"
#import "CameraController.h"
#import "PlayerBullet.h"
#import "KeyObject.h"
#import "InterstatialGameScene.h"
#import "Bomb.h"
#import "SceneWall.h"
#import "CollisionResponse.h"
#import "StarfieldFX.h"



//#import "TouchController.h"

@interface GameSceneL2_0 : SKScene <SKPhysicsContactDelegate, UIGestureRecognizerDelegate, CollisionResponseProtocol>

//@property /*(nonatomic, strong)*/ MultiplayerNetworking *networkEngine;
@property float timeInGame;

@end
