//
//  GameSceneL0.m
//  Escape: A Space Shooter
// Copyright (c) 2020 Imaginary Root Studio LLC. All rights reserved.

#import "GameSceneL3.h"

@implementation GameSceneL3{
    
    Player* player;
    double rotation;
    CGVector selfAngle; //angle of the player
    double speedScale; //based on magnitude of selfAngle
    float dot;
    VectorSupport *vectorSupport;
    float touchPositionX, touchPositionY;
    NSMutableArray *players;
    NSInteger localPlayerIndex;
    Constants *constants;
    //Labels
    SKLabelNode *localPlayerScoreLabel;
    SKLabelNode *otherPlayerScoreLabel;
    SKLabelNode *host;
    SKLabelNode *playingTime;
    
    //Timing
    float timeSinceLastEnemySpawn;
    CFTimeInterval frameTime;
    NSTimeInterval lastUpdateTimeInterval;
    
    float timeInWave;
    
    //Lighting
    SKLightNode *light;
    
    //Misc
    BOOL sceneCreated;
    long numberOfEnemiesOnScreen;
    
    //used to prevent two touches from effecting player movement
    UITouch *currentTouch;
    
    SKTexture *screenCapture;
    
    SKSpriteNode *background;
    
    bool endGame;
    
    TapticEngine *enemyTaptic;
    TapticEngine *powerupTaptic;
    
    Enemy *enemyTurret1;
    Enemy *enemyTurret2;
    Enemy *enemyTurret3;
    Enemy *enemyTurret4;
    
    float coolDownTimer;
    bool inCoolDown;
    
    ShooterBase *shooterBase;
    SceneWall *sceneWall;
    
    bool ignoreTouchesCollision;
    
    SKCameraNode *camera;
    
    WaypointNavigation *waypointNavigation;
    CameraController *cameraController;
    
    KeyObject *keyObject;
    
    PickupObject *pickupObject;
    
    //Game stat counters
    int enemiesHit;
    int numberOfBulletsFired;
    int numberOfTimesHit;
    
    //Emitter nodes
    SKEmitterNode *playerTrailFX;
    
    CollisionResponse *collisionReponse;
    
    TouchController *touchController;
    
    NSArray *removableWalls;
    NSArray *removableWallSprites;
    SKEmitterNode *emitter2;
    
//    SKEmitterNode *backgroundFX1;
//    SKEmitterNode *backgroundFX2;
    
    StarfieldFX *starfieldFX;
    
    SKLabelNode *levelText;
}

-(void)didMoveToView:(SKView *)view {
    constants = [Constants getInstance];
    constants.currentScene = self;
    currentTouch = nil;
    //TODO: undo the following lines for multiplayer
    //    //Setup AuthenticationViewController
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showAuthenticationViewController)
    //                                                 name: PresentAuthenticationViewController object:nil];
    //    if (!constants.playerAuthenticated)
    //        [[GameKitHelper sharedGameKitHelper] authenticateLocalPlayer];
    
    self.physicsWorld.gravity = CGVectorMake(0, 0); //no gravity
    self.physicsWorld.contactDelegate = self;
    sceneCreated = NO;
    if (!sceneCreated){
        [self initializeScene];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(pauseGameScene)
                                                 name:@"PauseGameScene"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(resumeGameScene)
                                                 name:@"ResumeGameScene"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                              selector:@selector(removeText)
                                                  name:@"CameraFollowingPlayer"
                                                object:nil];
}
-(void)removeText{
    [levelText removeFromParent];
}
- (void)pauseGameScene {
    if (self.view.scene) {
        self.view.paused = self.view.scene.paused = YES;
        //        [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
        UIView *v = (UIView*)self.view;
        v.userInteractionEnabled = YES;
    }
}
- (void)resumeGameScene {
    if (self.view.scene) {
        self.view.paused = self.view.scene.paused = NO;
        //        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        UIView *v = (UIView*)self.view;
        v.userInteractionEnabled = YES;
    }
}
- (void)initializeScene{
    sceneCreated = YES;
    vectorSupport = [[VectorSupport alloc]init];
    rotation = 0;
    dot = 0;
    speedScale = 0;
    selfAngle = CGVectorMake(0, 1);         //start point to top of screen
    self.backgroundColor = [UIColor blackColor];
    self.timeInGame = 0;
    timeInWave = 0;
    
    //TODO: change this for multiplayer
    localPlayerIndex =  0;
    SKSpriteNode *p1 = (SKSpriteNode*)[self childNodeWithName:@"PlayerSpawn"];
    if (p1 == NULL)
        NSLog(@"GameSceneL2::init element not found PlayerSpawn");
    player = [[Player alloc] init:0 andInitialPosition:p1.position];
    player.name = @"Player";
    //player.position = p1.position;
    [self addChild:player];
    
    //TODO: review following for multiplayer;
    players = [NSMutableArray arrayWithCapacity:constants.numberOfPlayers];
    
    //TODO: remove the following after debugging
    host = [SKLabelNode labelNodeWithFontNamed:gDefaultFontName];
    host.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    host.fontColor = [UIColor whiteColor];
    host.fontSize = 12;
    host.zPosition = 0.6;
    [self addChild:host];
    
    timeSinceLastEnemySpawn = 0.0f;
    lastUpdateTimeInterval = (float)0;
    frameTime = (float)0;
    
    endGame = NO;
    coolDownTimer = 0;
    inCoolDown = NO;
    
#pragma mark Shooter Set Up
    SKSpriteNode *t1 = (SKSpriteNode*)[self childNodeWithName:@"TurretShooter_1"]; //@"Turret1"];
    if (t1 == NULL)
        NSLog(@"GameSceneL2::init element not found urretShooter_1");
    SKSpriteNode *t2 = (SKSpriteNode*)[self childNodeWithName:@"TurretShooter_2"];
    if (t2 == NULL)
        NSLog(@"GameSceneL2::init element not found urretShooter_2");
    SKSpriteNode *t3 = (SKSpriteNode*)[self childNodeWithName:@"Turret3"];
    if (t3 == NULL)
        NSLog(@"GameSceneL2::init element not found Turret3");
    SKSpriteNode *t4 = (SKSpriteNode*)[self childNodeWithName:@"Turret2"];
    if (t4 == NULL)
        NSLog(@"GameSceneL2::init element not found Turret2");
    
    enemyTurret1 = [[Enemy alloc] init:t1.position andDirection:CGVectorMake(2, 0)];
    enemyTurret2 = [[Enemy alloc] init:t2.position andDirection:CGVectorMake(2, 0)];
    enemyTurret3 = [[Enemy alloc] init:t3.position andDirection:CGVectorMake(2, 0)];
    enemyTurret4 = [[Enemy alloc] init:t4.position andDirection:CGVectorMake(2, 0)];
    
    ShooterBase *turret1 = [[ShooterBase alloc] init:CGVectorMake(0,1) andCoolDown:1 andBulletType:kSimpleBulletType];
    enemyTurret1.position = t1.position;
    enemyTurret1.zRotation = t1.zRotation;
    [enemyTurret1 addChild:turret1];
    [self addChild:enemyTurret1];
    
    ShooterBase *turret2 = [[ShooterBase alloc] init:CGVectorMake(0,1) andCoolDown:1 andBulletType:kSimpleBulletType];
    enemyTurret2.position = t2.position;
    enemyTurret2.zRotation = t2.zRotation;
    [enemyTurret2 addChild:turret2];
    [self addChild:enemyTurret2];
    
    TargetedShootBase *targetedShooterBase = [[TargetedShootBase alloc] init:CGVectorMake(0, 1) andCoolDown:1 andPlayer:player];
    enemyTurret3.position = t3.position;
    enemyTurret3.zRotation = t3.zRotation;
    [enemyTurret3 addChild:targetedShooterBase];
    [self addChild:enemyTurret3];
    
    TargetedShootBase *targetedShooterBase2 = [[TargetedShootBase alloc] init:CGVectorMake(0, 1) andCoolDown:1 andPlayer:player];
      enemyTurret4.position = t4.position;
      enemyTurret4.zRotation = t4.zRotation;
      [enemyTurret4 addChild:targetedShooterBase2];
      [self addChild:enemyTurret4];
    
    ignoreTouchesCollision = NO;
    camera = (SKCameraNode*)[self childNodeWithName:@"SceneCamera"];
    if (camera == NULL)
        NSLog(@"GameSceneL2::init element not found camera");
    
#pragma mark Wall Setup
    SKSpriteNode *wall = (SKSpriteNode*)[self childNodeWithName:@"TopWall"];
    if (wall == NULL)
        NSLog(@"GameSceneL2::init element not found TopWall");
    SceneWall *topWall = [[SceneWall alloc]init];
    topWall.position = wall.position;
    topWall.zRotation = wall.zRotation;
    topWall.xScale = wall.xScale;
    topWall.yScale = wall.yScale;
    topWall.name = @"TopWall";
    [self addChild:topWall];
    
    wall = (SKSpriteNode*)[self childNodeWithName:@"BottomWall"];
    if (wall == NULL)
        NSLog(@"GameSceneL2::init element not found BottomWall");
    SceneWall *bottomWall = [[SceneWall alloc]init];
    bottomWall.position = wall.position;
    bottomWall.zRotation = wall.zRotation;
    bottomWall.xScale = wall.xScale;
    bottomWall.yScale = wall.yScale;
    bottomWall.name = @"BottomWall";
    [self addChild:bottomWall];
    
    wall = (SKSpriteNode*)[self childNodeWithName:@"RightWall"];
    if (wall == NULL)
        NSLog(@"GameSceneL2::init element not found RightWall");
    SceneWall *rightWall = [[SceneWall alloc]init];
    rightWall.position = wall.position;
    rightWall.zRotation = wall.zRotation;
    rightWall.xScale = wall.xScale;
    rightWall.yScale = wall.yScale;
    rightWall.name = @"rightWall";
    [self addChild:rightWall];
    
    wall = (SKSpriteNode*)[self childNodeWithName:@"LeftWall"];
    if (wall == NULL)
        NSLog(@"GameSceneL2::init element not found LeftWall");
    SceneWall *leftWall = [[SceneWall alloc]init];
    leftWall.position = wall.position;
    leftWall.zRotation = wall.zRotation;
    leftWall.xScale = wall.xScale;
    leftWall.yScale = wall.yScale;
    leftWall.name = @"LeftWall";
    [self addChild:leftWall];
    
    wall = (SKSpriteNode*)[self childNodeWithName:@"LeftDownWall"];
    if (wall == NULL)
        NSLog(@"GameSceneL2::init element not found LeftDownWall");
    SceneWall *leftDownWall = [[SceneWall alloc]init];
    leftDownWall.position = wall.position;
    leftDownWall.zRotation = wall.zRotation;
    leftDownWall.xScale = wall.xScale;
    leftDownWall.yScale = wall.yScale;
    leftDownWall.name = @"LeftWall";
    [self addChild:leftDownWall];
    
    wall = (SKSpriteNode*)[self childNodeWithName:@"BottomHalfWall"];
       if (wall == NULL)
           NSLog(@"GameSceneL2::init element not found BottomHalfWall");
       SceneWall *bottomHalfWall = [[SceneWall alloc]init];
       bottomHalfWall.position = wall.position;
       bottomHalfWall.zRotation = wall.zRotation;
       bottomHalfWall.xScale = wall.xScale;
       bottomHalfWall.yScale = wall.yScale;
       bottomHalfWall.name = @"BottomHalfWall";
       [self addChild:bottomHalfWall];
    
    //In-scene walls
    SKSpriteNode* lWall0 = (SKSpriteNode*)[self childNodeWithName:@"LevelWall0"];
    if (lWall0 == NULL)
        NSLog(@"GameSceneL2::init element not found LevelWall0");
    SceneWall *levelWall0 = [[SceneWall alloc]init];
    levelWall0.position = lWall0.position;
    levelWall0.zRotation = lWall0.zRotation;
    levelWall0.xScale = lWall0.xScale;
    levelWall0.yScale = lWall0.yScale;
    levelWall0.name = @"levelWall0";
    [self addChild:levelWall0];
    
    SKSpriteNode* lWall1 = (SKSpriteNode*)[self childNodeWithName:@"LevelWall1"];
    if (lWall1 == NULL)
        NSLog(@"GameSceneL2::init element not found LevelWall1");
    SceneWall *levelWall1 = [[SceneWall alloc]init];
    levelWall1.position = lWall1.position;
    levelWall1.zRotation = lWall1.zRotation;
    levelWall1.xScale = lWall1.xScale;
    levelWall1.yScale = lWall1.yScale;
    levelWall1.name = @"levelWall1";
    [self addChild:levelWall1];
    
    SKSpriteNode* lWall2 = (SKSpriteNode*)[self childNodeWithName:@"LevelWall2"];
    if (lWall2 == NULL)
        NSLog(@"GameSceneL3::init element not found LevelWall2");
    SceneWall *levelWall2 = [[SceneWall alloc]init];
    levelWall2.position = lWall2.position;
    levelWall2.zRotation = lWall2.zRotation;
    levelWall2.xScale = lWall2.xScale;
    levelWall2.yScale = lWall2.yScale;
    levelWall2.name = @"levelWall2";
    [self addChild:levelWall2];
    
    SKSpriteNode* sWall0 = (SKSpriteNode*)[self childNodeWithName:@"SceneWall0"];
     if (sWall0 == NULL)
         NSLog(@"GameSceneL3::init element not found sWall0");
     SceneWall *sceneWall0 = [[SceneWall alloc]init];
     sceneWall0.position = sWall0.position;
     sceneWall0.zRotation = sWall0.zRotation;
     sceneWall0.xScale = sWall0.xScale;
     sceneWall0.yScale = sWall0.yScale;
     sceneWall0.name = @"sceneWall0";
     [self addChild:sceneWall0];
    
    removableWalls = [[NSArray alloc]initWithObjects:levelWall0, levelWall1, levelWall2, nil];
    removableWallSprites = [[NSArray alloc]initWithObjects: lWall0, lWall1, lWall2, nil];
    //WaypointNavigation
    SKSpriteNode *c0 = (SKSpriteNode*)[self childNodeWithName:@"WayPoint0Camera"];
    if (c0 == NULL)
        NSLog(@"GameSceneL2::init element not found WayPoint10Camera");
    SKSpriteNode *c1 = (SKSpriteNode*)[self childNodeWithName:@"WayPoint1Camera"];
    if (c1 == NULL)
        NSLog(@"GameSceneL2::init element not found WayPoint1Camera");
    SKSpriteNode *c2 = (SKSpriteNode*)[self childNodeWithName:@"WayPoint2Camera"];
    if (c2 == NULL)
        NSLog(@"GameSceneL2::init element not found WayPoint2Camera");
    SKSpriteNode *c3 = (SKSpriteNode*)[self childNodeWithName:@"WayPoint3Camera"];
    if (c3 == NULL)
        NSLog(@"GameSceneL2::init element not found WayPoint3Camera");
    camera.position = c0.position;
    cameraController = [[CameraController alloc]init:camera andWaypoints:@[c0,c1,c2,c3] andPlayer:player ];
    cameraController.cameraScaleFlyover = CGVectorMake(1.4, 1.4);
    cameraController.cameraScaleFollowPlayer = CGVectorMake(0.85, 0.85);
    [cameraController followWaypoints:YES andFlyOverSpeed:gSceneFlyOverSpeed];
    
    SKEmitterNode *emitter = (SKEmitterNode*)[self childNodeWithName:@"KeyParticles"];
    if (emitter == NULL)
        NSLog(@"GameSceneL2::init element not found KeyParticles");
    keyObject = [[KeyObject alloc]init:emitter andPosition:emitter.position];
    [self addChild:keyObject];
    
    emitter2 = (SKEmitterNode*)[self childNodeWithName:@"KeyParticlesTrigger"];
    if (emitter2 == NULL)
        NSLog(@"GameSceneL2::init element not found KeyParticlesTrigger");
    pickupObject = [[PickupObject alloc]init:emitter2 andPosition:emitter2.position];
    pickupObject.name = @"KeyParticlesTrigger";
    [self addChild:pickupObject];
    
    // Stat counter initialization
    enemiesHit = 0;
    numberOfBulletsFired = 0;
    numberOfTimesHit = 0;
    
    //Emitter setup
    playerTrailFX = [[SKEmitterNode alloc]init];
    NSString *fxTrail = [[NSBundle mainBundle] pathForResource:@"PlayerTrail" ofType:@"sks"];
    playerTrailFX = [NSKeyedUnarchiver unarchiveObjectWithFile:fxTrail];
    if (playerTrailFX == nil)
        NSLog(@"GameScene::initializedScene can't load player particle emitter");
    playerTrailFX.targetNode = self.scene;
    playerTrailFX.name = @"playerTrailFX";
    
    collisionReponse = [[CollisionResponse alloc]init];
    collisionReponse.delegate = self;
    
    touchController = [[TouchController alloc]init:player];

    starfieldFX = [[StarfieldFX alloc] init:self topWallPosition:topWall.position bottomWallPosition:bottomWall.position
                           leftWallPosition:leftWall.position rightWallPosition:rightWall.position];
    
    levelText = (SKLabelNode*) [self childNodeWithName:@"LevelText"];
}


#pragma mark Touch Processing

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [touchController touchesBegan:touches andScene:self];
}

-(void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event  {
    if (endGame)
        return;
    if (player.isHidden)
        return;
    [touchController touchesMoved:touches andScene:self andNumberOfBullets:numberOfBulletsFired];
    
}

-(void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [touchController touchesCancelled:touches];
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [touchController touchesEnded:touches];
}

-(void)changeSceneToGameOver{
    sleep(1.1); //allow last sound fx to play
    screenCapture = [self.view textureFromNode:self];
    for(SKNode * child in self.scene.children)
        [child removeAllActions];
    [self.scene removeAllChildren];
    [self removeAllActions];
//    constants.timeInGame = 0;
//    constants.timeInWave = 0;
    //    SKTransition *reveal = [SKTransition revealWithDirection:SKTransitionDirectionLeft duration:1.0];
    timeInWave = 0;
    self.timeInGame = 0;
}

#pragma mark Update

-(void)update:(CFTimeInterval)currentTime {
    // Time stuff
    frameTime = currentTime - lastUpdateTimeInterval;
    lastUpdateTimeInterval = currentTime;
    if (frameTime > 1) { // more than a second since last update
        frameTime = 1.0 / 60.0;
        lastUpdateTimeInterval = currentTime;
    }
#pragma mark Camera_Player_Following
    self.timeInGame += frameTime;
    timeInWave += frameTime;
    //   endGame = NO;
    Player *losingPlayer;
    //Call the game object updates
    if (!endGame){
        for (Player *p in players){
            [p update:frameTime];
            // TODO: Make 20.0 60.0 for release
            if (p.health <= 0 ){
                endGame = YES;
                losingPlayer = p;
            }
        }
    }
    
    if (endGame & constants.showGameOverScene){
        [self changeSceneToGameOver];
    }
}

#pragma mark Physics Support Functions

#pragma mark Contact
-(void)didBeginContact:(SKPhysicsContact *)contact{
    [collisionReponse response:contact];
}

- (void)didLoadReferenceNode:(SKNode *)node{
    NSLog(@"Loaded");
}

#pragma mark CollisionResponse Protocol functions
-(void)reversePlayerPosition:(Player*)p{
    CGVector reverseDirection = CGVectorMake(p.position.x - p.lastPosition.x, p.position.y - p.lastPosition.y);
    reverseDirection = [vectorSupport normalize:reverseDirection];
    reverseDirection = [vectorSupport scalarMultiply:reverseDirection andScalar:-20];
    reverseDirection = [vectorSupport add:CGVectorMake(p.position.x, p.position.y) andVector2:reverseDirection];
    [player removeFromParent];
    player.position = CGPointMake(reverseDirection.dx, reverseDirection.dy);
    [self addChild:player];
    ignoreTouchesCollision = YES;
}

-(void)changeScene{
    SKAction *runBlock = [SKAction runBlock:^{
        InterstatialGameScene *s = (InterstatialGameScene *)[SKScene nodeWithFileNamed:@"InterstatialGameScene"];
        s.scaleMode = SKSceneScaleModeAspectFill;
        SKView *skView = (SKView *)self.view;
        s.numberOfBullets = self->numberOfBulletsFired;
        s.numberOfEnemiesHit = self->enemiesHit;
        s.numberOfPlayerHits = self->numberOfTimesHit;
        s.showStats = YES;
        s.nextLevel = @"GameScene";
        [skView presentScene:s];
    }];
    SKAction *wait = [SKAction waitForDuration:0.5];
    SKAction *sequence = [SKAction sequence:@[wait, runBlock]];
    [self runAction:sequence];
}

-(void)updateLevelStat:(NSString*)statName andValue:(float)value{
    if ([statName isEqualToString:@"numberOfTimesHit"]){
        numberOfTimesHit += (int)value;
        return;
    }
    if ([statName isEqualToString:@"enemiesHit"]){
        enemiesHit += value;
        return;
    }
    else
        NSLog(@"GameSceneL2::updateLevelStat stat not found %@", statName);
}

-(void)processPickup:(SKNode *)pickup{
    for (SceneWall *s in removableWalls)
        [s remove];
    for (SKSpriteNode *s in removableWallSprites)
         [s removeFromParent];
    [pickupObject removeFromParent];
    [emitter2 removeFromParent];
}

#pragma mark dealloc
-(void)dealloc{
//    constants.waveCounter = 0;
//    constants.numberOfWaveCycles = 0;
//    constants.numberOfWaves = 0;
//    constants.timeInGame = 0;
//    constants.timeInWave = 0;
    currentTouch = nil;
    [self removeAllChildren];
    [self removeAllActions];
    NSLog(@"Game Scene L3 dealloc");
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
