//
//  GameScene.h
//  Escape: A Space Shooter
// Copyright (c) 2019, 2020 Imaginary Root Studio LLC. All rights reserved.


#import <SpriteKit/SpriteKit.h>
//#import "MultiplayerNetworking.h"
#import "Constants.h"
#import "TapticEngine.h"
#import "Player.h"
#import "ScreenWalls.h"
#import "VectorSupport.h"
#import "Bullet.h"
#import "ShooterBase.h"
#import "WaypointNavigation.h"
#import "CameraController.h"
#import "PlayerBullet.h"
#import "KeyObject.h"
#import "InterstatialGameScene.h"
#import "CollisionResponse.h"


//TODO: For multiplayer, add the following:
// MultiplayerNetworkingProtocol

@interface GameScene : SKScene <SKPhysicsContactDelegate, UIGestureRecognizerDelegate, CollisionResponseProtocol>

//@property /*(nonatomic, strong)*/ MultiplayerNetworking *networkEngine;
@property float timeInGame;

@end
