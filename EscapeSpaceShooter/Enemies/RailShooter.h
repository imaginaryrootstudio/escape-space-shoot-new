//
//  RailShoter.h
//  EscapeSpaceShooter
//
//  Created by William Birmingham on 11/30/19.
//  Copyright © 2019, 2020 Imaginary Root Studio LLC. All rights reserved.
//

#ifndef RailShooter_h
#define RailShooter_h
#import <Foundation/Foundation.h>
#import "Constants.h"
#import "ShooterBase.h"
#import "Player.h"
#import "TargetedShootBase.h"


@interface RailShooter : TargetedShootBase


-(id)init:(CGVector) shootDir andCoolDown:(float)time andPlayer:(Player*)player;
-(void)loadWayPoints:(NSArray*)waypoints;
-(void)moveRailShooter;
-(void)stopRailShooter;
-(void) takeDamage:(float)damageAmount;

@end


#endif /* RailShooter_h */
