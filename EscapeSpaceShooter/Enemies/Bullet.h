//
//  Bullet.h
//  EscapeSpaceShooter
//
//  Created by William Birmingham on 10/28/19.
//  Copyright © 2019, 2020 Imaginary Root Studio LLC. All rights reserved.
//

#ifndef Bullet_h
#define Bullet_h
#import <SpriteKit/SpriteKit.h>
#import "Constants.h"
#import "VectorSupport.h"
#import <AVFoundation/AVFoundation.h>

@interface Bullet : SKSpriteNode

@property int32_t damage;
@property double bulletSpeed;
@property SKAudioNode *audioNode;

-(id) init:(CGVector)direction andBulletLifetime:(double)lifeTime andBulletType:(BulletType)btype;
-(id)init:(CGVector)direction andBulletLifetime:(double)lifeTime andBulletType:(BulletType)btype andBulletSpeed:(float)bulletSpeed;
-(void)update:(float)dt;
-(void)remove;
-(void)takeDamage:(float)damageReceived;

@end

#endif /* Bullet_h */
