//
//  Bomb.h
//  EscapeSpaceShooter
//
//  Created by William Birmingham on 3/17/20.
//  Copyright © 2020 William Birmingham. All rights reserved.
//

#ifndef Bomb_h
#define Bomb_h

#import <Foundation/Foundation.h>
#import "Constants.h"
#import "VectorSupport.h"
#import "Player.h"

typedef NS_ENUM(NSInteger, BombType) {
    kNoBombType,
    kTimedBombType,
    kProximityBombType
};

@interface Bomb : SKSpriteNode

-(id)init:(CGPoint)location /*andType:(BombType)bombType*/ andPlayer:(Player*)player; //timed bomb
-(id)init:(CGPoint)location andPlayer:(Player*)player andTime:(float)timer; //proxmity bomb
-(void) setTime;
-(void) remove;
//-(void) takeDamage:(float)damage;

@end

#endif /* Bomb_h */
