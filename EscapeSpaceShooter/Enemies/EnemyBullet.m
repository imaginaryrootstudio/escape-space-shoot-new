//
//  EnemyBullet.m
//  EscapeSpaceShooter
//
//  Created by William Birmingham on 11/16/19.
//  Copyright © 2019, 2020 Imaginary Root Studio LLC. All rights reserved.
//

#import "EnemyBullet.h"

@implementation EnemyBullet{
    Constants *constants;
    CGVector dir;
    float speed;
    VectorSupport *vectorSupport;
}
-(id)init:(CGVector)direction{
    if (self = [super init:direction andBulletLifetime:gBulletLifetime andBulletType:kSimpleBulletType]){   //@"player_outline.png"]){
        self.damage = 1;
        self.physicsBody.dynamic = YES;
        self.texture = [SKTexture textureWithImageNamed:@"Enemy2.png"];
        self.audioNode = [[SKAudioNode alloc]initWithFileNamed:@"EnemyPewPew.mp3"];
        self.audioNode.autoplayLooped = NO;
        self.audioNode.positional = YES;
        [self addChild:self.audioNode];
        
        SKLightNode *light = [[SKLightNode alloc]init];
        light.categoryBitMask = 0x1;
        light.ambientColor = [SKColor whiteColor];
        [self addChild:light];
       // self.lightingBitMask = 0x1;
    }
    return self;
}


-(void)dealloc{
    
}

@end
