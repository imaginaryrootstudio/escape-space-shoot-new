//
//  Bomb.m
//  EscapeSpaceShooter
//
//  Created by William Birmingham on 3/17/20.
//  Copyright © 2020 William Birmingham. All rights reserved.
//

#include "Bomb.h"

@implementation Bomb {
    Constants *constants;
    VectorSupport *vectorSupport;
    Player *thePlayer;
    float health;
    SKEmitterNode *particleFX;
    //for controlling fxs
    bool shaking;
    bool exploding;
    float bombTimer;
    bool allowedToStartTimer;
}

//init the timed bomb
-(id)init:(CGPoint)location andPlayer:(Player*)player andTime:(float)timer{
    if (self = [super initWithImageNamed:@"Bomb.png"]){
        constants = [Constants getInstance];
        vectorSupport = [[VectorSupport alloc]init];
        health = 2.0;
        thePlayer = player;
        
        self.position = location;
        self.name = [NSString stringWithFormat:@"Bomb_%f", [[NSDate date] timeIntervalSince1970]];
        //self.lightingBitMask = 0x100;
        self.color = [SKColor whiteColor];
        self.zPosition = gPlayerZPosition;
        [self setScale: 0.5];
        
        self.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:self.size];
        self.physicsBody.categoryBitMask = constants.bombCollisionCategory;
        self.physicsBody.contactTestBitMask = constants.playerCollisionCategory | constants.playerCollisionCategory;
        self.physicsBody.collisionBitMask = 0x00;
        self.physicsBody.dynamic = YES;
        self.physicsBody.usesPreciseCollisionDetection = NO;
        self.physicsBody.restitution = 0.0;
        self.physicsBody.angularDamping = 1.0;
        self.physicsBody.friction = 0.0;
        self.physicsBody.linearDamping = 0.0;
        self.physicsBody.affectedByGravity = NO;
        self.physicsBody.mass = 0.00001;
//        SKAction *a = [self explosionSequence];
//        SKAction *w = [SKAction waitForDuration:2];
//        SKAction *s = [SKAction sequence:@[w,a]];
        bombTimer = timer;
        particleFX = [[SKEmitterNode alloc]init];
        NSString *fxTrail = [[NSBundle mainBundle] pathForResource:@"Bomb" ofType:@"sks"];
        particleFX = [NSKeyedUnarchiver unarchiveObjectWithFile:fxTrail];
        if (particleFX == nil)
            NSLog(@"Bomb::init can't load bomb particle emitter");
        particleFX.targetNode = self.scene;
        particleFX.name =  [NSString stringWithFormat:@"bomb_%f", [[NSDate date] timeIntervalSince1970]];
        shaking = NO;
        exploding = NO;
        allowedToStartTimer = NO;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setStartTimer) name:@"CameraFollowingPlayer" object:nil];
    }
    return self;
}

//init the proxmity bomb
-(id)init:(CGPoint)location andPlayer:(Player*)player{
    if (self = [super initWithImageNamed:@"Bomb.png"]){
        constants = [Constants getInstance];
        vectorSupport = [[VectorSupport alloc]init];
        health = 2.0;
        thePlayer = player;
        
        self.position = location;
        self.name = [NSString stringWithFormat:@"Bomb_%f", [[NSDate date] timeIntervalSince1970]];
        //self.lightingBitMask = 0x100;
        self.color = [SKColor whiteColor];
        self.zPosition = gPlayerZPosition;
        [self setScale: 0.5];
        
        self.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:self.size];
        self.physicsBody.categoryBitMask = constants.bombCollisionCategory;
        self.physicsBody.contactTestBitMask = constants.playerCollisionCategory | constants.playerCollisionCategory;
        self.physicsBody.collisionBitMask = 0x00;
        self.physicsBody.dynamic = YES;
        self.physicsBody.usesPreciseCollisionDetection = NO;
        self.physicsBody.restitution = 0.0;
        self.physicsBody.angularDamping = 1.0;
        self.physicsBody.friction = 0.0;
        self.physicsBody.linearDamping = 0.0;
        self.physicsBody.affectedByGravity = NO;
        self.physicsBody.mass = 0.00001;
//        SKAction *a = [self explosionSequence];
//        SKAction *w = [SKAction waitForDuration:2];
//        SKAction *s = [SKAction sequence:@[w,a]];
        bombTimer = 0.75;
        [self createExplosionProximity];

        particleFX = [[SKEmitterNode alloc]init];
        NSString *fxTrail = [[NSBundle mainBundle] pathForResource:@"Bomb" ofType:@"sks"];
        particleFX = [NSKeyedUnarchiver unarchiveObjectWithFile:fxTrail];
        if (particleFX == nil)
            NSLog(@"Bomb::init can't load bomb particle emitter");
        particleFX.targetNode = self.scene;
        particleFX.name = @"particleFX";
        shaking = NO;
        exploding = NO;
        allowedToStartTimer = NO;
       // [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setStartTimer) name:@"CameraFollowingPlayer" object:nil];
    }
    return self;
}

-(void)setStartTimer{
    allowedToStartTimer = YES;
    SKAction *a = [self explosionSequence:YES];
    SKAction *w = [SKAction waitForDuration:2];
    SKAction *s = [SKAction sequence:@[w,a]];
    [self runAction:s];
}

-(SKAction*)explosionSequence:(bool)addExtraTime{
    int extra;
    if (addExtraTime)
        extra = arc4random_uniform(15);
    else
        extra = 0;
    SKAction *scale = [SKAction scaleBy:1.7 duration:2];
    SKAction *fadeOut = [SKAction fadeAlphaTo:0.4 duration:1.5];
    SKAction *particles = [SKAction runBlock:^{
        Bomb* t = (Bomb*)[self->particleFX parent];
        if (t != nil){
            [self->particleFX removeFromParent];
            NSLog(@"Parent %@", t.name);
        }
        [self addChild:self->particleFX];
    }];
    SKAction *g = [SKAction group:@[scale, fadeOut, particles]];
    SKAction *shake = [SKAction runBlock:^{
        [self bombShake:1.25 amplitudeX:20 amplitudeY:20];
    }];

    SKAction *w = [SKAction waitForDuration:bombTimer + extra];
    SKAction *remove = [SKAction runBlock:^{
        [self remove];
    }];
//    SKAction *group = [SKAction group:@[g, particles]];
    SKAction *s = [SKAction sequence:@[w, shake, g, remove]];
    return s;
}

-(void)createExplosionTimed{
    SKAction *remove = [SKAction runBlock:^{
        [self remove];
    }];
    SKAction *seq = [SKAction sequence:@[[self explosionSequence:YES],remove]];
    [self runAction:seq];
}

-(void)createExplosionProximity{
    SKAction *distance = [SKAction runBlock:^{
        float d = [self->vectorSupport CGPointDistance:self->thePlayer.position andPoint2:self.position];
        if (self->shaking && d >= 200){
            self->shaking = NO;
        }
        if (d < 150 && !self->shaking){
            [self bombShake:.25 amplitudeX:15 amplitudeY:15];
            self->shaking = YES;
        }
        if (d < 120 && !self->exploding){
            //            NSLog(@"Distance %f Player pos %f %f",d, self->thePlayer.position.x, self->thePlayer.position.y);
            [self removeAllActions];
            SKAction *e = [self explosionSequence:NO];
            [self runAction:e];
            self->exploding = YES;
        }
    }];
    SKAction *wait = [SKAction waitForDuration:0.1];
    SKAction *g = [SKAction group:@[wait, distance]];
    SKAction *forever = [SKAction repeatActionForever:g];
    [self runAction: forever];
    
}

-(void) bombShake:(float) duration amplitudeX:(float) amplitudeX amplitudeY:(float) amplitudeY{
    float numShakes = duration*2/0.02;
    NSMutableArray *actions = [[NSMutableArray alloc]init];
    for (int i = 0; i<numShakes; i++) {
        float dx = arc4random_uniform(amplitudeX)-amplitudeX/2;
        float dy = arc4random_uniform(amplitudeY)-amplitudeY/2;
        SKAction *foo = [SKAction moveByX:dx y:dy duration:0.02];
        SKAction *reversedFoo = [foo reversedAction];
        [actions addObject:foo];
        [actions addObject:reversedFoo];
    }
    SKAction *shrink = [SKAction scaleBy:0.8 duration:0.1];
    SKAction *reverseShrink = [shrink reversedAction];
    SKAction *shrinkSequence = [SKAction sequence:@[shrink,reverseShrink]];
    SKAction *seq = [SKAction sequence:actions];
    SKAction *group = [SKAction group:@[shrinkSequence, seq]];
    [self runAction:group completion:^{
        self->shaking = NO;
    }];
}
-(void)remove{
    [self removeAllActions];
    [self removeAllChildren];
    [self removeFromParent];
}

-(void)dealloc{
//    NSLog(@"Bomb dealloc");
}
@end
