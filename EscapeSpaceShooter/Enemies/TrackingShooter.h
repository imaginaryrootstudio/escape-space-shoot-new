//
//  TrackingShooter.h
//  EscapeSpaceShooter
//
//  Created by William Birmingham on 12/8/19.
//  Copyright © 2019, 2020 Imaginary Root Studio LLC. All rights reserved.
//

#ifndef TrackingShooter_h
#define TrackingShooter_h

#import <Foundation/Foundation.h>
#import "Constants.h"
#import "Player.h"
#import "TargetedShootBase.h"


@interface TrackingShooter : TargetedShootBase


-(id)init:(CGVector) shootDir andCoolDown:(float)time andPlayer:(Player*)player;
-(void)moveTrackingShooter;
-(void)stopTrackingShooter;
-(void)setMoveDirection:(CGVector)d;

@end

#endif /* TrackingShooter_h */
