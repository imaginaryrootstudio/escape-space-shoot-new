//
//  TargetedShootBase.h
//  EscapeSpaceShooter
//
//  Created by William Birmingham on 11/19/19.
//  Copyright © 2019, 2020 Imaginary Root Studio LLC. All rights reserved.
//

#ifndef TargetedShootBase_h
#define TargetedShootBase_h

#import <Foundation/Foundation.h>
#import "Constants.h"
//#import "Bullet.h"
#import "VectorSupport.h"
//#import "EnemyBullet.h"
#import "ShooterBase.h"
#import "Player.h"


@interface TargetedShootBase : ShooterBase


-(id)init:(CGVector) shootDir andCoolDown:(float)time andPlayer:(Player*)player;

@end
#endif /* TargetedShootBase_h */
