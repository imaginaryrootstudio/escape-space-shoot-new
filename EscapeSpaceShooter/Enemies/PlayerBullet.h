//
//  PlayerBullet.h
//  EscapeSpaceShooter
//
//  Created by William Birmingham on 12/14/19.
//  Copyright © 2019, 2020 Imaginary Root Studio LLC. All rights reserved.
//

#ifndef PlayerBullet_h
#define PlayerBullet_h

#import <Foundation/Foundation.h>
#import <SpriteKit/SpriteKit.h>
#import "Constants.h"
#import "VectorSupport.h"
#import "Bullet.h"



@interface PlayerBullet : Bullet

-(id)init:(CGVector)direction;
//-(id)init:(CGPoint)location andDirection:(CGVector)velocity;

//-(NSInteger)damage;
//-(void) update:(float) dt;
-(void) remove;
@end

#endif /* PlayerBullet_h */
