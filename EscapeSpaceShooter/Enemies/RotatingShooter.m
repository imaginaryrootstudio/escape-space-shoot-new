//
//  RotatingShooter.m
//  EscapeSpaceShooter
//
//  Created by William Birmingham on 1/29/20.
//  Copyright © 2020 William Birmingham. All rights reserved.
//

#import "RotatingShooter.h"
@implementation RotatingShooter{
    Constants *constants;
    VectorSupport *vectorSupport;
//    Player *p;
//    CGVector dir;
    float rotation;
    float rotationRate;
    float coolDownT;
}

-(id)init:(float)coolDownTime andRotationRate:(float)rate{
//-(id)init:(CGVector) shootDir andCoolDown:(float)time andPlayer:(Player *)player{
    if (self = [super init:CGVectorMake(1, 0) andCoolDown:coolDownTime andBulletType:kSimpleBulletType]){
        constants = [Constants getInstance];
        vectorSupport = [[VectorSupport alloc]init];
        rotation = 0;
        coolDownT = coolDownTime;
        rotationRate = rate;
        [self rotateShooter];
    }
    return self;
}

-(void)rotateShooter{
    SKAction *runBlock = [SKAction runBlock:^{
        self->rotation += self->rotationRate;
        if (self->rotation > 360)
            self->rotation = 0;
        //self->rotation = (float)t;
        self.zPosition = self->rotation;
        float x = sinf(self->rotation);
        float y = cosf(self->rotation);
        [self setShootDirection:CGVectorMake(x, y)];
    }];
    
    SKAction *sleep = [SKAction waitForDuration:0.15];
    SKAction *sequence = [SKAction sequence:@[runBlock,sleep]];
    SKAction *run = [SKAction repeatActionForever:sequence];
    [self runAction:run];
}

-(void)stopRotatingShooter{
    rotation = 0;
}
@end
