//
//  MovementBlockingWall.m
//  EscapeSpaceShooter
//
//  Created by William Birmingham on 4/12/20.
//  Copyright © 2020 William Birmingham. All rights reserved.
//

#import "MovementBlockingWall.h"
@implementation MovementBlockingWall{
    Constants *constants;
}

-(id)init{
    if (self = [super initWithImageNamed:@"Blue3Purple3 Circle.png"]){   //@"player_outline.png"]){
        self.normalTexture = [SKTexture textureWithImageNamed:@"Blue3Purple3 Circle_n.png"];
        self.lightingBitMask = 0x1;
        constants = [Constants getInstance];
        self.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize: self.size];
        self.physicsBody.categoryBitMask = constants.movementBlockingWallCategory;
        self.physicsBody.contactTestBitMask = constants.playerCollisionCategory | constants.enemyCollisionCategory;
//        NSLog(@"SceneWall category %i %i", self.physicsBody.categoryBitMask, constants.sceneWallCollisionCategory);
        self.physicsBody.collisionBitMask = 0x0;
        self.physicsBody.dynamic = YES;
        self.physicsBody.usesPreciseCollisionDetection = NO;
        self.physicsBody.restitution = 0;
        self.physicsBody.angularDamping = 1;
        self.physicsBody.linearDamping = 1;
        self.physicsBody.friction = 0;
        self.physicsBody.allowsRotation = NO;
        self.physicsBody.angularVelocity = 0;
        self.physicsBody.mass = 100;
//        self.physicsBody.density = 10;
        
        self.colorBlendFactor = gPlayerColorBlendFactor;
        self.color = [UIColor whiteColor];
        self.zPosition = 0.1;
        [self setScale:0.15];
    }
    return self;
}

-(void)remove{
    [self removeFromParent];
}

-(void)dealloc{
//    NSLog(@"MovementBlockingWall dealloc");
}

@end
