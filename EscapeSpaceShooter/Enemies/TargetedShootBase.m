//
//  TargetedShootBase.m
//  EscapeSpaceShooter
//
//  Created by William Birmingham on 11/19/19.
//  Copyright © 2019, 2020 Imaginary Root Studio LLC. All rights reserved.
//

//IMPORTANT: in the .sks file, the base sprite must have it zRotation = 0!!!!!!!!
//          Must face  ->  direction
//

#import <Foundation/Foundation.h>
#import "TargetedShootBase.h"

@implementation TargetedShootBase{
    Constants *constants;
    VectorSupport *vectorSupport;
    Player *p;
    CGVector dir;
}

-(id)init:(CGVector) shootDir andCoolDown:(float)time andPlayer:(Player *)player{
    if (self = [super init:shootDir andCoolDown:time  andBulletType: kSimpleBulletType]){
        constants = [Constants getInstance];
        vectorSupport = [[VectorSupport alloc]init];
        dir = shootDir;
        p = player;
        [self createAimSequence];
    }
    return self;
}

-(id)init{
    if (self = [super init]){ // [super initWithImageNamed:@"enemy.png"]){
        constants = [Constants getInstance];
        self.name = [NSString stringWithFormat:@"Shooter_%f", [[NSDate date] timeIntervalSince1970]];
        self.zPosition = gPlayerZPosition;
        vectorSupport = [[VectorSupport alloc]init];
        self.coolDownTime = gGunNormalCoolDown;
    }
    return self;
}

-(void)createAimSequence{
    SKAction *runBlock = [SKAction runBlock:^{
        CGPoint foo = [self parent].position;
        self->dir = CGVectorMake(self->p.position.x - foo.x, self->p.position.y - foo.y);
        self->dir = [self->vectorSupport normalize:self->dir];
        [super setShootDirection:self->dir];
    }];
    SKAction *wait = [SKAction waitForDuration:gGunDirectionCalcPeriod];
    SKAction *seq = [SKAction sequence:@[runBlock, wait]];
    [self runAction:[SKAction repeatActionForever:seq]];
}

-(void)update:(float) dt{
}


-(void)dealloc{
//    NSLog(@"Shooter dealloc");
}

@end
