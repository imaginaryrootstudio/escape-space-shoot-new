//  ShooterBase.m
//  EscapeSpaceShooter
//
//  Created by William Birmingham on 11/11/19.
//  Copyright © 2019, 2020 Imaginary Root Studio LLC. All rights reserved.
//

//NOTE: This class depends on a Enemy parent
#include "ShooterBase.h"
@implementation ShooterBase{
    Constants *constants;
    VectorSupport *vectorSupport;
    CGVector shootDirection;
    bool pauseShooting;
    BulletType bulletType;
    bool dontShoot;
    bool shaking;
    SKLightNode* light;
    SKEmitterNode *enemyHitFX;
    bool playingEnemyHitFX;
    NSString *fxTrail;
}

-(id)init:(CGVector) shootDir andCoolDown:(float)time andBulletType:(BulletType)bType{
    if (self = [super init]){
        shootDirection = shootDir;
        playingEnemyHitFX = NO;
        self.coolDownTime = time;
        [self createShootSequence];
        bulletType = bType;
        self.name = @"shooterBase";
        self.health = gEnemyHealth;
        dontShoot = NO;
        shaking = NO;
#pragma mark lighting
        light = [[SKLightNode alloc]init];
        light.categoryBitMask = 0x00000001;
        light.lightColor = [UIColor darkGrayColor];
        light.ambientColor = [UIColor darkGrayColor];
        light.position = self.position;
        light.falloff = 1;
        [self addChild:light];
//               fxTrail = [[NSBundle mainBundle] pathForResource:@"Enemy" ofType:@"sks"];
//                   enemyHitFX = [NSKeyedUnarchiver unarchiveObjectWithFile:fxTrail];
//                   if (enemyHitFX == nil)
//                       NSLog(@"GameSceneL0::EnemyDamageFX can't load player particle emitter");
//                   //enemyHitFX.targetNode = self->constants.currentScene;
//                   enemyHitFX.name = @"enemyHitFX";
//                   enemyHitFX.xScale = 1.5;
//                   enemyHitFX.yScale = 1.5;
//        [self addChild:enemyHitFX];
    }
    return self;
}

-(id)init{
    if (self = [super init]){
        shootDirection = CGVectorMake(1, 0);
        self.coolDownTime = gGunNormalCoolDown;
        [self initializeShooter];
    }
    return self;
}

-(void)initializeShooter{
    constants = [Constants getInstance];
    self.name = [NSString stringWithFormat:@"Shooter_%f", [[NSDate date] timeIntervalSince1970]];
    self.zPosition = gPlayerZPosition;
    vectorSupport = [[VectorSupport alloc]init];
    [self createShootSequence];
#pragma mark lighting
    light = [[SKLightNode alloc]init];
    light.categoryBitMask = 0x00000001;
    light.lightColor = [UIColor whiteColor];
    light.ambientColor = [UIColor whiteColor];
    light.position = self.position;
    light.falloff = 0.01;
    [self addChild:light];
    
}
-(void)setShootDirection:(CGVector)direction{
    shootDirection = direction;
}

-(void)createShootSequence{
    SKAction *shoot = [SKAction runBlock:^{
        [self shoot];
    }];
    SKAction *wait = [SKAction waitForDuration: self.coolDownTime];
    SKAction *shootSequence = [SKAction sequence:@[shoot, wait]];
    [self runAction:[SKAction repeatActionForever:shootSequence]];
}

-(void)shoot{
    if (dontShoot)
        return;
    EnemyBullet *eb;
    HeavyBullet *hb;
    switch (bulletType) {
        case kSimpleBulletType:
            eb = [[EnemyBullet alloc]init:shootDirection]; //CGVectorMake(shootDirection.dy, -shootDirection.dx)];
            eb.position = self.position;
            [eb setScale:0.6];
            eb.zRotation =  -atan2((double)shootDirection.dx, (double)shootDirection.dy);// self.zRotation;
            [self addChild:eb];
            break;
        case kHeavyBulletType:
            hb = [[HeavyBullet alloc]init: shootDirection];
            hb.position = self.position;
            [hb setScale:0.6];
            hb.zRotation = -atan2((double)shootDirection.dx, (double)shootDirection.dy);//shootDirection;// self.zRotation;
            [self addChild:hb];
        default:
            break;
    }
}

-(void)deleteSelf{
    [self removeAllActions];
    [self removeAllChildren];
    [self removeFromParent];
}

-(void)interruptShooting{
    dontShoot = YES;
    SKAction *wait = [SKAction waitForDuration:1];
    SKAction *restore = [SKAction runBlock:^{
        self.health = gEnemyHealth;// self->fullHealth;
        self->dontShoot = NO;
    }];
    SKAction *sequence = [SKAction sequence:@[wait, restore]];
    [self runAction:sequence];
}

-(void)stopShooting{
    [self removeAllActions];
    SKSpriteNode *shooterSprite = (SKSpriteNode*)[self parent];
    if (shooterSprite == nil){
        NSLog(@"ShooterBase::stopShooting turret has no parent");
        return;
    }
    SKAction *fade = [SKAction fadeAlphaTo:0.6 duration:1.5];
    [shooterSprite runAction:fade];
}

-(void)takeDamage:(float)damageAmount andStopOnZeroHealth:(bool)stop{
    self.health -= damageAmount;
    if (!playingEnemyHitFX)
        [self playHitDamageFX];
    [self shooterShake:0.15 amplitudeX:15 amplitudeY:15];
    if (self.health <= 0){
        if (stop)
            [self stopShooting];
        else
            [self interruptShooting];
    }
}

-(void) playHitDamageFX{
    SKAction *addParticles = [SKAction runBlock:^{
        self->fxTrail = [[NSBundle mainBundle] pathForResource:@"Enemy" ofType:@"sks"];
        self->enemyHitFX = [NSKeyedUnarchiver unarchiveObjectWithFile:self->fxTrail];
        if (self->enemyHitFX == nil)
            NSLog(@"GameSceneL0::playEnemyDamageFX can't load player particle emitter");
        self->enemyHitFX.targetNode = self->constants.currentScene;
        self->enemyHitFX.name = @"enemyHitFX";
        self->enemyHitFX.xScale = 1.5;
        self->enemyHitFX.yScale = 1.5;
        [self addChild:self->enemyHitFX];
        self->playingEnemyHitFX = YES;
    }];
    SKAction *removeParticles = [SKAction runBlock:^{
        self->enemyHitFX.targetNode = nil;
        [self->enemyHitFX removeFromParent];
        self->playingEnemyHitFX = NO;
        self->enemyHitFX = nil;
//        [self removeAllActions];
    }];
    
    SKAction *waits = [SKAction waitForDuration:1.5];
    SKAction *sequence = [SKAction sequence:@[addParticles, waits, removeParticles]];
    SKSpriteNode *parent =(SKSpriteNode*) [self parent];
    [parent runAction:sequence completion:^{
        self->playingEnemyHitFX = NO;
    }];
}


-(void)incrementHealth:(float)healAmount{
    self.health += healAmount;
}
-(void) shooterShake:(float) duration amplitudeX:(float) amplitudeX amplitudeY:(float) amplitudeY{
    float numShakes = duration*2/0.02;
    NSMutableArray *actions = [[NSMutableArray alloc]init];
    for (int i = 0; i<numShakes; i++) {
        float dx = arc4random_uniform(amplitudeX)-amplitudeX/2;
        float dy = arc4random_uniform(amplitudeY)-amplitudeY/2;
        SKAction *foo = [SKAction moveByX:dx y:dy duration:0.02];
        SKAction *reversedFoo = [foo reversedAction];
        [actions addObject:foo];
        [actions addObject:reversedFoo];
    }
    SKAction *shrink = [SKAction scaleBy:0.8 duration:0.1];
    SKAction *reverseShrink = [shrink reversedAction];
    SKAction *shrinkSequence = [SKAction sequence:@[shrink,reverseShrink]];
    SKAction *seq = [SKAction sequence:actions];
    SKAction *group = [SKAction group:@[shrinkSequence, seq]];
    SKSpriteNode *parent =(SKSpriteNode*) [self parent];
    [parent runAction:group completion:^{
        self->shaking = NO;
    }];
}

-(void)update:(float) dt{
}

-(void)dealloc{
    //    NSLog(@"Shooter dealloc");
}

@end
