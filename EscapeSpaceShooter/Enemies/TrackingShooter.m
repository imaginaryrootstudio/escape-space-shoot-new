//
//  TrackingShooter.m
//  EscapeSpaceShooter
//
//  Created by William Birmingham on 12/8/19.
//  Copyright © 2019, 2020 Imaginary Root Studio LLC. All rights reserved.
//

#import "TrackingShooter.h"

@implementation TrackingShooter{
    Constants *constants;
    VectorSupport *vectorSupport;
    Player *p;
    CGVector dir;
    float targetRotation;
}

-(id)init:(CGVector) shootDir andCoolDown:(float)time andPlayer:(Player *)player{
    if (self = [super init:shootDir andCoolDown:time andBulletType:kSimpleBulletType]){
        constants = [Constants getInstance];
        vectorSupport = [[VectorSupport alloc]init];
        dir = shootDir;
        p = player;

    }
    return self;
}

-(void)calculateNewDirection{
//NSLog(@"Currentnode x %f y %f", currentNode.position.x, currentNode.position.y);
    CGPoint parentPosition = [self parent].position;
    dir = CGVectorMake(parentPosition.x - p.position.x, parentPosition.y - p.position.y);
    dir.dx *= -1;
    dir.dy *= -1;
    dir = [vectorSupport normalize:dir];
}

-(void)setMoveDirection:(CGVector)d{
    dir = d;
}

-(void)moveTrackingShooter{
//    NSLog(@"Move Tracking Shooter");
    [self calculateNewDirection];
    [super setShootDirection:dir];
    SKAction *runBlock = [SKAction runBlock:^{
        [self calculateNewDirection];
        [super setShootDirection:self->dir];
        CGVector deltaPosition = self->dir;
        deltaPosition = [self->vectorSupport scalarMultiply:deltaPosition andScalar:1.5];
        deltaPosition = [self->vectorSupport add:deltaPosition andVector2:CGVectorMake([self parent].position.x,
                                                                                       [self parent].position.y)];// //CGVectorMake(self.position.x, self.position.y)];
        [self parent].position = CGPointMake(deltaPosition.dx, deltaPosition.dy);
//        [self parent].zRotation = [vectorSupport lerp:[self parent].zRotation andPoint2:targetRotation andWeight:0.1];
    }];
    
    SKAction *sleep = [SKAction waitForDuration:0.05];
    SKAction *sequence = [SKAction sequence:@[runBlock,sleep]];
    SKAction *run = [SKAction repeatActionForever:sequence];
    [self runAction:run];
}

-(void)stopTrackingShooter{
    dir = CGVectorMake(0, 0);
}




@end
