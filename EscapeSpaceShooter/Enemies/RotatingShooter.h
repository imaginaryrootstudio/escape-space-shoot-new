//
//  RotatingShooter.h
//  EscapeSpaceShooter
//
//  Created by William Birmingham on 1/29/20.
//  Copyright © 2020 William Birmingham. All rights reserved.
//

#ifndef RotatingShooter_h
#define RotatingShooter_h

#import <Foundation/Foundation.h>
#import "Constants.h"
#import "Player.h"
#import "ShooterBase.h"


@interface RotatingShooter : ShooterBase


-(id)init:(float)coolDownTime andRotationRate:(float)rate;
-(void)stopRotatingShooter;


@end
#endif /* RotatingShooter_h */
