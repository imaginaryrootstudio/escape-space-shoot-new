//
//  PlayerBullet.m
//  EscapeSpaceShooter
//
//  Created by William Birmingham on 12/14/19.
//  Copyright © 2019, 2020 Imaginary Root Studio LLC. All rights reserved.
//

#import "PlayerBullet.h"
@implementation PlayerBullet{
    Constants *constants;
    CGVector dir;
    float speed;
    VectorSupport *vectorSupport;
    SKEmitterNode *trailFX;
}
-(id)init:(CGVector)direction{
    if (self = [super init:direction andBulletLifetime:gBulletLifetime andBulletType:kPlayerBulletType]){   //@"player_outline.png"]){
        constants = [Constants getInstance];
        self.damage = 1;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(remove)
                                                     name: @"RemoveBullet" object:nil];
        self.audioNode = [[SKAudioNode alloc]initWithFileNamed:gPlayerShooterSoundFX]; //@"PewPew.mp3"];
        self.audioNode.autoplayLooped = NO;
        self.audioNode.positional = YES;
        [self addChild:self.audioNode];
        SKAction *play = [SKAction play];
        SKAction *stop = [SKAction stop];
        SKAction *wait = [SKAction waitForDuration:0.25];
        SKAction *seq = [SKAction sequence:@[play, wait, stop]];
        [self.audioNode runAction:seq];
        trailFX = (SKEmitterNode*)[self childNodeWithName:@"bulletTrailFX"];
        if (trailFX == NULL)
            NSLog(@"PlayerBullet:: couldn't find bullet trail");
        else
            [trailFX removeFromParent];
        trailFX = [[SKEmitterNode alloc]init];
        NSString *fxTrail = [[NSBundle mainBundle] pathForResource:@"PlayerBulletTrail" ofType:@"sks"];
        trailFX = [NSKeyedUnarchiver unarchiveObjectWithFile:fxTrail];
        if (trailFX == nil)
            NSLog(@"PlayerBullet::initializedScene can't load player particle emitter");
        trailFX.targetNode = constants.currentScene;
        trailFX.name = @"PlayerBulletTrailFX";
        [self addChild:trailFX];
        
        SKLightNode *light = [[SKLightNode alloc]init];
        light.categoryBitMask = 0x10;
        light.ambientColor = [SKColor whiteColor];
        light.falloff = 0.25;
        [self addChild:light];
    }
    return self;
}
-(void)remove{
    [self removeAllActions];
    [self removeFromParent];
}
-(void)dealloc{
    
}

@end
