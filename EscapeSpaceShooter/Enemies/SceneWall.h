//
//  SceneWall.h
//  EscapeSpaceShooter
//
//  Created by William Birmingham on 12/9/19.
//  Copyright © 2019, 2020 Imaginary Root Studio LLC. All rights reserved.
//

#ifndef SceneWall_h
#define SceneWall_h

#import <Foundation/Foundation.h>
#import "Constants.h"

@interface SceneWall : SKSpriteNode

-(id)init ;
-(void)remove;

@end
#endif /* SceneWall_h */
