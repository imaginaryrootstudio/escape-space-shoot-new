//
//  ShooterBase.h
//  EscapeSpaceShooter
//
//  Created by William Birmingham on 11/11/19.
//  Copyright © 2019, 2020 Imaginary Root Studio LLC. All rights reserved.
//
//  Assumes that this sprite will be a child of an enemy or player

#ifndef ShooterBase_h
#define ShooterBase_h
#import <Foundation/Foundation.h>
#import "Constants.h"
#import "VectorSupport.h"
#import "EnemyBullet.h"
#import "HeavyBullet.h"

@interface ShooterBase : SKSpriteNode

@property float coolDownTime;
@property float health;

-(id)   init;
-(id)   init:(CGVector)shootDir andCoolDown:(float)time andBulletType:(BulletType)bType;
-(void) setShootDirection:(CGVector)direction;
-(void) takeDamage:(float)damageAmount andStopOnZeroHealth:(bool)stop;
-(void) incrementHealth:(float)healAmount;
@end

#endif /* ShooterBase_h */
