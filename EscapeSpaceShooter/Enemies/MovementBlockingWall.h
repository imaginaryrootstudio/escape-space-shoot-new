//
//  MovementBlockingWall.h
//  EscapeSpaceShooter
//
//  Created by William Birmingham on 4/12/20.
//  Copyright © 2020 William Birmingham. All rights reserved.
//

#ifndef MovementBlockingWall_h
#define MovementBlockingWall_h

#import "Constants.h"

@interface MovementBlockingWall : SKSpriteNode

-(id)init ;
-(void)remove;
@end
#endif /* MovementBlockingWall_h */
