//
//  HeavyBullet.m
//  EscapeSpaceShooter
//
//  Created by William Birmingham on 1/19/20.
//  Copyright © 2020 Imaginary Root Studio LLC. All rights reserved.
//

#import "HeavyBullet.h"

@implementation HeavyBullet{
    Constants *constants;
    CGVector dir;
    float speed;
    VectorSupport *vectorSupport;
}
-(id)init:(CGVector)direction{
    if (self = [super init:direction andBulletLifetime:gBulletLifetime * 2.5 andBulletType:kHeavyBulletType]){
//        constants = [Constants getInstance];
        self.texture = [SKTexture textureWithImageNamed:@"enemy_dash.png"];
        self.normalTexture = [SKTexture textureWithImageNamed:@"enemy_dash_n.png"];
//        self.physicsBody.categoryBitMask = constants.enemyBulletCollisionCategory;
//        self.physicsBody.contactTestBitMask = constants.playerCollisionCategory | constants.screenEdgeCollisionCategory | constants.sceneWallCollisionCategory;
        self.damage = 1;
        self.physicsBody.dynamic = YES;
        self.bulletSpeed = gBulletNormalSpeed/2;
        self.xScale = 2.1;
        //Change bullet scale
    }
    return self;
}


-(void)dealloc{
    
}

@end





