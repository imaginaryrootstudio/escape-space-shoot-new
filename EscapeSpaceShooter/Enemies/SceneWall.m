//
//  SceneWall.m
//  EscapeSpaceShooter
//
//  Created by William Birmingham on 12/9/19.
//  Copyright © 2019, 2020 Imaginary Root Studio LLC. All rights reserved.
//

#import "SceneWall.h"
@implementation SceneWall{
    Constants *constants;
}

-(id)init{
    if (self = [super initWithImageNamed:@"Blue3LRDiag.png"]){   //@"player_outline.png"]){
//        self.normalTexture = [SKTexture textureWithImageNamed:@"player_center_n.png"];
        self.lightingBitMask = 0x1;
        constants = [Constants getInstance];
        self.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize: self.size];
        self.physicsBody.categoryBitMask = constants.sceneWallCollisionCategory;
        self.physicsBody.contactTestBitMask = constants.playerCollisionCategory | constants.bulletCollisionCategory |  constants.playerBulletCollisionCategory | constants.enemyBulletCollisionCategory | constants.enemyCollisionCategory;
//        NSLog(@"SceneWall category %i %i", self.physicsBody.categoryBitMask, constants.sceneWallCollisionCategory);
        self.physicsBody.collisionBitMask = 0x0;
        self.physicsBody.dynamic = YES;
        self.physicsBody.usesPreciseCollisionDetection = NO;
        self.physicsBody.restitution = 0;
        self.physicsBody.angularDamping = 1;
        self.physicsBody.linearDamping = 1;
        self.physicsBody.friction = 0;
        self.physicsBody.allowsRotation = NO;
        self.physicsBody.angularVelocity = 0;
        self.physicsBody.mass = 100;
//        self.physicsBody.density = 10;
        
        self.colorBlendFactor = gPlayerColorBlendFactor;
        self.color = [UIColor whiteColor];
        self.zPosition = gPlayerZPosition;
        [self setScale:0.15];
    
    }
    return self;
}
-(void)update:(float)dt{
}

-(void)remove{
    [self removeFromParent];
}

-(void)dealloc{
//    NSLog(@"SceneWall dealloc");
}

@end
