//
//  HeavyBullet.h
//  EscapeSpaceShooter
//
//  Created by William Birmingham on 1/19/20.
//  Copyright © 2020 Imaginary Root Studio LLC. All rights reserved.
//

// Defines an enemy bullet with a longer texture and slower speed. Deals more damage.
#ifndef HeavyBullet_h
#define HeavyBullet_h

#import <Foundation/Foundation.h>
#import <SpriteKit/SpriteKit.h>
#import "Constants.h"
#import "VectorSupport.h"
#import "EnemyBullet.h"


@interface HeavyBullet : EnemyBullet

-(id)init:(CGVector)direction;

@end
#endif /* HeavyBullet_h */
