//
//  Bullet.m
//  EscapeSpaceShooter
//
//  Created by William Birmingham on 10/28/19.
//  Copyright © 2019, 2020 Imaginary Root Studio LLC. All rights reserved.
//

#import "Bullet.h"

@implementation Bullet{
    Constants *constants;
    CGVector dir;
    VectorSupport *vectorSupport;
    float health;
    SKEmitterNode *trailFX;
    SKLightNode *light;
}

-(id)init:(CGVector)direction andBulletLifetime:(double)lifeTime andBulletType:(BulletType)btype andBulletSpeed:(float)bulletSpeed{
    if (self = [super initWithImageNamed:@"PCenter2.png"]){
        [self bulletInitMain:direction andBulletSpeed:bulletSpeed andBulletLifetime:lifeTime andBulletType:btype];
    }
    return self;
}

-(id)init:(CGVector)direction andBulletLifetime:(double)lifeTime andBulletType:(BulletType)btype{
    if (self = [super initWithImageNamed:@"PCenter2.png"]){
        [self bulletInitMain:direction andBulletSpeed:gBulletNormalSpeed andBulletLifetime:lifeTime andBulletType:btype];
        
    }
    return self;
}
-(void)update:(float)dt{
}

-(void)bulletInitMain:(CGVector)direction andBulletSpeed:(float)bulletSpeed andBulletLifetime:(double)lifeTime andBulletType:(BulletType)btype{
    //        self.normalTexture = [SKTexture textureWithImageNamed:@"player_center_n.png"];

    self.lightingBitMask = 0x1;
    constants = [Constants getInstance];
    self.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize: self.size];
    switch (btype) {
        case kNoBulletType:
            break;
        case kHeavyBulletType:
            self.physicsBody.categoryBitMask = constants.enemyBulletCollisionCategory;
            self.physicsBody.contactTestBitMask = constants.playerCollisionCategory | constants.screenEdgeCollisionCategory
            | constants.sceneWallCollisionCategory ;
            break;
        case kSimpleBulletType:
            self.physicsBody.categoryBitMask = constants.enemyBulletCollisionCategory;
            self.physicsBody.contactTestBitMask = constants.playerCollisionCategory | constants.screenEdgeCollisionCategory
            | constants.sceneWallCollisionCategory ;
            // self.audioNode = [[SKAudioNode alloc]initWithFileNamed:@"EnemyPewPew.wav"];
            break;
        case kPlayerBulletType:
            self.physicsBody.categoryBitMask = constants.playerBulletCollisionCategory;
            self.physicsBody.contactTestBitMask = constants.enemyCollisionCategory | constants.screenEdgeCollisionCategory | constants.sceneWallCollisionCategory | constants.keyCollisionCategory;
            break;
    }
    
    self.physicsBody.dynamic = NO;
    self.physicsBody.usesPreciseCollisionDetection = NO;
    self.physicsBody.restitution = 0;
    self.physicsBody.angularDamping = 1;
    self.physicsBody.linearDamping = 1;
    self.physicsBody.friction = 0;
    self.physicsBody.allowsRotation = NO;
    self.physicsBody.angularVelocity = 0;
    self.physicsBody.mass = 0.0001;
    
    self.physicsBody.collisionBitMask = 0x00;
    self.colorBlendFactor = gPlayerColorBlendFactor;
    self.color = [UIColor whiteColor];
    self.zPosition = gBulletZPosition;
    [self setScale:5];//0.85];
    self.name = @"Bullet";
    self.zRotation = M_PI;
    
    self.damage = 0;
    self.bulletSpeed = bulletSpeed;// gBulletNormalSpeed;
    
    dir = direction;
    
    vectorSupport = [[VectorSupport alloc]init];
    SKAction *runBlock = [SKAction runBlock:^{
        CGVector deltaPosition = self->dir;
        deltaPosition = [self->vectorSupport normalize:self->dir];
        deltaPosition = [self->vectorSupport scalarMultiply:deltaPosition andScalar:self.bulletSpeed];
        deltaPosition = [self->vectorSupport add:deltaPosition andVector2: CGVectorMake(self.position.x, self.position.y)];
        self.position = CGPointMake(deltaPosition.dx, deltaPosition.dy);
    }];
    
    SKAction *sleep = [SKAction waitForDuration:0.01];
    SKAction *sequence = [SKAction sequence:@[runBlock,sleep]];
    SKAction *run = [SKAction repeatActionForever:sequence];
    [self runAction:run];
    
    SKAction *wait2 = [SKAction waitForDuration: 2.2*lifeTime ];
    SKAction *remove = [SKAction runBlock:^{
        [self removeFromParent];
        [self removeAllActions];
    }];
    SKAction *s = [SKAction sequence:@[wait2, remove]];
    [self runAction:s];
    health = 2;
    //Emitter setup
    trailFX = [[SKEmitterNode alloc]init];
    NSString *fxTrail = [[NSBundle mainBundle] pathForResource:@"BulletTrail" ofType:@"sks"];
    trailFX = [NSKeyedUnarchiver unarchiveObjectWithFile:fxTrail];
    if (trailFX == nil)
        NSLog(@"Bullet::initializedScene can't load player particle emitter");
    trailFX.targetNode = constants.currentScene;
    trailFX.name = @"bulletTrailFX";
    [self addChild:trailFX];
    
#pragma mark lighting
    light = [[SKLightNode alloc]init];
     light.categoryBitMask = 0x00000001;
     light.lightColor = [UIColor lightGrayColor];
     light.ambientColor = [UIColor lightGrayColor];
     light.position = self.position;
     light.falloff = 0.5;
 //    [self addChild:light];
}
-(void)takeDamage:(float)damageReceived{
    health -= damageReceived;
    if (health < 0)
        [self remove];
}

-(void)remove{
    [self removeAllActions];
    [self removeAllChildren];
    [self removeFromParent];
}

-(void)dealloc{
    //    NSLog(@"Bullet dealloc");
}

@end

