//
//  EnemyBullet.h
//  EscapeSpaceShooter
//
//  Created by William Birmingham on 11/16/19.
//  Copyright © 2019, 2020 Imaginary Root Studio LLC. All rights reserved.
//

#ifndef EnemyBullet_h
#define EnemyBullet_h
#import <Foundation/Foundation.h>
#import <SpriteKit/SpriteKit.h>
#import "Constants.h"
#import "VectorSupport.h"
#import "Bullet.h"


@interface EnemyBullet : Bullet

-(id)init:(CGVector)direction;
@end

#endif /* EnemyBullet_h */
