//
//  Enemy.h
//  EscapeSpaceShooter
//
//  Created by William Birmingham on 10/28/19.
//  Copyright © 2019, 2020 Imaginary Root Studio LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SpriteKit/SpriteKit.h>
#import "Constants.h"
#import "VectorSupport.h"


@interface Enemy : SKSpriteNode


-(id)init:(CGPoint)location andDirection:(CGVector)velocity;

//-(NSInteger)damage;
-(void) update:(float) dt;
-(void) remove;
-(void) setMoveDirection:(CGVector)direction;
-(void) takeDamage:(float)damage;
-(void)playSound;

@property (nonatomic, strong) SKAudioNode* audioNode;
@end
