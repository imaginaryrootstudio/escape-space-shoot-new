//
//  RailShooter.m
//  EscapeSpaceShooter
//
//  Created by William Birmingham on 11/30/19.
//  Copyright © 2019, 2020 Imaginary Root Studio LLC. All rights reserved.
//

#import "RailShooter.h"

@implementation RailShooter{
    Constants *constants;
    VectorSupport *vectorSupport;
    Player *p;
    CGVector dir;
    CGVector oldDir;
    NSMutableArray *wayPoints;
    int currentWayPoint;
    bool directionChangeAllowed;
    float targetRotation;
    float fullHealth;
}

-(id)init:(CGVector) shootDir andCoolDown:(float)time andPlayer:(Player *)player{
    if (self = [super init:shootDir andCoolDown:time andBulletType:kSimpleBulletType]){
        constants = [Constants getInstance];
        vectorSupport = [[VectorSupport alloc]init];
        dir = shootDir;
        p = player;
        currentWayPoint = 0;
        wayPoints = [[NSMutableArray alloc]init];
        directionChangeAllowed = YES;
        fullHealth = self.health;
        self.name = @"RailShooter";
    }
    return self;
}
-(void)loadWayPoints:(NSArray *)waypoints{
    [wayPoints addObjectsFromArray:waypoints];
}

-(void)calculateNewDirection{
    SKSpriteNode* currentNode = wayPoints[currentWayPoint];
    //NSLog(@"Currentnode x %f y %f", currentNode.position.x, currentNode.position.y);
    CGPoint parentPosition = [self parent].position;
    dir = CGVectorMake(parentPosition.x - currentNode.position.x, parentPosition.y - currentNode.position.y);
    dir.dx *= -1;
    dir.dy *= -1;
    dir = [vectorSupport normalize:dir];
        oldDir = dir;
    [self parent].zRotation = currentNode.zRotation;
    
}

-(void)changeDirectionChangeAllowed{
    directionChangeAllowed = YES;
//    NSLog(@"Direction changw allowed");
}

-(void)navigateWaypoints{
    SKSpriteNode* currentNode = wayPoints[currentWayPoint];
    //NSLog(@"Currentnode x %f y %f", currentNode.position.x, currentNode.position.y);
    CGPoint parentPosition = [self parent].position;
    CGVector tempDirection = CGVectorMake(parentPosition.x - currentNode.position.x, parentPosition.y - currentNode.position.y);
    float distance = [vectorSupport magnitude:CGVectorMake(tempDirection.dx, tempDirection.dy)];
    if (distance < 1 && directionChangeAllowed){
        currentWayPoint++;
        currentWayPoint %= wayPoints.count;
        directionChangeAllowed = NO;
        [self calculateNewDirection];
        [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(changeDirectionChangeAllowed) userInfo:NULL repeats:NO];
    }
    dir = [vectorSupport normalize:dir];
}

-(void)moveRailShooter{
    [self calculateNewDirection];
    SKAction *runBlock = [SKAction runBlock:^{
        [self navigateWaypoints];
        CGVector deltaPosition = self->dir;
        deltaPosition = [self->vectorSupport scalarMultiply:deltaPosition andScalar:1.5];
        deltaPosition = [self->vectorSupport add:deltaPosition andVector2:CGVectorMake([self parent].position.x,
                                                                                       [self parent].position.y)];//
        [self parent].position = CGPointMake(deltaPosition.dx, deltaPosition.dy);
    }];
    
    SKAction *sleep = [SKAction waitForDuration:0.05];
    SKAction *sequence = [SKAction sequence:@[runBlock,sleep]];
    SKAction *run = [SKAction repeatActionForever:sequence];
    [self runAction:run];
}

-(void)stopRailShooter{
    dir = CGVectorMake(0, 0);
}

-(void)restartRailShooter{
    dir = oldDir;
}

-(void) takeDamage:(float)damageAmount{
    self.health -= damageAmount;
    if (self.health < 0){
        SKAction *wait = [SKAction waitForDuration: 2.5];
        SKAction *stop = [SKAction runBlock:^{
            [self stopRailShooter];
        }];
        SKAction *start = [SKAction runBlock:^{
            [self restartRailShooter];
            self.health = self->fullHealth;
        }];
        SKAction *s = [SKAction sequence:@[stop, wait, start]];
        [self runAction:s];
    }
}
@end
