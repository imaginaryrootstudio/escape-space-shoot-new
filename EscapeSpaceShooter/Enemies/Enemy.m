//
//  Enemy.m
//  EscapeSpaceShooter
//
//  Created by William Birmingham on 10/28/19.
//  Copyright © 2019, 2020 Imaginary Root Studio LLC. All rights reserved.
//

#import "Enemy.h"

@implementation Enemy{
    Constants *constants;
    VectorSupport *vectorSupport;
    CGVector moveDirection;
    float health;
}


-(id) init{
    NSLog(@"DO NOT USE THE DEFAULT CONSTRUCTOR: Enemy.m");
    return self;
}

-(id)init:(CGPoint)location andDirection:(CGVector)direction{
    if (self = [super initWithImageNamed:@"Enemy2.png"]){//@"enemy.png"]){
        constants = [Constants getInstance];
        moveDirection = direction;
        self.position = location;
//        self.normalTexture = [SKTexture textureWithImageNamed:@"enemy_n.png"];
        self.name = [NSString stringWithFormat:@"Enemy_%f", [[NSDate date] timeIntervalSince1970]];
        self.lightingBitMask = 0x10;
        self.color = [SKColor whiteColor];
        self.zPosition = gPlayerZPosition;
        [self setScale: 0.5];//gPlayerScale];
        
        self.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:self.size];
        self.physicsBody.categoryBitMask = constants.enemyCollisionCategory;
        self.physicsBody.contactTestBitMask = constants.playerCollisionCategory | constants.bulletCollisionCategory | constants.sceneWallCollisionCategory;
        self.physicsBody.collisionBitMask = 0x00;
        //        self.physicsBody.collisionBitMask = constants.playerCategory;
        self.physicsBody.dynamic = YES;
        self.physicsBody.usesPreciseCollisionDetection = NO;
        self.physicsBody.restitution = 0.0;
        self.physicsBody.angularDamping = 1.0;
        self.physicsBody.friction = 0.0;
        self.physicsBody.linearDamping = 0.0;
        self.physicsBody.affectedByGravity = NO;
        //        self.physicsBody.velocity = moveDirection;
        self.physicsBody.mass = 0.00001;
        vectorSupport = [[VectorSupport alloc]init];
        health = 2;
    }
    return self;
}


-(void)setMoveDirection:(CGVector)direction{
    moveDirection = direction;
}

-(void)update:(float) dt{
}

-(void)takeDamage:(float)damage{
    health -= damage;
    if (health <= 0){
        //stun the shooter
        [self removeAllActions];
       // [self remove];
    }
}

-(void)playSound{
    SKAction *enemySoundfx = [SKAction playSoundFileNamed:@"PlayerHurt.mp3" waitForCompletion:NO];
    [self runAction:enemySoundfx];
//    self.audioNode = [[SKAudioNode alloc]initWithFileNamed:@"PlayerHurt.mp3"];
//    self.audioNode.autoplayLooped = NO;
//    self.audioNode.positional = YES;
//    //[self.adu addChild:self.audioNode];
//    SKAction *play = [SKAction play];
//    SKAction *stop = [SKAction stop];
//    SKAction *wait = [SKAction waitForDuration:0.5];
//    SKAction *seq = [SKAction sequence:@[play, wait, stop]];
//    [self.audioNode runAction:seq completion:^{
//        [self.audioNode removeAllActions];
//        self.audioNode = nil;
//    }];
}

-(void)remove{
    [self removeAllActions];
    [self removeAllChildren];
    [self removeFromParent];
}
-(void)dealloc{
//    NSLog(@"Enemy dealloc");
}

@end
