//
//  Player.h
//  Escape: A Space Shooter
// Copyright (c) 2019, 2020 Imaginary Root Studio LLC. All rights reserved.

//#import <Foundation/Foundation.h>
#import <SpriteKit/SpriteKit.h>
#import "Constants.h"
#import "Bullet.h"
#import "VectorSupport.h"
#import <AVFoundation/AVFoundation.h>
#import "TapticEngine.h"

@interface Player : SKSpriteNode

@property float health;
@property NSInteger playerIndex; //index in players[] for this player object
@property float playerLifetime;
@property CGPoint lastPosition; //used to create a direction vector
//@property (nonatomic, retain) SKScene *scene;

-(id) init:(NSInteger)index andInitialPosition:(CGPoint)initialPos;
-(void) update:(float) dt;
-(void) playPlayerDamageFX;
-(void) playPlayerTrailFX;
-(void) stopPlayerTrailFX;

-(void) setPlayerAliasText:(NSString*)playerAlias;
-(void) playerTakeDamage:(NSInteger)damage;
-(void) playerTakeDamageWithNoSoundFX:(NSInteger)damage;
-(BOOL) playerAlive;
-(void) restoreAllHealth;
-(void) invincibility:(int)timeForInvincibility;
-(float) percentOfHealthRemaining;
-(void) playDieAnimation;
-(void) changeToEnemyTexture:(kEnemyAttackType)enemyAttackType;
-(void) restorePlayerCenterTexture;
-(void) setMovementDirection:(CGVector)direction;
-(void) shootBullet;
-(void) fadeInPlayer;
-(void) resetPlayerToInitialPosition;
-(void) playShootSoundFX;

@end
