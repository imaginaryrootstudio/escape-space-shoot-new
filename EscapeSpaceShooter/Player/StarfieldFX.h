//
//  StarfieldFX.h
//  EscapeSpaceShooter
//
//  Created by William Birmingham on 7/30/20.
//  Copyright © 2020 William Birmingham. All rights reserved.
//

#ifndef StarfieldFX_h
#define StarfieldFX_h

#import <SpriteKit/SpriteKit.h>
#import "Constants.h"

@interface StarfieldFX : SKSpriteNode

-(id)init:(SKScene*)scene topWallPosition:(CGPoint)topWallPosition bottomWallPosition:(CGPoint)bottomWallPosition leftWallPosition:(CGPoint)leftWallPosition rightWallPosition:(CGPoint)rightWallPosition;

@end
#endif
