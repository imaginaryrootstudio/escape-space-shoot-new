//
//  Player.m
//  Escape: A Space Shooter
// Copyright (c) 2019, 2020 Imaginary Root Studio LLC. All rights reserved.

// PewPew sound from https://freesound.org/people/Problematist/sounds/371745/ CC 0 license

#import "Player.h"

@implementation Player{
    Constants *constants;
    BOOL invincible;
    float invincibilityTimer;
    float invincibilityTimeLimit;
    SKSpriteNode *playerCenter;
    float fadeOutTimeDieAnimation;
    CGPoint initialPosition;
    SKAudioNode *audioNode;
    SKEmitterNode *playerDamageFX;
    bool playingPlayerDamageFX;
    NSString *fxTrail;
    SKEmitterNode *playerTrailFX;
    SKLightNode *playerLight;
    TapticEngine *tapticEngineHeavy;
//    SKScene *scene;
}
#define kPlayerAliasLabelName @"player_alias"

-(id)init{
    NSLog(@"Do not use the default init: Player.m");
    return self;
}
-(id)init:(NSInteger)index andInitialPosition:(CGPoint)initialPos{
    if (self = [super initWithImageNamed:@"POutline2.png"]){   //@"player_outline.png"]){
        self.normalTexture = [SKTexture textureWithImageNamed:@"POutline2_n.png"];
        self.lightingBitMask = 0x1;
        constants = [Constants getInstance];
        self.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize: self.size];
        self.physicsBody.categoryBitMask = constants.playerCollisionCategory;
        self.physicsBody.contactTestBitMask = constants.enemyCollisionCategory | constants.enemyBulletCollisionCategory | constants.bulletCollisionCategory | constants.sceneWallCollisionCategory | constants.keyCollisionCategory | constants.pickupCollisionCategory;
        
        self.physicsBody.collisionBitMask = 0x00;
        self.physicsBody.dynamic = NO;
        self.physicsBody.usesPreciseCollisionDetection = YES;
        self.physicsBody.restitution = 1;
        self.physicsBody.angularDamping = 1;
        self.physicsBody.linearDamping = 0.25;
        self.physicsBody.friction = 0;
        self.physicsBody.allowsRotation = YES;
        self.physicsBody.angularVelocity = 0;
        self.physicsBody.mass = 0.001;
        
        [self createLabel:@""];
        self.colorBlendFactor = gPlayerColorBlendFactor;
        self.color = [UIColor whiteColor];
        //        self.zPosition = gPlayerZPosition;
        self.zRotation = 0;
        self.position = initialPos;// CGPointMake(220,220);
        initialPosition = initialPos;
        self.zPosition = gPlayerZPosition;
        [self setScale:0.75];//gPlayerScale];
        self.health = gPlayerInitialHealth;
        self.playerLifetime = 0;
        self.playerIndex = index;
        
        self.lastPosition = CGPointMake(0, 0);
        
        //self.shadowCastBitMask = 0x1;
        
        invincible = NO;
        invincibilityTimer = 0;
        invincibilityTimeLimit = 0;
        
        //player center
        playerCenter = [[SKSpriteNode alloc]initWithImageNamed:@"PCenter2.png"];//player_center.png"];
        playerCenter.normalTexture = [SKTexture textureWithImageNamed:@"PCenter2_n.png"];
        [playerCenter setScale:1];//0.85];
        playerCenter.colorBlendFactor = gPlayerColorBlendFactor;
        playerCenter.color = [UIColor whiteColor];
        [self addChild:playerCenter];

        playerDamageFX.name = @"playerHitFX";
        playingPlayerDamageFX = NO;
        
        tapticEngineHeavy = [[TapticEngine alloc]init:UIImpactFeedbackStyleHeavy];
        [tapticEngineHeavy prepare];
        
        //        audioNode = [[SKAudioNode alloc]initWithFileNamed:@"PewPew.mp3"];
        //        audioNode.autoplayLooped = NO;
        //        audioNode.positional = YES;
        //        [self addChild:audioNode];
        //
        //        uint64_t c = constants.enemyDieCels.count;
        //        fadeOutTimeDieAnimation = c * gFadeOutTimeDieAnimation;
        
        playerLight = [[SKLightNode alloc]init];
        playerLight.categoryBitMask = 0x00000010;
        playerLight.lightColor = [UIColor whiteColor];
        playerLight.ambientColor = [UIColor blackColor];
        playerLight.position = self.position;
        [self addChild:playerLight];
 
    }
    return self;
}

//Made SKLabelNode a local variable to help fix a problem with restoring the text size after scaling.
//Turns out to not be necessary, but it probably makes the game more efficient, so leaving it here.
-(void)createLabel:(NSString*)playerName {
    SKLabelNode *playerAliasLabel = [[SKLabelNode alloc] initWithFontNamed:gDefaultFontName];
    playerAliasLabel.fontSize = gPlayerTextSize+20;
    playerAliasLabel.fontColor = [SKColor redColor];
    playerAliasLabel.position = CGPointMake(0, 0);
    playerAliasLabel.name = gPlayerAliasLabelName;
    playerAliasLabel.text = playerName;
    [self addChild:playerAliasLabel];
}

-(void) playPlayerTrailFX{
    fxTrail = [[NSBundle mainBundle] pathForResource:@"PlayerTrail" ofType:@"sks"];
    playerTrailFX = [NSKeyedUnarchiver unarchiveObjectWithFile:fxTrail];
    if (playerTrailFX == nil)
        NSLog(@"GameSceneL0::playPlayerTrailFX can't load player particle emitter");
    playerTrailFX.targetNode = self->constants.currentScene;
    playerTrailFX.name = @"playerTrailFX";
    playerTrailFX.targetNode = self->constants.currentScene;
    [self addChild:playerTrailFX];
}
-(void) stopPlayerTrailFX{
    playerTrailFX.targetNode = nil;
    [playerTrailFX removeFromParent];
    playerTrailFX = nil;
}
-(void) playPlayerDamageFX{
    SKAction *addParticles = [SKAction runBlock:^{
        fxTrail = [[NSBundle mainBundle] pathForResource:@"PlayerHitParticles" ofType:@"sks"];
       // self->playerDamageFX.numParticlesToEmit = 500;
       // self->playingPlayerDamageFX =
        self->playerDamageFX = [NSKeyedUnarchiver unarchiveObjectWithFile:fxTrail];
        if (self->playerDamageFX == nil)
            NSLog(@"GameSceneL0::playPlayerDamageFX can't load player particle emitter");
        self->playerDamageFX.targetNode = self->constants.currentScene;
        self->playerDamageFX.name = @"playerDamageFX";
        //self->playerDamageFX.targetNode = self->constants.currentScene;
        [self addChild:self->playerDamageFX];
        self->playingPlayerDamageFX = YES;
        
    }];
    SKAction *removeParticles = [SKAction runBlock:^{
        self->playerDamageFX.targetNode = nil;
        [self->playerDamageFX removeFromParent];
         self->playingPlayerDamageFX = NO;
        //self->playerDamageFX = nil;
    }];
    SKAction *wait = [SKAction waitForDuration:1];
    SKAction *sequence = [SKAction sequence:@[addParticles, wait, removeParticles,wait]];
    [self runAction:sequence];
 //      [self addChild:playerDamageFX];
}

-(void)playDieAnimation{
    
    //    [self restorePlayerCenterTexture];
    //    self.physicsBody.velocity = CGVectorMake(0, 0);
    //
    //    SKAction *fadeOut = [SKAction fadeOutWithDuration:1.1*fadeOutTimeDieAnimation];
    //    SKAction *animate = [SKAction animateWithTextures:constants.playerDieCels timePerFrame:gFadeOutTimeDieAnimation
    //                                               resize:NO restore:NO];
    //    SKAction *remove = [SKAction runBlock:^{
    //        [self removeAllChildren];
    //        [self removeAllActions];
    //        [self removeFromParent];
    //        constants.showGameOverScene = YES;
    //    }];
    //    SKAction *group = [SKAction group:@[fadeOut, animate]];
    //    SKAction *sequence = [SKAction sequence:@[group, remove]];
    //    [self runAction:animate];
}

//---- Update

-(void)update:(float) dt{

}

//------
// Helper Functions
//------

- (void)setPlayerAliasText:(NSString*)playerAlias{
    ((SKLabelNode*) [self childNodeWithName:kPlayerAliasLabelName]).text = playerAlias;
    //self.playerAliasProperty = playerAlias;
}


-(void) playerShake:(float) duration amplitudeX:(float) amplitudeX amplitudeY:(float) amplitudeY{
    float numShakes = duration*2/0.015;
    NSMutableArray *actions = [[NSMutableArray alloc]init];
    for (int i = 0; i<numShakes; i++) {
        float dx = arc4random_uniform(amplitudeX)-amplitudeX/2;
        float dy = arc4random_uniform(amplitudeY)-amplitudeY/2;
        SKAction *foo = [SKAction moveByX:dx y:dy duration:0.015];
        SKAction *reversedFoo = [foo reversedAction];
        [actions addObject:foo];
        [actions addObject:reversedFoo];
    }
    SKAction *shrink = [SKAction scaleBy:0.8 duration:0.1];
    SKAction *reverseShrink = [shrink reversedAction];
    SKAction *shrinkSequence = [SKAction sequence:@[shrink,reverseShrink]];
    SKAction *seq = [SKAction sequence:actions];
    SKAction *group = [SKAction group:@[shrinkSequence, seq]];
    [self runAction:group];
}

-(void)playerTakeDamage:(NSInteger)damage{
    if (invincible)
        return;
    [tapticEngineHeavy playImpactHaptic];
    if (!playingPlayerDamageFX)
        [self playPlayerDamageFX];
    playerCenter.color = [SKColor redColor];
    [self playerShake:0.5 amplitudeX:5 amplitudeY:6];
    self.health -= damage;
    if (self.health < 0){
        [self restoreAllHealth];
//        self.health = 1;
//        playerCenter.color = [SKColor whiteColor];
        [self resetPlayerToInitialPosition];
    }
}

//-(void)playerTakeDamageWithNoSoundFX:(NSInteger)damage{
//    if (invincible)
//        return;
//    if (!playingPlayerDamageFX)
//        [self playPlayerDamageFX];
//    playerCenter.color = [SKColor redColor];
//    self.alpha = 0.8;
//    self.health -= damage;
//    if (self.health < 0)
//        self.health = 0;
//}

-(BOOL)playerAlive{
    if (self.health > 0)
        return YES;
    else
        return NO;
}

-(void)invincibility:(int)timeForInvincibility{
    invincible = YES;
    invincibilityTimer = timeForInvincibility;
}

-(void)restoreAllHealth{
    self.health = gPlayerInitialHealth;
    self.alpha = 1;
    playerCenter.color = [SKColor whiteColor];
    playerCenter.alpha = 1;
    [playerCenter removeAllActions];
    self.alpha = 1.0;
    //[self restorePlayerCenterTexture];
}

-(float) percentOfHealthRemaining{
    return self.health/gPlayerInitialHealth;
}

-(void)playShootSoundFX{
    SKAction *play = [SKAction play];
    SKAction *stop = [SKAction stop];
    SKAction *wait = [SKAction waitForDuration:0.5];
    SKAction *seq = [SKAction sequence:@[play, wait, stop]];
    [audioNode runAction:seq];
}
-(void) changeToEnemyTexture:(kEnemyAttackType)enemyAttackType{
    //    NSString *textureName = [[NSString alloc]init];
    //    NSString *normalTextureName = [[NSString alloc]init];
    ////    NSLog(@"Attack type %li", (long)enemyAttackType);
    //    float scaleAmt = 0.95;
    //    float alphaAmt = 1;
    //    switch (enemyAttackType) {
    //        case kSpeedAttack:
    //            textureName = @"enemy_dash.png";
    //            normalTextureName = @"enemy_dash_n.png";
    //            break;
    //        case kRotatingAttack:
    //            textureName = @"enemy_spin.png";
    //            normalTextureName = @"enemy_spin_n.png";
    //            break;
    //        case kInvisibleAttack:
    //            alphaAmt = 0.8;
    //        case kShrinkingAttack:
    //        case kSpawnAttack:
    //            scaleAmt = 0.75;
    //            break;
    //        default:
    //            textureName = @"enemy.png";
    //            normalTextureName = @"enemy_n.png";
    //            break;
    //    }
    //    scaleAmt = 0.5;
    //    playerCenter.texture = [SKTexture textureWithImageNamed:textureName];
    //    [playerCenter setScale:scaleAmt];
    //    playerCenter.alpha = alphaAmt;
    //    playerCenter.normalTexture = [SKTexture textureWithImageNamed:normalTextureName];
    //    [playerCenter removeAllActions];
}

-(void)restorePlayerCenterTexture{
    //    playerCenter.texture = [SKTexture textureWithImageNamed:@"player_center.png"];
    //    playerCenter.normalTexture = [SKTexture textureWithImageNamed:@"player_center_n.png"];
    //     [playerCenter setScale:1.0];
}

-(void)resetPlayerToInitialPosition{
    SKAction *move = [SKAction moveTo:initialPosition duration:1.5];
    SKAction *fadeOut = [SKAction fadeAlphaTo:0.5 duration:1.5];
    SKAction *fadeIn = [SKAction fadeAlphaTo:1 duration:0.5];
    SKAction *group = [SKAction group:@[move, fadeOut]];
    SKAction *sequence = [SKAction sequence:@[group, fadeIn]];
    [self runAction:sequence];
}

-(void) fadeInPlayer{
    self.alpha = 0;
    self.hidden = NO;
    SKAction *fadeIn = [SKAction fadeAlphaTo:1 duration:1];
    [self runAction:fadeIn];
}

-(void)dealloc{
    [self removeAllActions];
    [self removeAllChildren];
    [self removeFromParent];
    //    NSLog(@"Player");
}

@end
