//
//  KeyObject.m
//  EscapeSpaceShooter
//
//  Created by William Birmingham on 2/12/20.
//  Copyright © 2020 William Birmingham. All rights reserved.
//

#import "KeyObject.h"
@implementation KeyObject{
    Constants *constants;
    SKEmitterNode *particleFX;
}
-(id)init{
    NSLog(@"Do not use the default init: KeyObject.m");
    return self;
}

-(id)init:(SKEmitterNode*)particles andPosition:(CGPoint)position{
    if ( [super initWithImageNamed:@"SceneExit.png"]){ //[super initWithColor:[UIColor whiteColor] size:CGSizeMake(1, 1)]){
        constants = [Constants getInstance];
        [self setScale:2];
        self.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize: self.size];
        self.physicsBody.categoryBitMask = constants.keyCollisionCategory;
        self.physicsBody.contactTestBitMask = constants.playerCollisionCategory || constants.playerBulletCollisionCategory;

        self.physicsBody.collisionBitMask = 0x00;
        self.physicsBody.dynamic = YES;
        self.physicsBody.usesPreciseCollisionDetection = YES;
        self.physicsBody.restitution = 1;
        self.physicsBody.angularDamping = 1;
        self.physicsBody.linearDamping = 0.25;
        self.physicsBody.friction = 0;
        self.physicsBody.allowsRotation = YES;
        self.physicsBody.angularVelocity = 0;
        self.physicsBody.mass = 0.001;
        
        
        self.color = [UIColor whiteColor];
        self.position = position;
        self.zPosition = gPlayerZPosition;
    }
    return self;
}

//-(void)dealloc{
//    NSLog(@"Player");
//}

@end
