//
//  PickupObject.h
//  EscapeSpaceShooter
//
//  Created by William Birmingham on 5/26/2020.
//  Copyright © 2020 William Birmingham. All rights reserved.
//

#ifndef KPickupObject_h
#define PickupObject_h

#import <SpriteKit/SpriteKit.h>
#import "Constants.h"

@interface PickupObject : SKSpriteNode

-(id)init:(SKEmitterNode*)particles andPosition:(CGPoint)position;

@end
#endif
