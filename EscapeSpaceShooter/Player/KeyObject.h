//
//  KeyObject.h
//  EscapeSpaceShooter
//
//  Created by William Birmingham on 2/12/20.
//  Copyright © 2020 William Birmingham. All rights reserved.
//

#ifndef KeyObject_h
#define KeyObject_h

#import <SpriteKit/SpriteKit.h>
#import "Constants.h"

@interface KeyObject : SKSpriteNode

-(id)init:(SKEmitterNode*)particles andPosition:(CGPoint)position;

@end
#endif /* KeyObject_h */
