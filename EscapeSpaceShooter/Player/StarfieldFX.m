//
//  StarfieldFX.m
//  EscapeSpaceShooter
//
//  Created by William Birmingham on 7/30/20.
//  Copyright © 2020 William Birmingham. All rights reserved.
//

#import "StarfieldFX.h"

@implementation StarfieldFX{
    Constants *constants;
    SKEmitterNode *particleFX;
}
-(id)init{
    NSLog(@"Do not use the default init: StarFieldFX.m");
    return self;
}

-(id)init:(SKScene*)scene topWallPosition:(CGPoint)topWall bottomWallPosition:(CGPoint)bottomWall leftWallPosition:(CGPoint)leftWall rightWallPosition:(CGPoint)rightWall;{
    if ( [super init]){ //[super initWithColor:[UIColor whiteColor] size:CGSizeMake(1, 1)]){
        particleFX = [[SKEmitterNode alloc]init];
        NSString* fxTrail = [[NSBundle mainBundle] pathForResource:@"StarfieldFX" ofType:@"sks"];
        particleFX = [NSKeyedUnarchiver unarchiveObjectWithFile:fxTrail];
        if (!particleFX)
            NSLog(@"GameScene::initializedScene can't load background particle emitter");
        particleFX.targetNode = scene;
        particleFX.name = @"BackgoundFX";
        particleFX.particlePositionRange = CGVectorMake(rightWall.x-leftWall.x, topWall.y-bottomWall.y);
        particleFX.position = CGPointMake((rightWall.x+leftWall.x)/2, (topWall.y+bottomWall.y)/2);
        [scene addChild: particleFX];
    }
    return self;
}

-(void)dealloc{
    NSLog(@"StarfieldFX");
}

@end
